<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$tId = '';
if (isset($_GET['tid'])) 
{
    $tId = $_GET['tid'];
}
?>
<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title> Edit Tutor Info | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
  <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
  <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
  <link rel="stylesheet" type="text/css" href="vendors/sweetalert/sweetalert.css">  
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
  <!-- END: Custom CSS-->
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

   <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

  <!-- BEGIN: Page Main-->
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span> Add New Tutor</span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                <li class="breadcrumb-item"><a href="tutor-list.php">Tutor's List </a>
                <li class="breadcrumb-item active">Add New Tutor
                </li>
              </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
            <div class="card-content">
              <div class="row">
                <form action="./inc/_tutorController.php" method="POST" enctype="multipart/form-data"> 
                  <div class="row">   
                    <div class="input-field col m4 s12">
                      <input type="text" id="tutorName"  name="tutorName" required/> 
                      <label for="tutorName">Tutor  Name</label>
                    </div>
                  </div>
                  <div class="row">   
                    <div class="input-field col m12 s12">
                    <textarea id="tutorDesc" class="materialize-textarea" name="tutorDesc" required></textarea>
                    <label for="tutorDesc">Tutor Description</label>
                    </div>
                  </div>
                  
                  <div class="row ">  
                    <div class="col m2 s12 mt-3">
                        <label>
                            <span class="black-text"> Upload Tutor Image : </span>
                        </label>
                    </div> 
                    <div class="col m5 s12 file-field inline input-field">  
                        <div class="btn float-right inline ardent-orange">
                        <span>Choose Image</span>
                        <input type="file" id="tutorImage" name="tutorImage" onchange="readImagURL(this,346,254,700,400);"  accept="image/x-png,image/jpeg"  required/>
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id="fileWrapper">
                      </div>
                    </div> 
              </div>

                  <div class="row"> 
                    <div class="col m8">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image"  />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
                  </div>
 
                  <div class="row"> 
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="addtutor">Submit
                          <i class="material-icons right">send</i>
                        </button>
                      </div>  
                  </div>  
                </form>
              </div>
            </div> 
          </div>
        </div> 
      </div> 
      <script src="js/vendors.min.js"></script> 
      <script src="js/plugins.min.js"></script> 
    <script src="vendors/sweetalert/sweetalert.min.js"></script>  
      <script src="js/custom/custom-script.min.js"></script>    
</body> 
</html>