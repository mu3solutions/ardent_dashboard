<?php 
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$refId = '';
if (isset($_GET['q'])) {
	$refId = $_GET['q'];
}
?>

<!DOCTYPE html>
  
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
     
    <title>Search Result | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
      <!-- BEGIN: VENDOR CSS-->
      <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
    <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
    <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head>
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Search Result</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a> 
                  <li class="breadcrumb-item active">Search Results
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
        <div class="row">
                                <?php if (isset($_SESSION['editSuccess'])) { ?>
                                <div class="col m6">
                                    <div class="card-alert card green">
                                        <div class="card-content white-text">
                                            <p><?php echo $_SESSION['editSuccess']; ?></p>
                                        </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                                                    </div>
                                        </div>
                <?php
                }   
                unset($_SESSION['editSuccess']);
                if (isset($_SESSION['editError'])) {
                ?> 
                <div class="col m6">
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p><?php echo $_SESSION['editError']; ?></p>
                        </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                </div>
                                <?php
                                }
                                unset($_SESSION['editError']);
                                ?>
                                </div>
        <?php  
                $table = substr($refId , 0 ,2) ;
                $tableName = '';
                switch ($table) {
                    case 'MC' :
                        $tableName = 'qa'; 
                        break;

                    case 'MQ' :
                        $tableName = 'mcq_qa_of_the_day'; 
                        break;

                    case 'GT' :
                        $tableName = 'qa_grand_test';
                        break;

                    case 'NT':
                        $tableName = 'tbl_nuggets_qa';
                        break;

                    case 'CV' :
                        $tableName = 'tbl_clinical_vignettes';
                        break; 
                }  
                if(!empty($refId)) {   
                  if(!empty($tableName)){      
                  $sql = "SELECT * FROM `$tableName` WHERE `ref_id` = '$refId'";
                  $result = mysqli_query($dbconnection, $sql);
                  if(mysqli_num_rows($result) > 0)
                  {
                  if($row = mysqli_fetch_array($result)) {  
                    $answer = array();
                    $answerImage = '';
                    switch ($table) {
                        case  'MC' : 
                            $question = $row['mcq_qn_desc'];
                            $option = $row['mcq_answers'];
                            $correctAnsnum = $row['mcq_qn_correct_answer_num'];  
                            $answerExp = $row['mcq_answer_desc'];  
                            $questionImage = $row['mcq_qn_desc_images'];  
                            $answerImage = $row['mcq_answer_desc_images'];  
                            $mcq_reference = $row['mcq_reference'];  
                            $testId = $row['mcq_child_id'];
                            break;

                        case  'MQ' : 
                            $question = $row['mcq_qn_desc'];
                            $option = $row['mcq_answers'];
                            $correctAnsnum = $row['mcq_qn_correct_answer_num'];  
                            $answerExp = $row['mcq_answer_desc'];  
                            $questionImage = $row['mcq_qn_desc_images'];  
                            $answerImage = $row['mcq_answer_desc_images'];  
                            $mcq_reference = $row['mcq_reference'];  
                            $testId = ''; 
                            break;

                        case  'GT' : 
                            $question = $row['mcq_qn_desc'];
                            $option = $row['mcq_answers'];
                            $correctAnsnum = $row['mcq_qn_correct_answer_num'];
                            $answerExp = $row['mcq_answer_desc'];  
                            $questionImage = $row['mcq_qn_desc_images'];   
                            $answerImage = $row['mcq_answer_desc_images']; 
                            $mcq_reference = $row['mcq_reference']; 
                            $testId = $row['testId'];
                            break;

                        case  'NT' : 
                            $question = $row['nuggets_qn_desc'];
                            $option = $row['nuggets_answers']; 
                            $correctAnsnum = $row['nuggets_qn_correct_answer_num']; 
                            $answerExp = $row['nuggets_answer_desc']; 
                            $questionImage = $row['nuggets_qn_desc_images'];   
                            $answerImage = $row['nuggets_answer_desc_images'];   
                            $mcq_reference = $row['nuggets_ans_ref'];  
                            $testId ='';
                            break;

                        case  'CV'  : 
                            $question = $row['cv_qn_desc'];
                            $option = $row['cv_answers']; 
                            $correctAnsnum = $row['cv_qn_correct_answer_num']; 
                            $answerExp = $row['cv_answer_desc']; 
                            $questionImage = $row['cv_qn_desc_images'];   
                            $answerImage = $row['cv_answer_desc_images'];
                            $mcq_reference = $row['cv_ans_ref']; 
                            $testId ='';
                            break; 
                    }
                    $answer  = explode('$&',$option);
                    $answer = array_combine(range(1, count($answer)), $answer); 
                    
                 if(!empty($answerImage)){$answerImage = explode(',',$answerImage);}
                  ?> 
          <div class="row"> 
          <form action="./inc/_ediTQuestions.php"  method="POST" enctype="multipart/form-data">
            
            <div class="row">
                <div class="input-field col s12">
                <textarea id="Question" class="materialize-textarea" name="Question"><?php echo $question; ?></textarea>
                  <label for="Question">Question</label> 
                </div> 
            </div>
            
            <input type="hidden" name="tableName" value="<?php echo $tableName; ?>">
            <input type="hidden" name="refID" value="<?php echo $refId; ?>">
            <input type="hidden" name="testId" value="<?php echo $testId; ?>">
            
            <div class="row">
              <div class="input-field col m6 s12">
                <input id="option1" type="text" name="option1" value="<?php echo $answer[1]; ?>" required>
                <label for="option1">Option 1</label>
              </div>
              <div class="input-field col m6 s12">
                <input id="option2" type="text" name="option2" value="<?php echo $answer[2]; ?>" required>
                <label for="option2">Option 2</label>
              </div> 
            </div>

            <div class="row">
              <div class="input-field col m6 s12">
                  <input id="option3" type="text" name="option3" value="<?php echo $answer[3]; ?>" required>
                  <label for="option3">Option 3</label>
              </div>
              <div class="input-field col m6 s12">
                <input id="option4" type="text" name="option4" value="<?php echo $answer[4]; ?>" required>
                <label for="option4">Option 4</label>
              </div>
            </div>
         
            <div class="row ">
                
                <div class="col m2 s12">
                  <label>Set Correct Answer</label>
                    <select name="correctoption" required >
                      <?php $a = 1; while ($a <= count($answer)) { ?> 
                        <option value="<?php echo $a; ?>"  <?php if($a == $correctAnsnum){ echo 'selected';} ?> ><?php echo 'Option '.$a; ?></option> 
                      <?php $a++; }  ?>
                    </select>
                </div>
 
                <div class="col m2 s12 mt-3">
                     <label> 
                      <span class="black-text">Upload Question Image : </span>
                    </label> 
                </div>
                
              
                <div class="col m4 s12 file-field inline input-field"> 
                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="questionImage" name="questionImage" accept="image/x-png,image/gif,image/jpeg"  onchange="readURL(this);">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id="fileWrapper">
                      </div>
                    </div>

                    <div class="col m4 s12 file-field inline input-field ">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image" />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
                  </div>

             
            <div class="row mt-2">
              <div class="input-field col s12">
              <textarea id="answerExplanation" name="answerExplanation" class="materialize-textarea"  ><?php echo $answerExp; ?></textarea>
                <label for="answerExplanation">Answer Explanation</label> 
              </div> 
            </div> 

            <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="answerRef" name="answerRef" class="materialize-textarea"><?php echo $mcq_reference; ?></textarea>
                      <label for="answerRef">Reference Explanation</label>
                    </div>
                  </div>

                  <div class="row ">
                    <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Answer Image : </span>
                      </label>
                    </div>
                    <div class="col m6 s12 file-field inline input-field">
                        <div class="btn float-right inline ardent-orange">
                            <span>Choose File</span>
                            <input type="file" id="answerImage" name="answerImage[]" accept="image/x-png,image/jpeg,image/jpg"  multiple>
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate valid" type="text">
                        </div>
                    </div>
                  </div>

                  <div id="preview"></div> 

                    <div class="row">
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="updateQuestion"> Submit <i class="material-icons right">send</i>
                        </button>
                      </div>
                    </div> 
                    <div class="divider mb-3 mt-3"></div>
                    <?php if(!empty($questionImage)){?> 
                      <div class="row ">  
                        <div class="col m3 s12  ">
                            <p>Current Question Image:</p>
                        </div>
                        <div class="col m6 s12"> 
                            <img src="<?php  echo $questionImage;?>" class="border-radius-10" alt="" width="400"   >
                        </div>
                      </div>
                      <div class="divider mt-3 mb-3"></div>
                      <?php } ?>
                      <?php if(!empty($answerImage)){?> 
                      <div class="row ">  
                        <div class="col m3 s12 mt-3">
                            <p>Current Answer Images:</p>
                        </div>
                        <?php for ($i=0; $i < count($answerImage) ; $i++) {  ?> 
                        <div class="col m3  mt-1 s12"> 
                            <img src="<?php  echo $answerImage[$i];?>" class="border-radius-10" alt="" width="200">
                        </div>
                        <?php } } ?>
                         
                      </div>
                      <?php }  ?> 
                </form>
                    <?php  }else { ?>
                    <div class="row center-align">  
                      <div class="col m12 s12">
                      <h4 class="text-center"> No Search Results Found</h4>
                          <img   width="100" height="100" src="./images/favicon/no-results.png" alt="">
                      </div> 
                  </div>
                  <?php }  } else { ?>
                  <div class="row center-align">  
                      <div class="col m12 s12">
                          <h4 class="text-center"> No Search Results Found</h4>
                          <img   width="100" height="100" src="./images/favicon/no-results.png" alt="">
                      </div> 
                  </div>

                  <?php } } else{ ?>
                  <div class="row center-align">  
                      <div class="col m12 s12">
                          <h4 class="text-center"> No Search Results Found</h4>
                          <img   width="100" height="100" src="./images/favicon/no-results.png" alt="">
                      </div> 
                  </div>
                  <?php } ?>
        </div>
      </div>
    </div>
  </div> 
    </div>  
    <script src="js/vendors.min.js"></script> 
    <script src="js/plugins.min.js"></script> 
    <script src="js/scripts/ui-alerts.min.js"></script> 
    <script src="js/custom/custom-script.min.js"></script>  
  </body> 
</html>