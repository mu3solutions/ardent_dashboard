<?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php'); 
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title> Send Push Notification | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css"> 

  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
  <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
  <link rel="stylesheet" href="vendors/introjs/css/introjs.css"> 

  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">

</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

   <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

  <!-- BEGIN: Page Main-->
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span>Send Mobile Notification</span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a> 
                <li class="breadcrumb-item active">Send Mobile Notification
                </li>
              </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
            <div class="card-content" id="singleQuestion">
              <div class="row ">
                <form  
                    action="./inc/_sendNotification.php" 
                    method="POST" 
                    enctype="multipart/form-data" 
                    id="formNotification" >  
          <div class="row">
            <div class="col m6  s12">
            <?php if (isset($_SESSION['pushnotificationSuccess'])) { ?> 
                                    <div class="card-alert card green">
                                        <div class="card-content white-text">
                                            <p><?php echo $_SESSION['pushnotificationSuccess']; ?></p>
                                        </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                            </div> 
                <?php
                }   
                unset($_SESSION['pushnotificationSuccess']);
                if (isset($_SESSION['pushnotificationError'])) {
                ?>  
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p><?php echo $_SESSION['pushnotificationError']; ?></p>
                        </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                    </div> 
                                <?php
                                }
                                unset($_SESSION['pushnotificationError']);
                                ?>
            </div> 
          </div>     
                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="notificationTitle" class="materialize-textarea" name="notificationTitle" required></textarea>
                      <label for="notificationTitle">Notification Title</label>
                    </div>
                  </div>
                  
                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="notificationMessage" class="materialize-textarea" name="notificationMessage" required></textarea>
                      <label for="notificationMessage">Notification Message</label>
                    </div>
                  </div> 

                  <div class="row "> 

                    <div class="col m3 s12 mt-3">
                      <label>
                        <span class="black-text"> Choose Notification Image : </span>
                      </label>
                    </div>


                    <div class="col m4 s12 file-field inline input-field"> 
                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="questionImage" name="questionImage" onchange="readURL(this);">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id="fileWrapper">
                      </div>
                    </div>

                    <div class="col m4 s12 file-field inline input-field ">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image" />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
                  </div>  


                  <div class="row"> 
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="pushNotification" id="pushNotification"> Submit <i class="material-icons right">send</i>
                        </button>
                      </div> 
                  </div>


              </div>
            </div> 
          </div>
        </div> 
      </div> 
      <script src="js/vendors.min.js"></script>  
      <script src="js/plugins.min.js"></script>  
      <script src="js/scripts/ui-alerts.min.js"></script>
      <script src="js/custom/custom-script.min.js"></script> 
</body> 
</html>