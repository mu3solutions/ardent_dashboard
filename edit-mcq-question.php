<?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
if(isset($_GET['qid'])){
$questionId = decryptData($_GET['qid']);
}
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title>Edit MCQ Questions | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
  <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
  <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
  <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
  <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
  <link rel="stylesheet" type="text/css" href="vendors/sweetalert/sweetalert.css"> 
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
  <!-- END: Custom CSS-->
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

   <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

  <!-- BEGIN: Page Main-->
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add New MCQ Question</span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                <li class="breadcrumb-item"><a href="mcq-questions-list.php">MCQ Question List </a>
                <li class="breadcrumb-item active">Add New Question
                </li>
              </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
            <div class="card-content" id="singleQuestion">
              <div class="row ">
                <form action="./inc/_insertMCQquestions.php" method="POST" enctype="multipart/form-data">
                <?php
                if(!empty($questionId)) {
                $sql = "SELECT * FROM `qa` WHERE `mcq_qn_id`='$questionId'";
				        $result = mysqli_query($dbconnection, $sql);
                if(mysqli_num_rows($result) > 0)
                {
                 if($row = mysqli_fetch_array($result)) {   
                  $options = array();
                  $answerImage = array();
                  $question = $row['mcq_qn_desc']; 
                  $options = explode('$&',$row['mcq_answers']);
                  $answerImage = explode(',',$row['mcq_answer_desc_images']); 
                 ?>
                 <input type="hidden" name="questionId" value="<?php echo $questionId; ?>">
                 <input type="hidden" name="childId" value="<?php echo $row['mcq_child_id']; ?>">
                   <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="Question" class="materialize-textarea" name="Question" required><?php echo $question; ?></textarea>
                      <label for="Question">Question</label>
                    </div>
                   </div>
                   
                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="option1" type="text" name="option1" value="<?php echo $options[0]; ?>" required >
                      <label for="option1">Option 1</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="option2" type="text" name="option2" value="<?php echo $options[1]; ?>" required>
                      <label for="option2">Option 2</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="option3" type="text" name="option3" value="<?php echo $options[2]; ?>" required>
                      <label for="option3">Option 3</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="option4" type="text" name="option4" value="<?php echo $options[3]; ?>" required>
                      <label for="option4">Option 4</label>
                    </div>
                  </div>

                  <div class="row ">
                    <div class="col m2 s12">
                      <label>Set Correct Answer</label>
                      <select name="correctoption" required>
                      <option value="1" <?php if($row['mcq_qn_correct_answer_num']=='1'){echo 'selected';} ?> >Option 1</option>
                      
                        <option value="2" <?php if($row['mcq_qn_correct_answer_num']=='2'){echo 'selected';} ?>>Option 2</option>
                        <option value="3" <?php if($row['mcq_qn_correct_answer_num']=='3'){echo 'selected';} ?>>Option 3</option>
                        <option value="4" <?php if($row['mcq_qn_correct_answer_num']=='4'){echo 'selected';} ?>>Option 4</option>
                      </select>
                    </div>

                    <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Question Image : </span>
                      </label>
                    </div>


                    <div class="col m4 s12 file-field inline input-field">

                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="questionImage" name="questionImage" accept="image/x-png,image/gif,image/jpeg"  onchange="readURL(this);">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id="fileWrapper">
                      </div>
                    </div>

                    <div class="col m4 s12 file-field inline input-field">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>Image Preview : </p>
                          <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image" />
                          <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                            <i class="material-icons">clear</i>
                          </button>
                      </div>
                    </div>
                    
                  </div>


                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="answerExplanation" name="answerExplanation" class="materialize-textarea"><?php echo $row['mcq_answer_desc']; ?></textarea>
                      <label for="answerExplanation">Answer Explanation</label>
                    </div>
                  </div>
                 
                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="answerExplanation" name="answerReferece" class="materialize-textarea"><?php echo $row['mcq_reference']; ?></textarea>
                      <label for="answerExplanation">Reference Explanation</label>
                    </div>
                  </div>

                  <div class="row ">
                    <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Answer Image : </span>
                      </label>
                    </div>
                    <div class="col m6 s12 file-field inline input-field">
                        <div class="btn float-right inline ardent-orange">
                            <span>Choose File</span>
                            <input type="file" id="answerImage" name="answerImage[]" accept="image/x-png,image/jpeg,image/jpg"  multiple>
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate valid" type="text">
                        </div>
                    </div>
                  </div>
                  <div id="preview"></div>
 
                    <div class="row">
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="editMCQQuestion"> Submit <i class="material-icons right">send</i>
                        </button>
                      </div>
                    </div> 
                    <div class="divider "></div>
                    <?php if(!empty($row['mcq_qn_desc_images'])){?> 
                      <div class="row ">  
                        <div class="col m3 s12 mt-3">
                            <p>Current Question Image:</p>
                        </div>
                        <div class="col m6 s12"> 
                            <img src="<?php  echo $row['mcq_qn_desc_images'];?>" class="border-radius-10" alt="" width="400"   >
                            <button type="button" onclick="removeImage('qa','mcq_qn_desc_images','mcq_qn_id',<?php echo $questionId; ?>)" class="btn btn-lg mb-1 waves-effect waves-light ardent-orange" >
                              Delete Image
                            </button>
                        </div>
                      </div>
                      <div class="divider mt-3 mb-3"></div>
                      <?php } ?>
                      <?php if(!empty($row['mcq_answer_desc_images'])){?> 
                      <div class="row ">  
                        <div class="col m3 s12 mt-3">
                            <p>Current Answer Images:</p>
                        </div>
                        <?php for ($i=0; $i < count($answerImage) ; $i++) {  ?> 
                        <div class="col m3  mt-1 s12"> 
                            <img src="<?php  echo $answerImage[$i];?>" class="border-radius-10" alt="" width="200">
                        </div>
                        <?php } } ?>
                         
                      </div>
                      <?php }  ?> 
                    <?php  }else { ?>
                    <div class="row center-align">  
                      <div class="col m12 s12">
                          <h3 class="text-center"> Your Link is Broken</h3>
                          <img class="z-depth-4 circle" width="100" height="100" src="./images/favicon/no-access-icon.png " alt="">
                      </div> 
                  </div>
                  <?php }  } else { ?>
                  <div class="row center-align">  
                      <div class="col m12 s12">
                          <h3 class="text-center"> Your Link is Broken</h3>
                          <img class="z-depth-4 circle" width="100" height="100" src="./images/favicon/no-access-icon.png " alt="">
                      </div> 
                  </div>

                  <?php } ?>
                </form>
              </div>
            </div>
            
          </div>
        </div> 
      </div> 
    <script src="js/vendors.min.js"></script>  
    <script src="js/plugins.min.js"></script> 
    <script src="vendors/sweetalert/sweetalert.min.js"></script> 
    <script src="js/custom/custom-script.min.js"></script>   
</body> 
</html> 