 <?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
?>

<!DOCTYPE html>   
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> 
    <title>Add New Sub Module | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
    <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
    <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head>
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->
  <!-- END: SideNav-->


    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add Sub Module</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="mcq-sub-module-list.php"> Add Sub Module</a>
                  </li>
                  <li class="breadcrumb-item active">Add Sub Module
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
          <div class="row"> 
          
          <form action="./inc/_MCQcontroller.php" method="POST">  
            <div class="row">  
            <div class="col mt-2 m4 s12">
                          <label>Select Main Module</label>
                          <?php 
                              $query = "SELECT * FROM `mcq_module` ORDER BY `mcq_mid` ASC";
                              $result = mysqli_query($dbconnection, $query);
                              ?>
                              <select name="mainModuleName"  class="select2 browser-default" id="MainModule" required>     
                              <option value="" selected disabled>Select Module first</option>
                                  <?php
                                  if (mysqli_num_rows($result) > 0) {
                                    while ($row = mysqli_fetch_array($result)) 
                                    {
                                    echo "
                                      <option value='".$row['mcq_mid']."'>".$row['mcq_mname']."</option>
                                      ";
                                    }
                                  } 
                                  else 
                                  {
                                    echo "<option>'Module Not Available'</option>";
                                  }
                                  ?>
                             </select>
                      </div>
                      
                      <div class="col mt-2 m4 s12">
                          <label>Select Sub Module</label>
                          <select id="SubModuleSelect" class="select2-tag browser-default" name="subModuleid" required>  
                          <option> Select Module first</option>
                          </select>
                      </div>
                     
                      <div class="input-field col m4 s12 mt-4"> 
                          <input id="subChildModuleValue" type="text" name="subChildModuleValue" required> 
                          <label for="subChildModuleValue" class="">New Chlid Module Name</label>
                      </div>
            </div>  

            <div class="row mt-3">  
                <div class=" col m4 s12">
                    <label>Select Module Status</label>
                    <select id="testopen" name="testopen" required>  
                        <option value="0"> Opened </option>
                        <option value="1"> Locked </option>
                    </select>
                </div> 
                <div class="input-field col m8 s12">
                    <textarea id="smtext" class="materialize-textarea" name="smtext" required></textarea>
                    <label for="smtext">Main Module Description</label>
                </div> 
            </div>  
            
            <div class="row">   
                <div class="input-field col s12 center"> 
                  <button class="btn ardent-orange waves-effect waves-light" type="submit" name="addNewSubModule">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div>
 
          </form>    
        </div>
      </div>
    </div>
  </div>
       
    </div>
 
    <!-- END: Footer-->
   <!-- BEGIN VENDOR JS-->
   <script src="js/vendors.min.js"></script>   
    <script src="js/plugins.min.js"></script>  
    <script src="vendors/select2/select2.full.min.js"></script>
    <script src="js/scripts/form-select2.min.js"></script>
    <script src="js/custom/custom-script.min.js"></script> 
    <script>
    $(function() 
    { 

        $(".select2-tag").select2({
        tags: true
        }); 

    });  
 
    </script>
  </body> 
</html>