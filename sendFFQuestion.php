<?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
if(isset($_GET['qid'])){
$questionId = $_GET['qid'];
}
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title>Send Question | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
  <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
  <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
  <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
  <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
  <link rel="stylesheet" href="vendors/introjs/css/introjs.css"> 
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
  <!-- END: Custom CSS-->
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

   <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

  <!-- BEGIN: Page Main-->
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span>Review FF Questions</span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                <li class="breadcrumb-item"><a href="ff-quiz.php">FF Question List </a>
                <li class="breadcrumb-item active">Review FF Questions
                </li>
              </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
            <div class="card-content" id="singleQuestion">
              <div class="row ">
                <form action="./inc/_ffController.php" method="POST" enctype="multipart/form-data">
                <?php
                $sql = "SELECT * FROM `tbl_ff_qa` WHERE `ff_qn_id`='$questionId'";
				        $result = mysqli_query($dbconnection, $sql);
                if ($row = mysqli_fetch_array($result)) 
                {   
                  $options = array();
                  $answerImage = array();
                  $question = $row['ff_qn_desc']; 
                  $options = explode('$&',$row['ff_answers']);
                  $answerImage = explode(',',$row['ff_answer_desc_images']); 
                 ?>
                 <input type="hidden" name="questionId" value="<?php echo $questionId; ?>"> 
                 <div class="row">
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="send_question" > Send <i class="material-icons right">send</i>
                        </button>
                        <a href="edit-ff-questions.php?qid=<?php echo $questionId ?>" class="btn ardent-orange waves-effect waves-light"> Edit <i class="material-icons right">edit</i>
                        </a>
                      </div>
                       
                    </div> 
                   <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="Question" class="materialize-textarea" name="Question" readonly   required><?php echo $question; ?></textarea>
                      <label for="Question">Question</label>
                    </div>
                   </div>
                   
                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="option1" type="text" name="option1" value="<?php echo $options[0]; ?>" readonly   required >
                      <label for="option1">Option 1</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="option2" type="text" name="option2" value="<?php echo $options[1];  ?>"  readonly   required>
                      <label for="option2">Option 2</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="option3" type="text" name="option3" value="<?php echo $options[2]; ?>" readonly required>
                      <label for="option3">Option 3</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="option4" type="text" name="option4" value="<?php echo $options[3]; ?>" readonly required>
                      <label for="option4">Option 4</label>
                    </div>
                  </div>

                  <div class="row ">
                    <div class="col m2 s12">
                      <label>Set Correct Answer</label>
                      <select name="correctoption" required readonly disabled>
                      <option value="1" <?php if($row['ff_qn_correct_answer_num']=='1'){echo 'selected';} ?> >Option 1</option>
                      
                        <option value="2" <?php if($row['ff_qn_correct_answer_num']=='2'){echo 'selected';} ?>>Option 2</option>
                        <option value="3" <?php if($row['ff_qn_correct_answer_num']=='3'){echo 'selected';} ?>>Option 3</option>
                        <option value="4" <?php if($row['ff_qn_correct_answer_num']=='4'){echo 'selected';} ?>>Option 4</option>
                      </select>
                    </div>
 
 
  
                    <div class="input-field col m10 s12">
                      <textarea id="answerExplanation" name="answerExplanation" class="materialize-textarea" readonly  ><?php echo $row['ff_answer_desc']; ?></textarea>
                      <label for="answerExplanation">Answer Explanation</label>
                    </div> 
                  </div>   
                  
                    <?php if(!empty($row['ff_qn_desc_images'])){  ?>
                    <div class="divider mb-3"></div>
                     <div class="row ">  
                      <div class="col m3 s12 mt-3">
                          <p>Current Question Image:</p>
                      </div>
                      <div class="col m6 s12"> 
                          <img src="<?php  echo $row['ff_qn_desc_images'];?>" class="border-radius-10" alt="" width="200" height="200"   >
                      </div>
                  </div>
                  <?php } ?>
                  <?php if(!empty($row['ff_qn_desc_images'])){  ?>
                    <div class="divider mt-3 mb-3"></div>
                     <div class="row ">  
                      <div class="col m3 s12 mt-3">
                          <p>Current Answer Images:</p>
                      </div>
                      <?php for ($i=0; $i < count($answerImage) ; $i++) {  ?> 
                      <div class="col m3 s12"> 
                          <img src="<?php  echo $answerImage[$i];?>" class="border-radius-10" alt="" width="200"   >
                      </div>
                      <?php } ?>
                  </div>
                  <?php } ?>
                  <?php } ?>
                </form>
              </div>
            </div>
            
          </div>
        </div> 
      </div> 
      <script src="js/vendors.min.js"></script>  
      <script src="js/plugins.min.js"></script> 
      <script src="js/custom/custom-script.min.js"></script>   
</body> 
</html> 