<?php
include('dbConfig.php');
include('functions.php');
session_start();
if(isset($_POST['updateQuestion']))
{
    $question = $_POST['Question'];
    $tableName = $_POST['tableName'];
    $refID = $_POST['refID'];
    $option1 = $_POST['option1']; 
    $option2 = $_POST['option2']; 
    $option3 = $_POST['option3']; 
    $option4 = $_POST['option4']; 
    $correctoption = $_POST['correctoption']; 
    $answerExplanation = $_POST['answerExplanation']; 
    $answerRef = $_POST['answerRef']; 
    $testId  = $_POST['testId']; 

    if(isset($_POST['questionImage'])){
        $questionImage = $_POST['questionImage'];  
    }

    $arr = array($option1,$option2,$option3,$option4);
    $finalAns = implode("$&",$arr); 

    $table = substr($refID , 0 ,2) ;

     
    switch ($table) {
        case  'MC' : 
            $question_col = "mcq_qn_desc";
            $option_col = "mcq_answers";
            $correctAnsnum_col  = "mcq_qn_correct_answer_num";  
            $answerExp_col = "mcq_answer_desc";  
            $question_colImage = "mcq_qn_desc_images";  
            $answerImage_col = "mcq_answer_desc_images";
            $answerref = "mcq_reference";
            $tables = "mcq";
            break;
        
            case  'MQ' : 
            $question_col = "mcq_qn_desc";
            $option_col = "mcq_answers";
            $correctAnsnum_col  = "mcq_qn_correct_answer_num";  
            $answerExp_col = "mcq_answer_desc";  
            $question_colImage = "mcq_qn_desc_images";  
            $answerImage_col = "mcq_answer_desc_images";
            $answerref = "mcq_reference";
            $tables = "mcqoftd";
            break;

        case  'GT' : 
            $question_col = "mcq_qn_desc";
            $option_col = "mcq_answers";
            $correctAnsnum_col  = "mcq_qn_correct_answer_num";
            $answerExp_col = "mcq_answer_desc";  
            $question_colImage = "mcq_qn_desc_images";   
            $answerImage_col = "mcq_answer_desc_images";  
            $answerref = "mcq_reference";  
            $tables = "grand";
            break;

        case  'NT' : 
            $question_col = "nuggets_qn_desc";
            $option_col = "nuggets_answers"; 
            $correctAnsnum_col  = "nuggets_qn_correct_answer_num"; 
            $answerExp_col = "nuggets_answer_desc"; 
            $question_colImage = "nuggets_qn_desc_images";   
            $answerImage_col = "nuggets_answer_desc_images";   
            $answerref = "nuggets_ans_ref"; 
            $tables = "3min"; 
            break;

        case  'CV'  : 
            $question_col = "cv_qn_desc";
            $option_col = "cv_answers"; 
            $correctAnsnum_col  = "cv_qn_correct_answer_num"; 
            $answerExp_col = "cv_answer_desc"; 
            $question_colImage = "cv_qn_desc_images";   
            $answerImage_col = "cv_answer_desc_images";   
            $answerref = "cv_ans_ref";   
            $tables = "cv"; 
            break; 
    }


    switch ($table) {
        case  'MC' : 
            $chlidModule = $_POST['testId'];  
            $childModuleName = childNameUsingId($chlidModule,$dbconnection);   
            $trun = (strlen($childModuleName) > 40) ? substr($childModuleName, 0, 20) . '...' : $childModuleName;  

            $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/MCQ/'; 
            $location = "../images/MCQ/".$chlidModule.$trun;
            if(!is_dir($location)){
                mkdir($location, 0755);
            }

            if (!empty($_FILES['questionImage']['name'])) {
                $image = '';
                $image = $_FILES['questionImage']['name'];
                $uniquequesimage = uniqidReal() . $image; 
                $location  .= "/" .basename($uniquequesimage);
                $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $location);
                $questionImage = $imageURL .$chlidModule. $trun .'/' .$uniquequesimage; 
                mysqli_query($dbconnection,"UPDATE `qa` SET `mcq_qn_desc_images` = '$questionImage' WHERE `ref_id` = '$refID' ");
            }
            
                if (isset($_FILES['answerImage'])) { 
                $uniquequesimage = [];
        
                $total = count($_FILES['answerImage']['name']);
        
                for ($i = 0; $i < $total; $i++) {
                    
                    $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];
        
                    if ($tmpFilePath != "") 
                    {
                        $uniquedId = uniqidReal();
                        $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                        $uniquequesimageURL[] = $imageURL.$testId.$trun . '/'.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                        $newFilePath = "../images/MCQ/".$testId.$trun . '/'.$uniquequesimage[$i];  
                        if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                        {
                            if(!empty($_FILES['answerImage']['name'][$i])){
                                $answerImage = implode(',', $uniquequesimageURL);   
                            }
                        }
                    }
                }
                if(!empty($_FILES['answerImage']['name'][0])){ 
                    mysqli_query($dbconnection,"UPDATE `qa` SET `mcq_answer_desc_images` = '$answerImage' WHERE `ref_id` = '$refID' ");
        
                } 
            }  
           

            break;
        
            case  'MQ' : 
                $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/mcq-ofthe-day/'; 
    
                $location = "../images/mcq-ofthe-day/";
                if(!is_dir($location)){
                    mkdir($location, 0755);
                }
             
                if (!empty($_FILES['questionImage']['name'])) {
                    $image = '';
                    $image = $_FILES['questionImage']['name'];
                    $uniquequesimage = uniqidReal() . $image; 
                    $location  .= "/" .basename($uniquequesimage);
                    $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $location);
                    $questionImage = $imageURL .'/' .$uniquequesimage; 
                    mysqli_query($dbconnection,"UPDATE `mcq_qa_of_the_day` SET `mcq_qn_desc_images` = '$questionImage' WHERE `ref_id` = '$refID' "); 
                }
                
                    if (isset($_FILES['answerImage'])) { 
                    $uniquequesimage = [];
            
                    $total = count($_FILES['answerImage']['name']);
            
                    for ($i = 0; $i < $total; $i++) {
                        
                        $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];
            
                        if ($tmpFilePath != "") 
                        {
                            $uniquedId = uniqidReal();
                            $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                            $uniquequesimageURL[] = $imageURL. '/'.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                            $newFilePath = "../images/mcq-ofthe-day/".$uniquequesimage[$i];  
                            if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                            {
                                if(!empty($_FILES['answerImage']['name'][$i])){
                                    $answerImage = implode(',', $uniquequesimageURL);   
                                }
                            }
                        }
                    }
                    if(!empty($_FILES['answerImage']['name'][0])){ 
                        mysqli_query($dbconnection,"UPDATE `mcq_qa_of_the_day` SET `mcq_answer_desc_images` = '$answerImage' WHERE `ref_id` = '$refID' "); 
                    } 
                }  
                 
            break;

        case  'GT' : 

            $testId = $_POST['testId'];  
            $grandTestName = fetchData($dbconnection,'gt_test_name','grand_test',$testId,'testid'); 
            $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/GrandTest/'; 
            $location = "../images/GrandTest/".$testId.$grandTestName;
            if(!is_dir($location)){
                mkdir($location, 0755);
            }
            if (!empty($_FILES['questionImage']['name'])) { 
            $image = '';
            $image = $_FILES['questionImage']['name'];
            $uniquequesimage = uniqidReal() . $image; 
            $location  .= "/" .basename($uniquequesimage);
            $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $location);
            $questionImage = $imageURL .$testId. $grandTestName .'/' .$uniquequesimage; 
            mysqli_query($dbconnection,"UPDATE `qa_grand_test` SET `mcq_qn_desc_images` = '$questionImage' WHERE `ref_id` = '$refID' "); 
              }
    
        if (isset($_FILES['answerImage'])) { 
            $uniquequesimage = [];

            $total = count($_FILES['answerImage']['name']);

            for ($i = 0; $i < $total; $i++) {
                
                $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

                if ($tmpFilePath != "") 
                {
                    $uniquedId = uniqidReal();
                    $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                    $uniquequesimageURL[] = $imageURL.$testId.$grandTestName . '/'.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                    $newFilePath = "../images/GrandTest/".$testId.$grandTestName . '/'.$uniquequesimage[$i]; 
                    if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                    {
                        if(!empty($_FILES['answerImage']['name'][$i])){
                            $answerImage = implode(',', $uniquequesimageURL);   
                        }
                    }
                }
            }
            if(!empty($_FILES['answerImage']['name'][0])){ 
                mysqli_query($dbconnection,"UPDATE `qa_grand_test` SET `mcq_answer_desc_images` = '$answerImage' WHERE `ref_id` = '$refID' ");

            }
        
        }

            break;

        case  'NT' : 
            $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/3minchallenge/'; 
   
            if (!empty($_FILES['questionImage']['name'])) { 
                $image = '';
                $image = $_FILES['questionImage']['name'];
                $uniquequesimage = uniqidReal() . $image;
                $target = "../images/3minchallenge/" . basename($uniquequesimage);
                $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
                $questionImage = $imageURL . $uniquequesimage;
                mysqli_query($dbconnection,"UPDATE `tbl_nuggets_qa` SET `nuggets_qn_desc_images` = '$questionImage' WHERE `ref_id` = '$refID' ");
            }
            
                if (isset($_FILES['answerImage'])) { 
                $uniquequesimage = [];
        
                $total = count($_FILES['answerImage']['name']);
        
                for ($i = 0; $i < $total; $i++) 
                {
                    $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];
        
                    if ($tmpFilePath != "") 
                    {
                        $uniquedId = uniqidReal();
                        $uniquequesimage[] = $uniquedId . $_FILES['answerImage']['name'][$i];
                        $uniquequesimageURL[] = $imageURL . $uniquedId . $_FILES['answerImage']['name'][$i];
                        $newFilePath = "../images/3minchallenge/" . $uniquequesimage[$i];
        
                        if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                            $answerImage = implode(',', $uniquequesimageURL);
                        }
                    }
                }
                if(!empty($_FILES['answerImage']['name'][0])){ 
                    mysqli_query($dbconnection,"UPDATE `tbl_nuggets_qa` SET `nuggets_answer_desc_images` = '$answerImage' WHERE `ref_id` = '$refID' ");
        
                }
                
            }  
            break;

        case  'CV'  : 
            $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/clinical/';
            if (!empty($_FILES['questionImage']['name'])) 
            {
                $image = '';
                $image = $_FILES['questionImage']['name'];
                $uniquequesimage = uniqidReal() . $image;
                $target = "../images/clinical/" . basename($uniquequesimage);
                $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
                $questionImage = $imageURL . $uniquequesimage;
                mysqli_query($dbconnection,"UPDATE `tbl_clinical_vignettes` SET `cv_qn_desc_images` = '$questionImage' WHERE `ref_id` = '$refID' ");
        
            }
        
            if (isset($_FILES['answerImage'])) { 
        
                $uniquequesimage = [];
        
                $total = count($_FILES['answerImage']['name']);
        
                for ($i = 0; $i < $total; $i++) 
                {
                    $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];
        
                    if ($tmpFilePath != "") 
                    {
                        $uniquedId = uniqidReal();
                        $uniquequesimage[] = $uniquedId . $_FILES['answerImage']['name'][$i];
                        $uniquequesimageURL[] = $imageURL . $uniquedId . $_FILES['answerImage']['name'][$i];
                        $newFilePath = "../images/clinical/" . $uniquequesimage[$i]; 
                        
                        if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                            $answerImage = implode(',', $uniquequesimageURL);
                        }
                    }
                }
                if(!empty($_FILES['answerImage']['name'][0])){  
                    mysqli_query($dbconnection,"UPDATE `tbl_clinical_vignettes` SET `cv_answer_desc_images` = '$answerImage' WHERE `ref_id` = '$refID' ");
    
                }
            }

            break; 
    }
         
         

        $query = "UPDATE `$tableName` SET  `$question_col` = ? , `$option_col` = ?,`$correctAnsnum_col` = ?,`$answerExp_col` = ? ,`$answerref` = ? WHERE `ref_id` = ? "; 

        $stmt = $dbconnection->prepare($query) or die($dbconnection->error);
    
          
        $stmt->bind_param("ssssss",$question,$finalAns,$correctoption,$answerExplanation,$answerRef,$refID) or die($dbconnection->error);
        
        $result = $stmt->execute() or die($dbconnection->error);
        if($result){  
            $_SESSION['editSuccess']="Questions Updated Successfully"; 
            header("location:../search.php?q=$refID"); 
            exit(); 
        }else{ 
            $_SESSION['editError']="Data Updated Error"; 
            header("location:../student-list.php");
            exit(); 
        }
        
        $stmt->close();
   
        

}
