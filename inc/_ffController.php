<?php
include('dbConfig.php');
include('functions.php'); 
session_start();

if (isset($_POST['addFFQuestion'])) { 
    
    $question = $_POST['Question'];   
    $option1 = $_POST['option1'];
    $option2 = $_POST['option2'];
    $option3 = $_POST['option3'];
    $option4 = $_POST['option4'];
    $correctoption = $_POST['correctoption'];
    $answerExplanation = $_POST['answerExplanation'];
     
    $questionImage = null; 
    $answerImage = null; 
  
    if (isset($_POST['questionImage'])){ $questionImage = $_POST['questionImage']; }
    if (isset($_POST['answerImage'])){ $answerImage = $_POST['answerImage']; }

    $arr = array($option1, $option2, $option3, $option4); 
    
    $finalAns = implode("$&", $arr);  
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/fastestFinger/'; 
     
    
    if (!empty($_FILES['questionImage']['name'])) {
        $image = ''; 
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/fastestFinger/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
    }

    if (!empty($_FILES['answerImage']['name'])) {

        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) {
            
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL.'/'.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                $newFilePath = "../images/fastestFinger/".$uniquequesimage[$i]; 
                if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                {
                    $answerImage = implode(',', $uniquequesimageURL);  
                }
            }
        }
    } 

    $query = "INSERT INTO `tbl_ff_qa`(`ff_qn_desc`,`ff_answers`, `ff_qn_correct_answer_num`,`ff_answer_desc`,`ff_qn_desc_images`,`ff_answer_desc_images`) VALUES ( ? , ? , ? , ? , ? , ?)";

    $stmt = $dbconnection->prepare($query);


    $stmt->bind_param("ssssss", $question, $finalAns, $correctoption, $answerExplanation, $questionImage, $answerImage );

    $result = $stmt->execute();
    if ($result) {   
        $_SESSION['FFquestionSuccess'] = "Question Added Successfully";
        header("location:../ff-quiz.php");
        exit();
    } else { 
        $_SESSION['FFquestionError'] = "Question Update Failed";
        header("location:../ff-quiz.php");
        exit();
    } 
    $stmt->close();
}
if (isset($_POST['editFFQuestion'])) 
{
    $questionId = $_POST['questionId']; 
    $question = $_POST['Question'];  
    $option1 = $_POST['option1']; 
    $option2 = $_POST['option2']; 
    $option3 = $_POST['option3']; 
    $option4 = $_POST['option4']; 
    $correctoption = $_POST['correctoption']; 
    $answerExplanation = $_POST['answerExplanation'];  
    $questionImage = null; 
    $answerImage = null; 
  
 

    $arr = array($option1,$option2,$option3,$option4);
    $finalAns = implode("$&",$arr); 

    
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/fastestFinger/';  
 
    if (!empty($_FILES['questionImage']['name'])) {
        $image = ''; 
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/fastestFinger/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
        mysqli_query($dbconnection,"UPDATE `tbl_ff_qa` SET `ff_qn_desc_images` = '$questionImage' WHERE `ff_qn_id` = '$questionId' ");
    }
    
        if (isset($_FILES['answerImage'])) { 
        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) {
            
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL.'/'.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                $newFilePath = "../images/fastestFinger/".$uniquequesimage[$i]; 
                if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                {
                    if(!empty($_FILES['answerImage']['name'][$i])){
                        $answerImage = implode(',', $uniquequesimageURL);   
                    }
                }
            }
        }
        if(!empty($_FILES['answerImage']['name'][0])){ 
            mysqli_query($dbconnection,"UPDATE `tbl_ff_qa` SET `ff_answer_desc_images` = '$answerImage' WHERE `ff_qn_id` = '$questionId' ");

        } 
    }  
    $query = "UPDATE `tbl_ff_qa` SET `ff_qn_desc` = ? , `ff_answers` = ?,`ff_qn_correct_answer_num` = ?, `ff_answer_desc` = ?  WHERE `ff_qn_id` = ? "; 
        $stmt = $dbconnection->prepare($query);
        $stmt->bind_param("sssss",$question,$finalAns,$correctoption,$answerExplanation,$questionId);
        
        $result = $stmt->execute();
        if($result){  
            $_SESSION['FFquestionSuccess']="Question Update Successfully"; 
            header('location:../ff-quiz.php');
            exit(); 
        }else{ 
            $_SESSION['FFquestionError']="Question Update Failed";  
            header('location:../ff-quiz.php'); 
            exit(); 
        }
        
        $stmt->close();
}

if(isset($_POST['send_question']))
{
            $currentTime = date('Y-m-d H:i:s');
            $qid = mysqli_real_escape_string($dbconnection,$_POST['questionId']); 

            $result = false;

            $sql = "UPDATE `tbl_ff_qa` SET `ff_status` = '0',`ff_sendTime` = '$currentTime'  WHERE `ff_qn_id` = $qid";
            $execute = mysqli_query($dbconnection,$sql);
            if($execute){ 
                $selectQuestion  = "SELECT * FROM `tbl_ff_qa` WHERE `ff_qn_id` = $qid";
                $executeQuestion = mysqli_query($dbconnection,$selectQuestion);
                 if($resultQuestion  = mysqli_fetch_array($executeQuestion)){  
                    $questions = $resultQuestion['ff_qn_desc'];
                    $options = $resultQuestion['ff_answers'];
                    $ffqnid = $resultQuestion['ff_qn_id'];
                    $questionImage = $resultQuestion['ff_qn_desc_images'];
                    $answerImage = $resultQuestion['ff_answer_desc_images']; 
                    $correntAnswer = $resultQuestion['ff_qn_correct_answer_num']; 
                 }   
            $sql = "SELECT * FROM `user` WHERE `fcm_token` IS NOT NULL AND `fcm_token`<>''";
            $executesql = mysqli_query($dbconnection,$sql);
            $arr = array();
                if(mysqli_num_rows($executesql) > 0)
                {
                    while($row = mysqli_fetch_array($executesql))
                    {   
                          
                            $arr[] = $row[ 'fcm_token' ];
                            $result = true; 
                      
                    }
                     
                    echo sendQuiz($arr,$questions,$options,$questionImage,$answerImage,$ffqnid,$correntAnswer);  
                    
                    
                    // if($result) {   
                    //     $Eqid = encryptData($qid);
                    //     $_SESSION['pushnotificationSuccess'] = "Notification Sent Successfully";
                    //     header("location:../view-ff-results.php?qid=$Eqid");
                    //     exit();
                    // } else { 
                    //     $_SESSION['pushnotificationError'] = "Notification Failed";
                    //     header("location:../view-ff-results.php?qid=$Eqid");
                    //     exit();
                    // } 
                } 
            }
}