<?php
include('./dbConfig.php');
include('functions.php');
session_start(); 
if(isset($_POST["addNewFAQ"]))
{ 
        $faqQuestion =  $_POST["faqQuestion"]; 
        $faqAns = $_POST["faqAns"];    
        $status = $_POST["faqStatus"];    

        $query = "INSERT INTO `tbl_FAQs` (`FAQ_questions`,`FAQ_Answer`,`status`) VALUES (?,?,?)"; 

        $stmt = $dbconnection->prepare($query);


        $stmt->bind_param("sss", $faqQuestion, $faqAns, $status );

        $result = $stmt->execute(); 

        if ($result) {   
            $_SESSION['faqSuccess'] = "New FAQ Added successfully";
            header("location:../faq-list.php");
            exit();
        } else {
            $_SESSION['faqError'] = "Data Update Error";
            header("location:../faq-list.php");
            exit();
        }

        $stmt->close();
}
if(isset($_POST["editFAQ"]))
{ 
        $faqQuestion =  $_POST["faqQuestion"]; 
        $faqid =  $_POST["FAQ_id"]; 
        $faqAns = $_POST["faqAns"];  
        $status = $_POST["faqStatus"];   
        $query = "UPDATE `tbl_FAQs` SET   `FAQ_questions` = ?,FAQ_Answer = ?,`status` = ? WHERE `FAQ_id`='$faqid'";  
        $stmt = $dbconnection->prepare($query); 
        $stmt->bind_param("sss",  $faqQuestion, $faqAns ,$status );

        $result = $stmt->execute(); 

        if ($result) {   
            $_SESSION['faqSuccess'] = "FAQ Status Updated";
            header("location:../faq-list.php");
            exit();
        } else {
            $_SESSION['faqError'] = "Data Update Error";
            header("location:../faq-list.php"); 
            exit();
        }

        $stmt->close();
}

if(isset($_POST['deleteQuestion']))
{
    $questionId  = $_POST['questionID'];

    $delete = "DELETE FROM  `tbl_faqs` WHERE  `FAQ_id` ='$questionId'";
    $stmt = $dbconnection->prepare($delete); 
    $result = $stmt->execute();

    if($result)
    {
        echo's';
    }else
    {
        echo'f';
    }
}