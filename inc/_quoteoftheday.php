<?php
include('dbConfig.php'); 
include('functions.php');
session_start();
$imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/quoteoftheday/';

if (isset($_POST['insertquoteoftheday'])) { 

    $quote = $_POST['quote'];   
    
    $quoteBG = array('https://www.ardentmds.com/api1/image/Quote%20of%20the%20Day/Quote%201.png','https://www.ardentmds.com/api1/image/Quote%20of%20the%20Day/Quote%202.png','https://www.ardentmds.com/api1/image/Quote%20of%20the%20Day/Quote%203.png','https://www.ardentmds.com/api1/image/Quote%20of%20the%20Day/Quote%204.png','https://www.ardentmds.com/api1/image/Quote%20of%20the%20Day/Quote%205.png');
 
    $finalQuoteBG =  $quoteBG[array_rand($quoteBG)];

     
    
    if (!empty($_FILES['questionImage']['name'])) {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/quoteoftheday/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
        $finalQuoteBG = $imageURL . $uniquequesimage;
    }
     

    $query = "INSERT INTO `quote_of_the_day`(`Quote`,`Quote_bg_image`) VALUES ( ? , ? )"; 
    $stmt = $dbconnection->prepare($query);
    $stmt->bind_param("ss", $quote,$finalQuoteBG);
    $result = $stmt->execute(); 

    if ($result){  
        $_SESSION['quotelistsuccess'] = "Quote Added Successfully ";
        header("location:../quote-list.php");
        exit(); 
    } else { 
        $_SESSION['quotelisterror'] = "Data Update Error"; 
        header("location:../quote-list.php");
        exit();
    }

    $stmt->close();
}
if (isset($_POST['updatequoteoftheday'])) {
    
    $quote = $_POST['quote']; 
    $quoteId = $_POST['quoteId'];  


    if (!empty($_FILES['questionImage']['name'])) 
    {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/quoteoftheday/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
        mysqli_query($dbconnection,"UPDATE `quote_of_the_day` SET `Quote_bg_image` = '$questionImage' WHERE `sl_id` ='$quoteId'"); 

    }

    $query = "UPDATE  `quote_of_the_day` SET `Quote` = ? WHERE `sl_id`= ?"; 
    $stmt = $dbconnection->prepare($query) or die($dbconnection->error); 
    $stmt->bind_param("ss", $quote ,$quoteId ); 
    $result = $stmt->execute() or die($dbconnection->error);
    
     
    
    if ($result) {  
        $_SESSION['quotelistsuccess'] = "Quote Updated Successfully ";
        // header("location:../quote-list.php");
        exit();
    } else {

        $_SESSION['quotelisterror'] = "Quote Updated Successfully ";
        // header("location:../quote-list.php");
        exit();
    }

    $stmt->close();
}
if(isset($_POST['deleteQuestion']))
{
    $questionId  = $_POST['questionID'];

    $delete = "DELETE FROM  `quote_of_the_day` WHERE  `sl_id` ='$questionId'";
    $stmt = $dbconnection->prepare($delete); 
    $result = $stmt->execute();

    if($result)
    {
        echo 'success';
    }else
    {
        echo 'fail';
    }
    $stmt->close();
}
