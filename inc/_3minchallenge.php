    <?php
include('dbConfig.php');
include('functions.php');
  
session_start();

if (isset($_POST['add3minQuestion'])) 
{
    $question = $_POST['3minQuestion'];
    $examDate = $_POST['3minChallengeDate'];
    $option1 = $_POST['3minoption1'];
    $option2 = $_POST['3minoption2'];
    $option3 = $_POST['3minoption3'];
    $option4 = $_POST['3minoption4'];
    $correctoption = $_POST['3mincorrectoption'];
    $answerExplanation = $_POST['3minanswerExplanation'];
    $answerReferece = $_POST['answerReferece'];
    $questionImage = null;
    $answerImage = null;
    $testName  = "Nuggets Test 1 (From Q-Bank)";

    $testId  = checkAndReturnTestId($examDate,$dbconnection);

    if (isset($_POST['3minquestionImage'])) {
        $questionImage = $_POST['3minquestionImage'];
    }
    if (isset($_POST['answerImage'])) {
        $answerImage = $_POST['answerImage'];
    }

    $arr = array($option1, $option2, $option3, $option4);
    $finalAns = implode("$&", $arr);
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/3minchallenge/';



    if (!empty($_FILES['3minquestionImage']['name'])) {
        $image = '';
        $image = $_FILES['3minquestionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/3minchallenge/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['3minquestionImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
    }

    if (!empty($_FILES['answerImage']['name'])) 
    { 
        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) 
        {
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId . $_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL . $uniquedId . $_FILES['answerImage']['name'][$i];
                $newFilePath = "../images/3minchallenge/" . $uniquequesimage[$i];

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $answerImage = implode(',', $uniquequesimageURL);
                }
            }
        }
    }




    $query = "INSERT INTO `tbl_nuggets_qa`(`nuggets_testId`,`nuggets_test__name`,`test_date`,`nuggets_qn_desc`, `nuggets_answers`,`nuggets_qn_correct_answer_num`,`nuggets_answer_desc` ,`nuggets_ans_ref`,`nuggets_qn_desc_images`,`nuggets_answer_desc_images`) VALUES ( ? , ? , ? , ? , ? , ? , ? , ?, ? , ?)";

    $stmt = $dbconnection->prepare($query);


    $stmt->bind_param("ssssssssss",$testId,$testName, $examDate, $question, $finalAns, $correctoption, $answerExplanation ,$answerReferece , $questionImage, $answerImage);

    $result = $stmt->execute();
    
    if ($result) {
        $last_id = $dbconnection->insert_id;
        $update = "UPDATE tbl_nuggets_qa SET `ref_id`= CONCAT_WS('','NT', `nuggets_testId` , 'Q', `nuggets_qn_id`) WHERE `nuggets_qn_id`='$last_id'";
        mysqli_query($dbconnection, $update);
        $_SESSION['ntquestionaddsuccess'] = "Question Added Successfully";

        $encrypted = encryptData($examDate);

        header("location:../add-3minedited.php?testDate=$encrypted");
        exit();
    } else {
        $_SESSION['ntquestioaddnerror'] = "Data Update Error";
        header("location:../3-min-challenges.php");
        exit();
    }

    $stmt->close();
}

if(isset($_POST['editNTQuestion']))
{ 
    $questionId = $_POST['questionId'];  
    $testDate = $_POST['testDate'];  
    $question = $_POST['Question'];  
    $option1 = $_POST['option1']; 
    $option2 = $_POST['option2']; 
    $option3 = $_POST['option3']; 
    $option4 = $_POST['option4']; 
    $correctoption = $_POST['correctoption']; 
    $answerExplanation = $_POST['answerExplanation']; 
    $mcqReference = $_POST['answerReferece']; 
    $questionImage = null; 
    $answerImage = null; 
   

    $arr = array($option1,$option2,$option3,$option4);
    $finalAns = implode("$&",$arr);  
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/3minchallenge/'; 
   
    if (!empty($_FILES['questionImage']['name'])) { 
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/3minchallenge/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
        mysqli_query($dbconnection,"UPDATE `tbl_nuggets_qa` SET `nuggets_qn_desc_images` = '$questionImage' WHERE `nuggets_qn_id` = '$questionId' ");
    }
    
        if (isset($_FILES['answerImage'])) { 
        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) 
        {
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId . $_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL . $uniquedId . $_FILES['answerImage']['name'][$i];
                $newFilePath = "../images/3minchallenge/" . $uniquequesimage[$i];

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $answerImage = implode(',', $uniquequesimageURL);
                }
            }
        }
        if(!empty($_FILES['answerImage']['name'][0])){ 
            mysqli_query($dbconnection,"UPDATE `tbl_nuggets_qa` SET `nuggets_answer_desc_images` = '$answerImage' WHERE `nuggets_qn_id` = '$questionId' ");

        }
        
    }  
    $query = "UPDATE `tbl_nuggets_qa` SET `nuggets_qn_desc` = ? , `nuggets_answers` = ?,`nuggets_qn_correct_answer_num` = ?, `nuggets_answer_desc` = ?,`nuggets_ans_ref` = ? WHERE `nuggets_qn_id` = ? "; 
        $stmt = $dbconnection->prepare($query);
        $stmt->bind_param("ssssss",$question,$finalAns,$correctoption,$answerExplanation,$mcqReference,$questionId) or die($dbconnection->error);
        
        $result = $stmt->execute();
        if($result){  
            $_SESSION['ntquestionsuccess']="Question Update Successfully";  
            $EtestId = encryptData($testDate);
            header("location:../3-min-questions-list.php?testDate=$EtestId");
            exit(); 
        }else{ 
            $_SESSION['ntquestionerror']="Question Update Failed";  
            header("location:../3-min-questions-list.php?testDate=$EtestId"); 
            exit(); 
        }
        
        $stmt->close();
}



if (isset($_POST['addSelectedNtQuestion'])) {  
    $examDate = $_POST['3minChallengeDate'];  
    $mainModule = $_POST['mainsModule'];   

    $testId  = checkAndReturnTestId($examDate,$dbconnection);
    $testName  = "Nuggets Test 1 (From Q-Bank)";
    $checked = $_POST['checkbox'];
    $success = true;
    $currentQnCount = nuggetsQuestionCount($examDate,$dbconnection); 
    $conditionvalue  = 10 - $currentQnCount; 
        for ($i=0; $i < $conditionvalue; $i++) {   
        $select = mysqli_query($dbconnection,"SELECT * FROM `qa` WHERE `mcq_qn_id`='$checked[$i]'"); 
        if($row = mysqli_fetch_array($select)){
            $question = $row['mcq_qn_desc']; 
            $finalAns = $row['mcq_answers'];
            $correctoption = $row['mcq_qn_correct_answer_num'];
            $answerExplanation = $row['mcq_answer_desc'];
            $answerReferece  = $row["mcq_reference"];
            $questionImage  = $row["mcq_qn_desc_images"];
            $answerImage  = $row["mcq_answer_desc_images"];
            $mcqchildid = null;  

            $query = "INSERT INTO `tbl_nuggets_qa`(`nuggets_testId`,`nuggets_test__name`,`test_date`,`nuggets_qn_desc`, `nuggets_answers`,`nuggets_qn_correct_answer_num`,`nuggets_answer_desc` ,`nuggets_ans_ref`,`nuggets_qn_desc_images`,`nuggets_answer_desc_images`) VALUES ( ? , ? , ? , ? , ? , ? , ? , ?, ? , ?)" or die($dbconnection->error) ;
            $stmt = $dbconnection->prepare($query) or die($dbconnection->error);
            $stmt->bind_param("ssssssssss",$testId,$testName, $examDate, $question, $finalAns, $correctoption, $answerExplanation ,$answerReferece , $questionImage, $answerImage) or die($dbconnection->error);
            $result = $stmt->execute() or die($dbconnection->error);
            if($result){
                $last_id = $dbconnection->insert_id;
                $update = "UPDATE tbl_nuggets_qa SET `ref_id`= CONCAT_WS('','NT', `nuggets_testId` , 'Q', `nuggets_qn_id`) WHERE `nuggets_qn_id`='$last_id'";
                mysqli_query($dbconnection, $update);  
                $success= true;
            }
           
        }
    }


    if($success){
        $_SESSION['gtquestionsuccess']="Question Added Successfully";  
        echo "Question Added Successfully";  
        $EtestId = encryptData($examDate);
        
        header("location:../3-min-questions-list.php?testDate=$EtestId");
        exit(); 
    }
}

