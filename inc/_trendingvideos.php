<?php
include('dbConfig.php');
include('functions.php'); 
session_start();

if (isset($_POST['edittrendingvideos'])) {
    
    $tutor_Id = $_POST['chooseTutor']; 
    $uniqueId = $_POST['videoId'];   
    $videoUrl = $_POST['videoUrl'];  


        for ($i=0; $i < count($_POST['videoTimestamp']); $i++) { 
            $arr[]  =  $_POST['videoTimestamp'][$i].'-'.$_POST['videoTimestampText'][$i]; 
            $finalTimeStamp  = implode(',',$arr);
        }
         
 
        $query = "UPDATE  `trending_videos` SET `trendingvideo_url` = ? , `tutorId` = ?,`trendingvideo_title_timestamp` = ? WHERE `id`= ?";

        $stmt = $dbconnection->prepare($query);
   
   
        $stmt->bind_param("ssss",$videoUrl,$tutor_Id,$finalTimeStamp, $uniqueId);
       $result = $stmt->execute();
    
    
    if ($result) {  
        $_SESSION['trendingsuccess'] = "Updated Successfully ";
        header("location:../trending-videos.php");
        exit();
    } else {

        $_SESSION['trendingfail'] = "Data Update Error";
        header("location:../trending-videos.php");
        exit();
    }

    $stmt->close();
}