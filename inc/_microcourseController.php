<?php
include('./dbConfig.php');
include('functions.php');
session_start(); 
$imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/microcourses/';
 

if (isset($_POST['editMicroCourse'])) {
     
    $uniqueId = $_POST['videoId'];  
    $chooseTutor = $_POST['chooseTutor']; 
    $videoName = $_POST['videoName']; 
    $video_cost  = $_POST['video_cost']; 
    $videoLength = $_POST['videoLength']; 
    $course_topics = $_POST['course_topics']; 
    $whats_inside = $_POST['whats_inside'];  
    $course_description = $_POST['course_description']; 
    $expireson = $_POST['examDate']; 
    $video_status = $_POST['video_status']; 

    if (substr($videoLength, -1) == ':') 
    {  
        $videoLength = substr_replace($videoLength, "", -1);  
    }

    if (!empty($_FILES['questionImage']['name'])) 
    {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/microcourses/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
        mysqli_query($dbconnection,"UPDATE `tbl_microcourses` SET `video_front_image` = '$questionImage' WHERE `id` ='$uniqueId'"); 

    }


        for ($i=0; $i < count($_POST['videoTimestamp']); $i++) { 
             
            if (substr($_POST['videoTimestamp'][$i], -1) == ':') 
            {  
                $_POST['videoTimestamp'][$i] = substr_replace($_POST['videoTimestamp'][$i], "", -1);  
            }

            $arr[]  =  $_POST['videoTimestamp'][$i].'-'.$_POST['videoTimestampText'][$i]; 
            $finalTimeStamp  = implode(',',$arr);
        } 
         

     
        $querys = "UPDATE `tbl_microcourses` SET  `vedio_sm_name`= ? , `vedio_length`= ? ,`tutorid`= ? , `video_title_timestamp`= ? ,`course_topics`= ? ,`whats_inside`= ? ,`course_description`= ? , `video_cost`= ? , `expires_on`=  ? ,`status`= ?  WHERE `id`= ?" or die($dbconnection->error) ;
   
    

        $stmts = $dbconnection->prepare($querys) or die($dbconnection->error) ;
    
        $stmts->bind_param("sssssssssss",$videoName,$videoLength,$chooseTutor,$finalTimeStamp,$course_topics,$whats_inside,$course_description,$video_cost,$expireson,$video_status,$uniqueId) or die($dbconnection->error) ;

        $result = $stmts->execute() or die($dbconnection->error) ;
      
     
    
    if ($result) {   
        $_SESSION['newvideoMicroSuccess'] = "Micro Course Updated  ";  
        header("location:../microcourses-list.php");
        exit();
    } else { 
        $_SESSION['newvideoMicroError'] = "Data Update Error";
        header("location:../microcourses-list.php");
        exit();
    }

    $stmt->close();
}
 
if (isset($_POST['addMicroCourse'])) {
     
    $uniqueId = $_POST['videoId'];  
    $chooseTutor = $_POST['chooseTutor']; 
    $videoName = $_POST['videoName']; 
    $video_cost  = $_POST['video_cost']; 
    $videoLength = $_POST['videoLength']; 
    $course_topics = $_POST['course_topics']; 
    $whats_inside = $_POST['whats_inside'];  
    $course_description = $_POST['course_description']; 
    $expireson = $_POST['examDate']; 
    $video_status = $_POST['video_status']; 

    if (substr($videoLength, -1) == ':') 
    {  
        $videoLength = substr_replace($videoLength, "", -1);  
    }

    if (!empty($_FILES['questionImage']['name'])) 
    {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/microcourses/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
        mysqli_query($dbconnection,"UPDATE `tbl_microcourses` SET `video_front_image` = '$questionImage' WHERE `id` ='$uniqueId'"); 

    }


        for ($i=0; $i < count($_POST['videoTimestamp']); $i++) { 
             
            if (substr($_POST['videoTimestamp'][$i], -1) == ':') 
            {  
                $_POST['videoTimestamp'][$i] = substr_replace($_POST['videoTimestamp'][$i], "", -1);  
            }

            $arr[]  =  $_POST['videoTimestamp'][$i].'-'.$_POST['videoTimestampText'][$i]; 
            $finalTimeStamp  = implode(',',$arr);
        } 
         

     
        $querys = "INSERT INTO  `tbl_microcourses`(`vedio_sm_name`, `vedio_length` ,`tutorid` , `video_title_timestamp` ,`course_topics` ,`whats_inside` ,`course_description` , `video_cost`  , `expires_on` ,`status`) VALUES( ? , ? ,? , ? ,? , ? ,? , ? ,? , ? )";
   
    

        $stmts = $dbconnection->prepare($querys) or die($dbconnection->error) ;
    
        $stmts->bind_param("ssssssssss",$videoName,$videoLength,$chooseTutor,$finalTimeStamp,$course_topics,$whats_inside,$course_description,$video_cost,$expireson,$video_status) or die($dbconnection->error) ;

        $result = $stmts->execute() or die($dbconnection->error) ;
      
     
    
    if ($result) {   
        $_SESSION['newvideoMicroSuccess'] = "Micro Course Added Successfully  ";  
        header("location:../microcourses-list.php");
        exit();
    } else { 
        $_SESSION['newvideoMicroError'] = "Data Update Error";
        header("location:../microcourses-list.php");
        exit();
    }

    $stmt->close();
}