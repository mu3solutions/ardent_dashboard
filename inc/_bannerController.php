<?php
include('./dbConfig.php');
include('functions.php');
session_start(); 
if(isset($_POST["addNewBanner"]))
{ 
        $bannerlink =  $_POST["bannerLink"]; 
        $bannerStatus = $_POST["bannerStatus"]; 
        $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/topSlider/'; 
     
    
        if (!empty($_FILES['questionImage']['name'])) {
            $image = ''; 
            $image = $_FILES['questionImage']['name'];
            $uniquequesimage = uniqidReal() . $image;
            $target = "../images/topSlider/" . basename($uniquequesimage);
            $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
            $questionImage = $imageURL . $uniquequesimage;
        }

         
        $query = "INSERT INTO `topbanner` (`bannerImage`,`banner_link`,`status`) VALUES (?,?,?)"; 

        $stmt = $dbconnection->prepare($query);


        $stmt->bind_param("sss", $questionImage, $bannerlink, $bannerStatus );

        $result = $stmt->execute(); 

        if ($result) {   
            $_SESSION['bannerSuccess'] = "New Slider Image Uploaded successfully";
            header("location:../top-banner-list.php");
            exit();
        } else {
            $_SESSION['bannerError'] = "Data Update Error";
            header("location:../top-banner-list.php");
            exit();
        }

        $stmt->close();
}
if(isset($_POST["editBanner"]))
{ 
        $bannerlink =  $_POST["bannerLink"]; 
        $bannerid =  $_POST["bannerId"]; 
        $bannerStatus = $_POST["bannerStatus"];  

         

        $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/topSlider/';  
 
        if (!empty($_FILES['questionImage']['name'])) {
            $image = ''; 
            $image = $_FILES['questionImage']['name'];
            $uniquequesimage = uniqidReal() . $image;
            $target = "../images/topSlider/" . basename($uniquequesimage);
            $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
            $questionImage = $imageURL . $uniquequesimage;
            mysqli_query($dbconnection,"UPDATE `topbanner` SET `bannerImage` = '$questionImage' WHERE `bId` = '$bannerid' ");
        }
    

         
        $query = "UPDATE `topbanner` SET   `banner_link` = ? ,`status` = ? WHERE `bId`='$bannerid'"; 

        $stmt = $dbconnection->prepare($query);


        $stmt->bind_param("ss",  $bannerlink, $bannerStatus );

        $result = $stmt->execute(); 

        if ($result) {   
            $_SESSION['bannerSuccess'] = "Slider Image Updated";
            header("location:../top-banner-list.php");
            exit();
        } else {
            $_SESSION['bannerError'] = "Data Update Error";
            header("location:../top-banner-list.php"); 
            exit();
        }

        $stmt->close();
}