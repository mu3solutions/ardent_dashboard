<?php
include('./dbConfig.php');
include('functions.php');
session_start();
$sorting_order = "";
if(isset($_POST["addNewvideoModule"]))
{  
        $mainModuleName =  $_POST["videoModuleName"];
        $videoModuleDesc = $_POST["videoModuleDesc"];
        $setSubModuleCount = $_POST["setSubModuleCount"];
        $status = $_POST["status"];
        // $mcqIcon="https://www.ardentmds.com/api1/image/mcq_mod1.png";
        $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/videos/'; 
        $sorting_order = getMaxRecord('vedio_module','sort_order',$dbconnection) + 1;
        
          if (!empty($_FILES['questionImage']['name'])) {
            $image = ''; 
            $image = $_FILES['questionImage']['name'];
            $uniquequesimage = uniqidReal() . $image;
            $target = "../images/videos/" . basename($uniquequesimage);
            $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
            $mcqIcon = $imageURL . $uniquequesimage;
        } 
        
        $query = "INSERT INTO `vedio_module` (`mvname`,`mvdesc`,`sort_order`,`mv_total_sub_modules`,`mvicons`,`mv_status`) VALUES (?,?,?,?,?,?)";

        $stmt = $dbconnection->prepare($query);


        $stmt->bind_param("ssssss", $mainModuleName, $videoModuleDesc, $sorting_order , $setSubModuleCount,  $mcqIcon,$status);

        $result = $stmt->execute();

        if ($result) {  
            mysqli_query($dbconnection,"UPDATE `vedio_module` SET `mv_total_sub_modules` =(SELECT COUNT(*) FROM `vedio_sub_module` WHERE `vedio_sub_module`.`vedio_module_id_ref` = `vedio_module`.`mvid`)");
            $_SESSION['newvideoModulesuccess'] = "New Video Module Created successfully"; 
            header("location:../videos-module-list.php");
            exit();
        } else {
            $_SESSION['newvideoModulerror'] = "Data Update Error";
            header("location:../mcq-module-list.php");
            exit();
        }

        $stmt->close();
}
if(isset($_POST["EditNewvideoModule"]))
{
    $mcqId = $_POST["uniqueId"];
    $mainModuleName =  $_POST["videoModuleName"];
    $mainModuleDesc = $_POST["videoModuleDesc"]; 
    $status = $_POST["status"];
    $setSubModuleCount = $_POST["setSubModuleCount"];
    
    $query = "UPDATE `vedio_module`  SET `mvname` =  ? ,`mvdesc`  = ? , `mv_status` = ?  , `mv_total_sub_modules` = ? WHERE `mvid` = ? " or die($dbconnection->error) ;
    
    $stmt = $dbconnection->prepare($query) or die($dbconnection->error) ;
    

    $stmt->bind_param("sssss", $mainModuleName, $mainModuleDesc, $status ,$setSubModuleCount, $mcqId ) or die($dbconnection->error) ;
    
    $result = $stmt->execute() or die($dbconnection->error) ;
    
    if ($result) {  
        $_SESSION['newvideoModulesuccess'] = "Video Module Updated successfully"; 
        header("location:../videos-module-list.php");
        exit();
    } else {
        $_SESSION['newvideoModulerror'] = "Data Update Error";
        header("location:../mcq-module-list.php");
        exit();
    }

        $stmt->close();
}
 

if (isset($_POST['editVideos'])) {
     
    $videoName = $_POST['videoName']; 
    $videoLength = $_POST['videoLength']; 
    $mainModule = $_POST['mainModule']; 
    $chooseTutor = $_POST['chooseTutor']; 
    $vedio_sm_access_level = $_POST['video_status']; 
    $video_status = $_POST['video_status']; 
    $status = $_POST['status']; 
    $uniqueId = $_POST['videoId'];  
     $videoUrl = $_POST['videoUrl'];
 
        for ($i=0; $i < count($_POST['videoTimestamp']); $i++) { 
             
            if (substr($_POST['videoTimestamp'][$i], -1) == ':') 
            {  
                $_POST['videoTimestamp'][$i] = substr_replace($_POST['videoTimestamp'][$i], "", -1);  
            }

            $arr[]  =  $_POST['videoTimestamp'][$i].'-'.$_POST['videoTimestampText'][$i]; 
            $finalTimeStamp  = implode(',',$arr);
        } 
         
 
        $querys = "UPDATE  `vedio_sub_module` SET `sm_videos_url` = ? ,`vedio_module_id_ref` = ? , `vedio_sm_child_name` = ? ,`video_title_timestamp` = ? , `vedio_length` = ?  , `vedio_sm_access_level` = ?  , `video_status` = ? ,`status` = ? , `tutorId` = ? WHERE `id`= ?" or die($dbconnection->error) ;

        $stmts = $dbconnection->prepare($querys);
    
        $stmts->bind_param("ssssssssss",$videoUrl,$mainModule,$videoName,$finalTimeStamp, $videoLength,$vedio_sm_access_level ,$video_status,$status,$chooseTutor ,$uniqueId);
        $result = $stmts->execute();
      
     
    
    if ($result) {   
        $_SESSION['mainVideosSuccess'] = "Video details Updated"; 
        header("location:../video-sub-module-list.php");
        exit();
    } else { 
        $_SESSION['mainVideosError'] = "Data Update Error";
        header("location:../video-sub-module-list.php");
        exit();
    }

    $stmt->close();
}
if (isset($_POST['newVideos'])) {
     
    $videoName = $_POST['videoName']; 
    $videoLength = $_POST['videoLength']; 
    $mainModule = $_POST['mainModule']; 
    $chooseTutor = $_POST['chooseTutor']; 
    $vedio_sm_access_level = $_POST['video_status']; 
    $video_status = $_POST['video_status'];
    $status = $_POST['status'];
    $vedio_sm_child_rating = 5;
    $video_sm_id =  $_POST['mainModule']; 
    $vedio_child_id = getMaxRecord('vedio_sub_module','id',$dbconnection) + 1; 
    $vedio_sm_name =  fetchData($dbconnection,'mvname','vedio_module',$mainModule,'mvid'); 
    $finalvideoURL = $_POST['videoUrl'];


    $vedio_sm_child_image = "https://www.pexels.com/photo/photo-of-skeleton-2678059/";
     
    $baseURL = "https://www.ardentmds.com/ardent-dashboard/videos/subjectVideos/";
    $imageBg = "https://app.mu3innovativesolutions.com/api1/image/videos_bg.png";
    


        for ($i=0; $i < count($_POST['videoTimestamp']); $i++) 
        {
            if (substr($_POST['videoTimestamp'][$i], -1) == ':') 
            {  
                $_POST['videoTimestamp'][$i] = substr_replace($_POST['videoTimestamp'][$i], "", -1);  
            }
            $arr[]  =  $_POST['videoTimestamp'][$i].'-'.$_POST['videoTimestampText'][$i]; 
            $finalTimeStamp  = implode(',',$arr);
        }
        
        $exequery =  mysqli_query($dbconnection,"SELECT * FROM `tutordetails` WHERE `tutorId`='$chooseTutor'");
        
        if($row = mysqli_fetch_array($exequery)){
            $tutorName =  $row['tutorName'];
            $tutorDesc =  $row['tutorDesc'];
            $tutorImage =  $row['tutorImage'];
        }
         
        
        $query = "INSERT INTO `vedio_sub_module`( `vedio_module_id_ref`,`vedio_sm_id` ,`vedio_sm_name` , `vedio_sm_child_id`, `vedio_sm_child_name` , `vedio_sm_child_desc` ,`vedio_sm_child_rating` , `vedio_sm_access_level`,`vedio_length`,`sm_videos_url`, `tutorId` ,`tutor_name`, `tutor_image`, `image_bg`, `video_title_timestamp` , `video_status`, `status`) 
        VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $stmt = $dbconnection->prepare($query);


        $stmt->bind_param("sssssssssssssssss", $mainModule,$video_sm_id,$vedio_sm_name, $vedio_child_id,$videoName, $tutorDesc ,$vedio_sm_child_rating,$vedio_sm_access_level,$videoLength,$finalvideoURL,$chooseTutor,$tutorName,$tutorImage,$imageBg,$finalTimeStamp,$video_status,$status);

        $result = $stmt->execute();
     
    if ($result) {    

        $_SESSION['mainVideosSuccess'] = "New Video Uploaded"; 
        header("location:../video-sub-module-list.php");
        exit();
    } else { 
        $_SESSION['mainVideosError'] = "Data Update Error";
        header("location:../video-sub-module-list.php");
        exit();
    }

    $stmt->close();
}