<?php

function getplanName($dbconnection , $planID){
    $string_occ =  strlen($planID);
     
    if($string_occ > 2) 
    { 
        $exeplan  = mysqli_query($dbconnection , "SELECT `plan_name` FROM `user_sub_plans` WHERE user_plan_type='$planID'");
        $row = mysqli_fetch_assoc($exeplan);
               if(!empty($row['plan_name'])){
                   return $row['plan_name'];
               }else{
                   return 0;
               }                                     
    }else {
        $exeplan  = mysqli_query($dbconnection , "SELECT `plan_name` FROM `user_plans` WHERE user_plan_type='$planID'");
        $row = mysqli_fetch_assoc($exeplan);
               if(!empty($row['plan_name'])){
                   return $row['plan_name'];
               }else{
                   return 0;
               }  
    }
   }

function truncate($text, $chars) {
    if (strlen($text) <= $chars) {
        return $text;
    }
    $text = $text." ";
    $text = substr($text,0,$chars);
    $text = substr($text,0,strrpos($text,' '));
    $text = $text."...";
    return $text;
}

function uniqidReal($lenght = 13) { 
    if (function_exists("random_bytes")) {
        $bytes = random_bytes(ceil($lenght / 2));
    } elseif (function_exists("openssl_random_pseudo_bytes")) {
        $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
    }  
    return substr(bin2hex($bytes), 0, $lenght);
}

function nuggetsQuestionCount($testDate,$dbconnection) { 
        $query = mysqli_query($dbconnection, "SELECT COUNT(*) As RowCount FROM `tbl_nuggets_qa` WHERE `test_date` = '$testDate'");
        $row = mysqli_fetch_assoc($query);
        $rowCount =  $row['RowCount'];  
        return $rowCount;
     
}

function checkAndReturnTestId($testDate,$dbconnection){
    $query = mysqli_query($dbconnection, "SELECT `nuggets_testId` FROM `tbl_nuggets_qa` WHERE `test_date` = '$testDate'");
    $row = mysqli_fetch_assoc($query);  
    if(mysqli_num_rows($query) > 0){
        $testid =  $row['nuggets_testId'];    
                return $testid;
    }
        else
        {
            $query1 = mysqli_query($dbconnection, "SELECT `nuggets_testId` FROM `tbl_nuggets_qa` ORDER BY `nuggets_qn_id` DESC LIMIT 1");
            $row2 = mysqli_fetch_assoc($query1);
            $testid2 =  $row2['nuggets_testId'];   
            return $testid2 + 1;
        }
     
}

function childNameUsingId($childid,$dbconnection) { 
    $query = mysqli_query($dbconnection, "SELECT `mcq_sm_child_name` FROM `mcq_sub_module` WHERE `mcq_sm_child_id` = '$childid'");
    $row = mysqli_fetch_array($query);
    if(mysqli_num_rows($query) > 0){

        $mcq_sm_child_name =  $row['mcq_sm_child_name'];   
        if(!empty($mcq_sm_child_name)){ 
            return $mcq_sm_child_name;
        }else{
            return 0;
        }
    }


 
}

function getMaxRecord($tableName,$columnName,$dbconnection) {  
    $query = mysqli_query($dbconnection,"SELECT MAX(`$columnName`) AS 'maxValue' FROM `$tableName` ");
    $row = mysqli_fetch_assoc($query); 
        return utf8_encode($row['maxValue']); 
}

function getMaxClinical($dbconnection) {  
    $query = mysqli_query($dbconnection,"SELECT cv_test_date FROM tbl_clinical_vignettes ORDER BY STR_TO_DATE(cv_test_date, '%d-%m-%Y') DESC LIMIT 1");
    $row = mysqli_fetch_array($query); 
        return  $row['cv_test_date']; 
}
function getMaxNuggets($dbconnection) {  
    $query = mysqli_query($dbconnection,"SELECT test_date , COUNT(test_date) as COUNTMAX FROM tbl_nuggets_qa GROUP BY STR_TO_DATE(test_date, '%d-%m-%Y') DESC LIMIT 1");
    $row = mysqli_fetch_array($query);  
        return array($row['test_date'], $row['COUNTMAX']);
}

function checkTodayClinical($dbconnection,$date) {  
    $query = mysqli_query($dbconnection,"SELECT * FROM tbl_clinical_vignettes WHERE cv_test_date='$date'");
    $row = mysqli_fetch_array($query); 
    if(mysqli_num_rows($query) > 0){ 
        return  $row['cv_test_date']; 
    } else{
        return 0;
    }
}

function checkTodayNuggets($dbconnection,$date) {  
    $query = mysqli_query($dbconnection,"SELECT COUNT(*) as countmax FROM tbl_nuggets_qa WHERE test_date='$date'");
    $row = mysqli_fetch_array($query); 
    if(mysqli_num_rows($query) > 0){ 
        return  $row['countmax']; 
    } else{
        return 0;
    }
}


function getMaxCount($tableName,$columnName,$dbconnection) {  
    $query = mysqli_query($dbconnection,"SELECT COUNT(*) AS 'rowCount' FROM `$tableName` ");
    $row = mysqli_fetch_array($query); 
    
    if(mysqli_num_rows($query) > 0){
        return  $row['rowCount'];
    } else{
        return 0;
    }
}

function getSubModuleID($mainModuleId,$dbconnection) {  
    $query = mysqli_query($dbconnection,"SELECT MAX(`mcq_sm_id`) AS 'maxValue' FROM `mcq_sub_module`  WHERE `mcq_module_id_ref` = '$mainModuleId' " );
    $row = mysqli_fetch_assoc($query); 
        return utf8_encode($row['maxValue']); 
}

 

function fetchData($dbconnection,$columnName,$tableName,$columnValue,$columnID)
{
       $query = mysqli_query($dbconnection, "SELECT `$columnName` FROM $tableName WHERE `$columnID`='$columnValue'");
        $row = mysqli_fetch_assoc($query);
        if(!empty($row[$columnName])){
            return $row[$columnName];
        }else{
            return 0;
        }
        
}
function fetchsubModuleData($dbconnection,$columnName,$tableName,$columnValue1,$columnID1,$columnValue2,$columnID2)
{
       $query = mysqli_query($dbconnection, "SELECT `$columnName` FROM $tableName WHERE `$columnID1`='$columnValue1' AND `$columnID2`='$columnValue2' ");
        $row = mysqli_fetch_assoc($query);
        if(!empty($row[$columnName])){
            return utf8_encode($row[$columnName]);
        }else{
            return 0;
        }
        
}

function checkGTQuestionsCount($testId,$dbconnection) { 
    $query = mysqli_query($dbconnection, "SELECT COUNT(*) AS maxCount FROM `qa_grand_test` WHERE `testId` = '$testId' AND `status`=1");
    $row = mysqli_fetch_assoc($query);
    $count =  $row['maxCount'];   
    return $count;
 
}

function IND_money_format($number)
{
    $decimal = (string)($number - floor($number));
    $money = floor($number);
    $length = strlen($money);
    $delimiter = '';
    $money = strrev($money);

    for($i=0;$i<$length;$i++){
        if(( $i==3 || ($i>3 && ($i-1)%2==0) )&& $i!=$length){
            $delimiter .=',';
        }
        $delimiter .=$money[$i];
    }

    $result = strrev($delimiter);
    $decimal = preg_replace("/0\./i", ".", $decimal);
    $decimal = substr($decimal, 0, 3);

    if( $decimal != '0'){
        $result = $result.$decimal;
    }

    return $result;
}


function sendMCQ($key,$notificationTitle,$notificationMessage,$imageUrl,$userName){ 


    $path_to_fcm = "https://fcm.googleapis.com/fcm/send";
    $server_key = "AAAAX_HF8_I:APA91bHjKyBknrvdiWY-RHYcM8YXmwhReaceaqyWDSa4e57hUOfV0Jbul93GPxYxRtZ-6Hw1330I_9wsTE9pC_180MDF3nzM6SnpAvtwRGzcjDQPR_5L2PiFYPq042Z-gBzZayijfi5-"; 
      
      $messageBody = 'Dear '.$userName.','."\r\n".$notificationMessage;
      
    $headers = array(
    "Authorization:key=" .$server_key,
    "Content-Type:application/json"
    );
    
    
    $fields = array(
                    'to'=>$key,
                    'priority' => 'high', 
                    'notification'=>array(
                        'title'=>$notificationTitle,
                        'body'=>$messageBody,
                        'image'=>$imageUrl
                        ) 
                );
    
    $payload = json_encode($fields); 
     
    $curl_session = curl_init();
    curl_setopt($curl_session, CURLOPT_URL, $path_to_fcm);
    curl_setopt($curl_session, CURLOPT_POST, true);
    curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl_session, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt($curl_session, CURLOPT_POSTFIELDS, $payload); 
    $result = curl_exec($curl_session);
    curl_close($curl_session);   
}

function sendQuiz($key,$question,$answers,$questionImage,$answerImage,$ffqnid,$correntAnswer)
{
 
    $path_to_fcm = "https://fcm.googleapis.com/fcm/send";
    $server_key = "AAAAX_HF8_I:APA91bHjKyBknrvdiWY-RHYcM8YXmwhReaceaqyWDSa4e57hUOfV0Jbul93GPxYxRtZ-6Hw1330I_9wsTE9pC_180MDF3nzM6SnpAvtwRGzcjDQPR_5L2PiFYPq042Z-gBzZayijfi5-"; 
    $test_time = 30;
    $id = mt_rand(100000, 999999);
    
    $headers = array(
    "Authorization:key=" .$server_key,
    "Content-Type:application/json"
    );


    $fields = array(
                    'registration_ids'=>$key,
                    'priority' => 'high', 
                    'data'=>array("id" => $id ,"question"=>$question,"questionId"=>$ffqnid,"answers"=>$answers,"correct_answer"=>$correntAnswer,"answerImage" =>$answerImage,"questionImage" => $questionImage
                    ,"test_time" => $test_time)
                    );

    $payload = json_encode($fields);  
    $curl_session = curl_init();
    curl_setopt($curl_session, CURLOPT_URL, $path_to_fcm);
    curl_setopt($curl_session, CURLOPT_POST, true);
    curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl_session, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt($curl_session, CURLOPT_POSTFIELDS, $payload); 
    $result = curl_exec($curl_session);
    curl_close($curl_session);  
    var_dump($fields);
    // return $result;
}


function encryptData($password){
    $ciphering = "AES-128-CTR"; 
    $iv_length = openssl_cipher_iv_length($ciphering); 
    $options = 0; 
    $encryption_iv = '1234567891011121';
    $encryption_key = "GeeksforGeeks";
    $encryption = openssl_encrypt($password, $ciphering, $encryption_key, $options, $encryption_iv);
    return $encryption;
}
function decryptData($hash_pawssord){
    $ciphering = "AES-128-CTR"; 
    $iv_length = openssl_cipher_iv_length($ciphering); 
    $options = 0; 
    $decryption_iv = '1234567891011121';
    $decryption_key = "GeeksforGeeks";
    $decryption=openssl_decrypt ($hash_pawssord, $ciphering, $decryption_key, $options, $decryption_iv);
    return $decryption;
}
    
function fetcsubchildArray($dbconnection,$search)
{
    $query = mysqli_query($dbconnection, "SELECT * FROM `mcq_sub_module` WHERE `mcq_module_id_ref` = '$search'");
    $return_arr = array();
    while ($row = mysqli_fetch_array($query)) {
        $data =  $row['mcq_sm_child_id'];    
        array_push($return_arr,$data);
    } ;
    $data = implode(',',$return_arr); 
    return $data; 
}

