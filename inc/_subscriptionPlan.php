<?php
include('dbConfig.php');
session_start();
if(isset($_POST["editSubscriptionPlan"]))
{ 
        $planid =  $_POST["uniqueId"];
        $planType = $_POST["planType"];
        $planName = $_POST["planName"];
        $planDiscountCost = $_POST["planDiscountCost"];
        $planCancelledCost = $_POST["planCancelledCost"];
        $planexpiryMonth = $_POST["planexpiryMonth"];
        $planFullDesc = $_POST["planFullDesc"];
        $planDesc = $_POST["planDesc"]; 
        
        if($planType == 'type1')
        {
        $query = "UPDATE `user_plans` SET  `plan_name`= ? ,`plan_cost`=  ?,`plan_description`=?,`plan_expiry`=?, `plan_full_description`=?, `cancelled_price`= ? WHERE `plan_id` = ?";
        }
        else if($planType == 'type2')
        {
            $query = "UPDATE `user_sub_plans` SET `plan_name`= ? ,`plan_cost`=  ?,`plan_description`=?,`plan_expiry`=?, `plan_full_description`=?, `cancelled_price`= ?  WHERE `sl_no` = ?";
        }

      
        $stmt = $dbconnection->prepare($query);


        $stmt->bind_param("ssssssi", $planName, $planDiscountCost, $planDesc , $planexpiryMonth,  $planFullDesc,$planCancelledCost ,$planid) or die($dbconnection->error) ;

        $result = $stmt->execute(); 

        if ($result) {  
            $_SESSION['plansuccess'] = "Subscription Plan Updated successfully";  
            header("location:../subscription-plan-list.php");
            exit();
        } else {
            $_SESSION['planerror'] = "Data Update Error";
            header("location:../subscription-plan-list.php");
            exit();
        }

        $stmt->close();
}