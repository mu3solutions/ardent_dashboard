<?php
include('dbConfig.php');
include('functions.php'); 
session_start();

if (isset($_POST['addClinicalQuestion'])) {
    $question = $_POST['Question'];
    $examDate = $_POST['examDate'];
    $option1 = $_POST['option1'];
    $option2 = $_POST['option2'];
    $option3 = $_POST['option3'];
    $option4 = $_POST['option4'];
    $correctoption = $_POST['correctoption'];
    $answerExplanation = $_POST['answerExplanation'];
    $questionImage = null;
    if (isset($_POST['questionImage'])) {
        $questionImage = $_POST['questionImage'];
    }
    $answerImage = null;
    if (isset($_POST['answerImage'])) {
        $answerImage = $_POST['answerImage'];
    }

    $arr = array($option1, $option2, $option3, $option4);
    $finalAns = implode("$&", $arr);
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/clinical/';



    if (!empty($_FILES['questionImage']['name'])) {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/clinical/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
    }

    if (!empty($_FILES['answerImage']['name'])) {

        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) {
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId . $_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL . $uniquedId . $_FILES['answerImage']['name'][$i];
                $newFilePath = "../images/clinical/" . $uniquequesimage[$i];

                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $answerImage = implode(',', $uniquequesimageURL);
                }
            }
        }
    }




    $query = "INSERT INTO `tbl_clinical_vignettes`(`cv_test_date`,`cv_qn_desc`, `cv_answers`, `cv_qn_correct_answer_num`,`cv_answer_desc`,`cv_qn_desc_images`,`cv_answer_desc_images`) VALUES ( ? , ? , ? , ? , ? , ? , ? )";

    $stmt = $dbconnection->prepare($query) or die($dbconnection->error);


    $stmt->bind_param("sssssss", $examDate, $question, $finalAns, $correctoption, $answerExplanation, $questionImage, $answerImage);

    $result = $stmt->execute() or die($dbconnection->error);
    if ($result) {
        $last_id = $dbconnection->insert_id;
        $update = "UPDATE `tbl_clinical_vignettes` SET `ref_id`= CONCAT_WS('','CV', `cv_qn_id`, 'Q', `cv_qn_id`) WHERE `cv_qn_id`='$last_id'";
        mysqli_query($dbconnection, $update);
        $_SESSION['clinicalsuccess'] = "Clinilcal Question Added Successfully";
        header("location:../cliniccal-vignets-list.php");
        exit();
    } else {

        $_SESSION['clinicalerror'] = "Data Update Error";
        header("location:../cliniccal-vignets-list.php");
        exit();
    }

    $stmt->close();
}
// if (isset($_POST['excelImport'])) 
// {



//     if ($_FILES["excelfileImport"]["name"] != '') {
//         $allowed_extension = array('xls', 'xlsx', 'csv');
//         $file_array = explode(".", $_FILES["excelfileImport"]["name"]);
//         $file_extension = end($file_array);

//         if (in_array($file_extension, $allowed_extension)) {
//             $file_name = time() . '.' . $file_extension;
            
//             $uniquequesimage = uniqidReal() . $_FILES["excelfileImport"]["name"];
//             $target = "../uploads/" . basename($uniquequesimage);
          
//             move_uploaded_file($_FILES['excelfileImport']['tmp_name'], $file_name);
          
//             $file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name);
//             $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type); 

//             $spreadsheet = $reader->load($file_name);

//             unlink($file_name);

//             $data = $spreadsheet->getActiveSheet()->toArray();

//             $lines = 0;

//             foreach ($data as $row) {
//                 if ($lines > 0) {
//                     $option1 =  $row[2];
//                     $option2 =  $row[3];
//                     $option3 =  $row[4];
//                     $option4 =  $row[5];

//                     $arr = array($option1, $option2, $option3, $option4);
//                     $finalAns = implode("$&", $arr);

//                     $testDate = $row[0];
//                     $questions = $row[1];
//                     $correctans = $row[6];
//                     $answerDesc = $row[7];
 

//                     $query = "INSERT INTO `tbl_clinical_vignettes`(`cv_test_date`,`cv_qn_desc`, `cv_answers`, `cv_qn_correct_answer_num`,`cv_answer_desc`) VALUES ( ?, ?, ?, ?, ?)";

//                     $stmt = $dbconnection->prepare($query) or die($dbconnection->error);


//                     $stmt->bind_param("sssss", $testDate, $questions, $finalAns, $correctans, $answerDesc) or die($dbconnection->error);

//                     $result = $stmt->execute() or die($dbconnection->error);
                    
//                 } else {
//                     $lines = 1;
//                 }
//             }
//             if($result){
//                 $success = move_uploaded_file($_FILES["excelfileImport"]["tmp_name"], $file_name);
//                 $_SESSION['clinicalsuccess'] = "Student Update Successfully";
//         header("location:../cliniccal-vignets-list.php");
//             }
            
//         } else {
//             $message = '<div class="alert alert-danger">Only .xls .csv or .xlsx file allowed</div>';
//         }
//     } else {
//         $message = '<div class="alert alert-danger">Please Select File</div>';
//     } 
// }
if (isset($_POST['editClinical'])) {
    $question = $_POST['Question'];
    $questionId = $_POST['questionId'];
    $examDate = $_POST['examDate'];
    $option1 = $_POST['option1'];
    $option2 = $_POST['option2'];
    $option3 = $_POST['option3'];
    $option4 = $_POST['option4'];
    $correctoption = $_POST['correctoption'];
    $answerExplanation = $_POST['answerExplanation'];
    $questionImage = null; 
    if (isset($_POST['questionImage'])) {
        $questionImage = $_POST['questionImage'];
    }
    $answerImage = null;
    if (isset($_POST['answerImage'])) {
        $answerImage = $_POST['answerImage'];
    }

    $arr = array($option1, $option2, $option3, $option4);
    $finalAns = implode("$&", $arr);
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/clinical/';



    if (!empty($_FILES['questionImage']['name'])) 
    {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/clinical/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
        mysqli_query($dbconnection,"UPDATE `tbl_clinical_vignettes` SET `cv_qn_desc_images` = '$questionImage' WHERE `cv_qn_id` ='$questionId'"); 

    }

    if (!empty($_FILES['answerImage']['name'])) {

        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) 
        {
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId . $_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL . $uniquedId . $_FILES['answerImage']['name'][$i];
                $newFilePath = "../images/clinical/" . $uniquequesimage[$i]; 
                
                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $answerImage = implode(',', $uniquequesimageURL);
                }
            }
        }
        mysqli_query($dbconnection,"UPDATE `tbl_clinical_vignettes` SET `cv_answer_desc_images` = '$answerImage' WHERE `cv_qn_id` ='$questionId'"); 
    } 
    $query =  "UPDATE  `tbl_clinical_vignettes`  SET  `cv_test_date` = ? ,`cv_qn_desc` = ? , `cv_answers` = ? , `cv_qn_correct_answer_num` = ? ,`cv_answer_desc` = ? WHERE `cv_qn_id` = ? ";

    $stmt = $dbconnection->prepare($query) or die($dbconnection->error);


    $stmt->bind_param("ssssss", $examDate, $question, $finalAns, $correctoption, $answerExplanation,$questionId);

    $result = $stmt->execute() or die($dbconnection->error);
    if ($result) { 
        mysqli_query($dbconnection,"UPDATE `tbl_clinical_vignettes` SET `ref_id`= CONCAT_WS('','CV', `cv_qn_id`, 'Q', `cv_qn_id`)"); 
        $_SESSION['clinicalsuccess'] = "Clnical Question Updated Successfully"; 
        header("location:../cliniccal-vignets-list.php");
        exit();
    } else {

        $_SESSION['clinicalerror'] = "Data Update Error"; 
        header("location:../cliniccal-vignets-list.php");
        exit();
    }

    $stmt->close();
}

if(isset($_POST['deleteQuestion']))
{
    $questionId  = $_POST['questionID'];

    $delete = "UPDATE `tbl_clinical_vignettes` SET `status` = 0 WHERE  `cv_qn_id` ='$questionId'";
    $stmt = $dbconnection->prepare($delete) or die($dbconnection->error); 
    $result = $stmt->execute() or die($dbconnection->error);

    if($result)
    {
        echo's';
    }else
    {
        echo'f';
    }
}