<?php
include('dbConfig.php');
include('functions.php'); 
session_start();

 
if (isset($_POST['addCoupon'])) 
{ 
    $couponName = $_POST['couponName'];  
    $coupon_offer = $_POST['couponDiscout']; 
    $couponExpiry = $_POST['couponExpiry']; 
    $coupon_status = $_POST['coupon_status'];  

    $query = "INSERT INTO `tbl_coupons`(`coupon_name`,`coupon_offer`,`coupon_status`,`coupon_expiry`) VALUES (? , ?, ?, ?)"; 
        $stmt = $dbconnection->prepare($query);
        $stmt->bind_param("ssss",$couponName,$coupon_offer,$coupon_status,$couponExpiry);
        
        $result = $stmt->execute();
        if($result){  
            $_SESSION['Couponsuccess']="Coupons Added Successfully"; 
            header('location:../coupons-list.php'); 
            exit(); 
        }else{ 
            $_SESSION['Couponerror']="Coupons  Update Failed";  
            header('location:../coupons-list.php');
            exit(); 
        }
        
        $stmt->close();
}
if (isset($_POST['editCoupon'])) 
{
    $uniquedId = $_POST['uniqueId']; 
    $couponName = $_POST['couponName'];  
    $coupon_offer = $_POST['couponDiscout']; 
    $couponExpiry = $_POST['couponExpiry']; 
    $coupon_status = $_POST['coupon_status'];  

    $query = "UPDATE `tbl_coupons` SET  `coupon_name`= ? ,`coupon_offer`= ? ,`coupon_status`= ? ,`coupon_expiry`= ?  WHERE `coupon_id`= ?"; 
        $stmt = $dbconnection->prepare($query);
        $stmt->bind_param("sssss",$couponName,$coupon_offer,$coupon_status,$couponExpiry,$uniquedId);
        
        $result = $stmt->execute();
        if($result){  
            $_SESSION['Couponsuccess']="Coupons Update Successfully"; 
            header('location:../coupons-list.php'); 
            exit(); 
        }else{ 
            $_SESSION['Couponerror']="Coupons  Update Failed";  
            header('location:../coupons-list.php');
            exit(); 
        }
        
        $stmt->close();
}
 