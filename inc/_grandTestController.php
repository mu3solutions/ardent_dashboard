
<?php
 
include('./dbConfig.php');
include('functions.php');
session_start();

if(isset($_POST["addNewGrandTest"]))
{ 
        $testId =  getMaxRecord('grand_test','gt_id',$dbconnection) + 1;
        $testName =  $_POST["testName"];
        $testEndDate =  $_POST["testEnd"];
        $totalQuestions = $_POST["totalQuestions"];
        $totalTime = $_POST["totalTime"];
        $correctAnsMark = $_POST["correctAnsMark"];
        $negMark = $_POST["negMark"];
        $grandAccessStatus = $_POST["grandAccessStatus"];
        $grandActiveStatus = $_POST["grandActiveStatus"];
        $sortorder = '0';
        $comleteAnswerlist = '{}';
        $comleteSelectedAnswerlist = '{}';  

        if(isset($_POST['sortorder']))
        {
            $sortorder = $_POST['sortorder']; 
        }
         
       
        $gtIcon="https://t3.ftcdn.net/jpg/01/96/93/52/240_F_196935235_tg8Kf5bZYlrAklFMzsulcEeu4w4tqCha.jpg";
         
        if($sortorder =='0')
        {
            $sortorder = getMaxRecord('grand_test','sort_order',$dbconnection) + 1;
        }
        else if($sortorder =='1')
        { 
            mysqli_query($dbconnection,"UPDATE `grand_test` SET  `sort_order` = `sort_order` +1");
            $sortorder = 1;
        }


        $query = "INSERT INTO `grand_test`(`testid`, `gt_test_name`, `gt_icon`, `gt_expiry_date`, `gt_total_questions`, `gt_total_time`, `completed_answer_list`, `completed_selected_answer_list`,`sort_order`, `neg_mark`, `correct_answer_value`, `test_open`, `status`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        $stmt = $dbconnection->prepare($query)or die($dbconnection->error);


        $stmt->bind_param("sssssssssssss", $testId, $testName, $gtIcon, $testEndDate, $totalQuestions, $totalTime, $comleteAnswerlist, $comleteSelectedAnswerlist,$sortorder,$negMark,$correctAnsMark,$grandAccessStatus,$grandActiveStatus)or die($dbconnection->error);

        $result = $stmt->execute() or die($dbconnection->error);

        if ($result) {   
            $_SESSION['newGrandTestSuccess'] = "New Grand Test Created successfully";
            header("location:../grand-test-list.php");
            exit();
        } else {
            $_SESSION['newGrandTestError'] = "Data Update Error";
            header("location:../grand-test-list.php");
            exit();
        }

        $stmt->close();
}
if(isset($_POST["editGrandTest"]))
{
        $testId =  $_POST["testId"]; 
        $testName =  $_POST["testName"];
        $testEndDate =  $_POST["testEnd"];
        $totalQuestions = $_POST["totalQuestions"];
        $totalTime = $_POST["totalTime"];
        $correctAnsMark = $_POST["correctAnsMark"];
        $negMark = $_POST["negMark"];
        $grandAccessStatus = $_POST["grandAccessStatus"];
        $grandActiveStatus = $_POST["grandActiveStatus"];
    
    $query = "UPDATE `grand_test`  SET `gt_test_name` =  ? ,`gt_expiry_date`  = ? , `gt_total_questions` = ?  , `gt_total_time` = ? , `correct_answer_value` = ? , `neg_mark` = ? , `test_open` = ? , `status` = ?   WHERE `testid` = ? "; 
    
    $stmt = $dbconnection->prepare($query) or die($dbconnection->error);
    

    $stmt->bind_param("sssssssss", $testName, $testEndDate, $totalQuestions, $totalTime, $correctAnsMark, $negMark, $grandAccessStatus, $grandActiveStatus, $testId) or die($dbconnection->error);
    
    $result = $stmt->execute() or die($dbconnection->error); 
    
        if ($result) { 
            $_SESSION['newGrandTestSuccess'] = "Grand Test Updated successfully";
            header("location:../grand-test-list.php");
            exit();
        } else { 
            $_SESSION['newGrandTestError'] = "Update Failed";
            header("location:../grand-test-list.php");
            exit();
        }
        $stmt->close();
}
if (isset($_POST['addGtQuestion'])) { 
    
    $testId = $_POST['gtId']; 
    $totalQnCount = fetchData($dbconnection,'gt_total_questions','grand_test',$testId,'testid'); 
$currentQnCount = checkGTQuestionsCount($testId,$dbconnection);        

if($currentQnCount <= $totalQnCount){ 
    $question = $_POST['Question']; 
    $mainModule = $_POST['mainModule'];  
    $option1 = $_POST['option1'];
    $option2 = $_POST['option2'];
    $option3 = $_POST['option3'];
    $option4 = $_POST['option4'];
    $correctoption = $_POST['correctoption'];
    $answerExplanation = $_POST['answerExplanation'];
    $ref  = $_POST["answerRef"];
    $mcqchildid = null; 
    $questionImage = null; 
    $answerImage = null; 
  
    if (isset($_POST['questionImage'])){ $questionImage = $_POST['questionImage']; }
    if (isset($_POST['answerImage'])){ $answerImage = $_POST['answerImage']; }

    $arr = array($option1, $option2, $option3, $option4); 
    
    $finalAns = implode("$&", $arr); 
     
    $grandTestName = fetchData($dbconnection,'gt_test_name','grand_test',$testId,'testid');
     
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/GrandTest/'; 
    $location = "../images/GrandTest/".$testId.$grandTestName;
    if(!is_dir($location)){
        mkdir($location, 0755);
    }
   
    
    if (!empty($_FILES['questionImage']['name'])) {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image; 
        $location  .= "/" .basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $location);
        $questionImage = $imageURL .$testId. $grandTestName .'/' .$uniquequesimage; 
    }

    if (!empty($_FILES['answerImage']['name'])) {

        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) {
            
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL.$grandTestName . '/'.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                $newFilePath = "../images/GrandTest/".$testId.$grandTestName . '/'.$uniquequesimage[$i]; 
                if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                {
                    $answerImage = implode(',', $uniquequesimageURL);  
                }
            }
        }
    } 

    $query = "INSERT INTO `qa_grand_test`(`testId`,`mcq_child_id`,`mcq_qn_desc`, `mcq_answers`, `mcq_qn_correct_answer_num`,`mcq_answer_desc`,`mcq_qn_desc_images`,`mcq_answer_desc_images`,`mcq_reference`,`cat_id`) VALUES ( ? ,? , ? , ? , ? , ? , ? , ? , ? ,?)";

    $stmt = $dbconnection->prepare($query);


    $stmt->bind_param("issssssssi",$testId,$mcqchildid,$question, $finalAns, $correctoption, $answerExplanation, $questionImage, $answerImage,$ref,$mainModule); 

    $result = $stmt->execute();
    if ($result) {   
        $_SESSION['Gtquestionsuccess'] = "Question Added  Successfully"; 
        $EtestID = encryptData($testId);
        $EmainModule = encryptData($mainModule);
        $last_id = $dbconnection->insert_id;
        $update = "UPDATE `qa_grand_test` SET `ref_id`= CONCAT_WS('','GT', `testId`, 'Q', `mcq_qn_id`) WHERE `mcq_qn_id`='$last_id'";
        mysqli_query($dbconnection, $update);
        // $currentQnCount = checkGTQuestionsCount($testId,$dbconnection);
        // $update2 = "UPDATE `grand_test` SET `gt_total_questions`= '$currentQnCount' WHERE `testid`='$testId'";
        // mysqli_query($dbconnection, $update2); 
        header("location:../add-gteditedquestion.php?testID=".$EtestID.'&module='.$EmainModule);
        exit();
    } else { 
        $_SESSION['Gtquestionerror'] = "Data Update Error";
        header("location:../grand-test-list.php");
        exit();
    } 
    $stmt->close();
    }
    else{ 
        $_SESSION['Gtquestionerror'] = "Total Questions Limit Reached";
        header("location:../grand-test-list.php");
        exit();
    } 
}


if(isset($_POST['editGTQuestion']))
{ 
    $questionId = $_POST['questionId'];  
    $testId = $_POST['testId'];  
    $question = $_POST['Question'];  
    $option1 = $_POST['option1']; 
    $option2 = $_POST['option2']; 
    $option3 = $_POST['option3']; 
    $option4 = $_POST['option4']; 
    $correctoption = $_POST['correctoption']; 
    $answerExplanation = $_POST['answerExplanation']; 
    $mcqReference = $_POST['answerReferece']; 
    $questionImage = null; 
    $answerImage = null; 
   

    $arr = array($option1,$option2,$option3,$option4);
    $finalAns = implode("$&",$arr); 

    $grandTestName = fetchData($dbconnection,'gt_test_name','grand_test',$testId,'testid');

    
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/GrandTest/'; 
    $location = "../images/GrandTest/".$testId.$grandTestName;
    if(!is_dir($location)){
        mkdir($location, 0755);
    }
  
    if (!empty($_FILES['questionImage']['name'])) { 
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image; 
        $location  .= "/" .basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $location);
        $questionImage = $imageURL .$testId. $grandTestName .'/' .$uniquequesimage; 
        mysqli_query($dbconnection,"UPDATE `qa_grand_test` SET `mcq_qn_desc_images` = '$questionImage' WHERE `mcq_qn_id` = '$questionId' ");
    }
    
        if (isset($_FILES['answerImage'])) { 
        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) {
            
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL.$testId.$grandTestName . '/'.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                $newFilePath = "../images/GrandTest/".$testId.$grandTestName . '/'.$uniquequesimage[$i]; 
                if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                {
                    if(!empty($_FILES['answerImage']['name'][$i])){
                        $answerImage = implode(',', $uniquequesimageURL);   
                    }
                }
            }
        }
        if(!empty($_FILES['answerImage']['name'][0])){ 
            mysqli_query($dbconnection,"UPDATE `qa_grand_test` SET `mcq_answer_desc_images` = '$answerImage' WHERE `mcq_qn_id` = '$questionId' ");

        }
        
    }  
    $query = "UPDATE `qa_grand_test` SET `mcq_qn_desc` = ? , `mcq_answers` = ?,`mcq_qn_correct_answer_num` = ?, `mcq_answer_desc` = ?,`mcq_reference` = ? WHERE `mcq_qn_id` = ? "; 
        $stmt = $dbconnection->prepare($query);
        $stmt->bind_param("ssssss",$question,$finalAns,$correctoption,$answerExplanation,$mcqReference,$questionId);
        
        $result = $stmt->execute();
        if($result){  

            $_SESSION['gtquestionsuccess']="Question Update Successfully";  
            $EtestId = encryptData($testId);
            header("location:../gt-question-list.php?tid=$EtestId");
            exit(); 
        }else{ 
            $_SESSION['gtquestionerror']="Question Update Failed";  
            header("location:../gt-question-list.php?tid=$EtestId"); 
            exit(); 
        }
        
        $stmt->close();
}

if(isset($_POST['deleteQuestion']))
{
    $questionId  = $_POST['questionID'];

    $delete = "UPDATE `qa_grand_test` SET `status` = 0 WHERE  `mcq_qn_id` ='$questionId'";
    $stmt = $dbconnection->prepare($delete) or die($dbconnection->error); 
    $result = $stmt->execute() or die($dbconnection->error);

    if($result)
    {
        echo's';
    }else
    {
        echo'f';
    }
}

if (isset($_POST['addSelectedGtQuestion'])) {  
    $testId = $_POST['gtId'];  
    $totalQnCount = fetchData($dbconnection,'gt_total_questions','grand_test',$testId,'testid'); 
$currentQnCount = checkGTQuestionsCount($testId,$dbconnection);        

if($currentQnCount <= $totalQnCount){ 
    
    $mainModule = $_POST['mainsModule'];   
    $limit =false;

    foreach ($_POST['checkbox'] as $key => $value) { 
        $select = mysqli_query($dbconnection,"SELECT * FROM `qa` WHERE `mcq_qn_id`='$value'"); 
        if($row = mysqli_fetch_array($select)){
            $question = $row['mcq_qn_desc']; 
            $finalAns = $row['mcq_answers'];
            $correctoption = $row['mcq_qn_correct_answer_num'];
            $answerExplanation = $row['mcq_answer_desc'];
            $ref  = $row["mcq_reference"];
            $questionImage  = $row["mcq_qn_desc_images"];
            $answerImage  = $row["mcq_answer_desc_images"];
            $mcqchildid = null;  

  if($currentQnCount >= $totalQnCount){  
                $limit = true;
            }
            
            if($limit){
                $_SESSION['gtquestionsuccess']="Total Questions Limit Reached";  
                $EtestId = encryptData($testId);
                header("location:../gt-question-list.php?tid=$EtestId");
                exit(); 
            }
            $query = "INSERT INTO `qa_grand_test`(`testId`,`mcq_child_id`,`mcq_qn_desc`, `mcq_answers`, `mcq_qn_correct_answer_num`,`mcq_answer_desc`,`mcq_qn_desc_images`,`mcq_answer_desc_images`,`mcq_reference`,`cat_id`) VALUES ( ? ,? , ? , ? , ? , ? , ? , ? , ? ,?)" or die($dbconnection->error);
            $stmt = $dbconnection->prepare($query) or die($dbconnection->error);
            $stmt->bind_param("issssssssi",$testId,$mcqchildid,$question, $finalAns, $correctoption, $answerExplanation, $questionImage, $answerImage,$ref,$mainModule) or die($dbconnection->error);
            $result = $stmt->execute() or die($dbconnection->error);
            
            $totalQnCount = fetchData($dbconnection,'gt_total_questions','grand_test',$testId,'testid'); 
            $currentQnCount = checkGTQuestionsCount($testId,$dbconnection);        
            
          

            if($result){
                $last_id = $dbconnection->insert_id;
                $update = "UPDATE `qa_grand_test` SET `ref_id`= CONCAT_WS('','GT', `testId`, 'Q', `mcq_qn_id`) WHERE `mcq_qn_id`='$last_id'";
                mysqli_query($dbconnection, $update); 
                
                // $currentQnCount = checkGTQuestionsCount($testId,$dbconnection);
                // $update2 = "UPDATE `grand_test` SET `gt_total_questions`= '$currentQnCount' WHERE `testid`='$testId'";
                // mysqli_query($dbconnection, $update2); 
                $success= true;
            }
           
        }
    }


    if($success){
        $_SESSION['gtquestionsuccess']="Question Added Successfully";  
        $EtestId = encryptData($testId);
        header("location:../gt-question-list.php?tid=$EtestId");
        exit(); 
    }
    
}else{
      $_SESSION['gtquestionsuccess']="Total Questions Limit Reached";  
        $EtestId = encryptData($testId);
        header("location:../gt-question-list.php?tid=$EtestId");
        exit(); 
}
}