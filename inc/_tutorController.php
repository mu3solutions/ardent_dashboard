<?php
include('dbConfig.php');
include('functions.php'); 
session_start();

if (isset($_POST['addtutor'])) { 
    
    $tutorName = $_POST['tutorName'];   
    $tutorDesc = $_POST['tutorDesc']; 

    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/tutorImages/'; 
     
    
    if (!empty($_FILES['tutorImage']['name'])) {
        $image = ''; 
        $image = $_FILES['tutorImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/tutorImages/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['tutorImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
    }
 

    $query = "INSERT INTO `tutordetails`(`tutorName`, `tutorDesc`, `tutorImage`) VALUES ( ? , ? , ?)";

    $stmt = $dbconnection->prepare($query);


    $stmt->bind_param("sss", $tutorName, $tutorDesc, $questionImage);

    $result = $stmt->execute();
    if ($result) {   
        $_SESSION['tutorSuccess'] = "Tutor Added Successfully";
        header("location:../tutor-list.php");
        exit();
    } else { 
        $_SESSION['tutorError'] = "Data Insert Failed";
        header("location:../tutor-list.php");
        exit();
    } 
    $stmt->close();
} 

if (isset($_POST['edittutor'])) { 
    
    $tutorName = $_POST['tutorName'];   
    $tutorDesc = $_POST['tutorDesc']; 
    $tutorId = $_POST['tutorId']; 

    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/tutorImages/'; 
     
    
    if (!empty($_FILES['tutorImage']['name'])) {
        $image = ''; 
        $image = $_FILES['tutorImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/tutorImages/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['tutorImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
        mysqli_query($dbconnection,"UPDATE `tutordetails` SET `tutorImage` = '$questionImage' WHERE `tutorId` = '$tutorId' ");
    }
 

    $query = "UPDATE `tutordetails` SET `tutorName` = ? , `tutorDesc` = ?  WHERE `tutorId` = ? ";

    $stmt = $dbconnection->prepare($query);


    $stmt->bind_param("ssi", $tutorName, $tutorDesc ,$tutorId);

    $result = $stmt->execute();
    if ($result) {   
        $_SESSION['tutorSuccess'] = "Tutor Updated Successfully";
        header("location:../tutor-list.php");
        exit();
    } else { 
        $_SESSION['tutorError'] = "Data Update Failed";
        header("location:../tutor-list.php");
        exit();
    } 
    $stmt->close();
}  

if(isset($_POST['deleteQuestion']))
{
    $questionId  = $_POST['questionID'];

    $delete = "UPDATE `tutordetails` SET `status`=0 WHERE  `tutorId` ='$questionId'";
    $stmt = $dbconnection->prepare($delete) or die($dbconnection->error); 
    $result = $stmt->execute() or die($dbconnection->error);

    if($result)
    {
        echo'success';
    }else
    {
        echo'fail';
    }
    $stmt->close();
}
