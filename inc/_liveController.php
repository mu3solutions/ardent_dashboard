<?php
include('dbConfig.php');
include('functions.php'); 
session_start(); 
if (isset($_POST['editLive'])) {
    echo 's';
    
    $liveName = $_POST['liveName'];
    $liveDesc = $_POST['liveDesc'];
    $livedetails = $_POST['livedetails'];
    $upcoming = null;
    if(isset($_POST['upcoming'])){
    $upcoming = $_POST['upcoming']; 
    }
    $liveDate = $_POST['liveDate']; 
    $liveTime = $_POST['liveTime']; 
    $liveduration = $_POST['liveduration']; 
    $liveAttendies = $_POST['liveAttendies']; 
    $upcommingliveDate = $_POST['upcommingliveDate']; 
    $upcommingliveTime = $_POST['upcommingliveTime']; 
    // $liveUrl = $_POST['liveUrl']; 
    $chooseTutor = $_POST['chooseTutor']; 
    $status = $_POST['status'];  

    $finalLiveDate = $liveDate.'@'.$liveTime;
    $finalUpcomLiveDate = $upcommingliveDate.'@'.$upcommingliveTime;
    
    
    
    
    $exequery =  mysqli_query($dbconnection,"SELECT * FROM `tutordetails` WHERE `tutorId`='$chooseTutor'");
    if($row = mysqli_fetch_array($exequery)){
        $tutorName =  $row['tutorName']; 
        $tutorImage =  $row['tutorImage'];
    }
    
    $update =  "UPDATE `tbl_live` SET `liveName` = ? , `liveDesc` = ?, `live_class_date` = ?,  `live_duration`= ?,`live_attendies`= ?, `live_upcoming_date`= ?,   `exam_details` = ? ,`upcoming` = ? , `status`= ?, `live_tutor_id` = ? , `tutor_name`= ?, `tutor_image` = ? WHERE liveId = '1'";
  
  
    $stmt = $dbconnection->prepare($update) or die($dbconnection->error);
  
    $stmt->bind_param("ssssssssssss", $liveName, $liveDesc,$finalLiveDate,$liveduration,$liveAttendies ,$finalUpcomLiveDate,$livedetails,$upcoming,$status,$chooseTutor,$tutorName,$tutorImage);
     



    $result = $stmt->execute()or die($dbconnection->error);
    
    
    if ($result) {    
        $_SESSION['liveQuizSuccess'] = "Live Quiz Updated Successfully";
        header("location:../edit-live-quiz.php");
        exit();
    } else { 
        $_SESSION['liveQuizError'] = "Data Update Error";
        header("location:../edit-live-quiz.php"); 
        exit();
    }

    $stmt->close();
} 