<?php
include('dbConfig.php');
include('functions.php'); 
session_start();


if(isset($_POST['pushNotification']))
{
    $success = false; 
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/pushNotification/'; 
    $questionImage = null;


    if (!empty($_FILES['questionImage']['name'])) {
        $image = ''; 
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image;
        $target = "../images/pushNotification/" . basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
        $questionImage = $imageURL . $uniquequesimage;
    }
 
    $sql = "SELECT * FROM `user`";
    
     
    $executesql = mysqli_query($dbconnection,$sql);
    if(mysqli_num_rows($executesql) > 0)
    {  
        $notificationTitle = mysqli_real_escape_string($dbconnection,$_POST['notificationTitle']);
        $notificationMessage = mysqli_real_escape_string($dbconnection,$_POST['notificationMessage']);
        $arr = array();
        while($row = mysqli_fetch_array($executesql))
        {    
            if(!empty($row['fcm_token']))
            {  
                $arr[] = $row[ 'fcm_token' ]; 
                $userName = $row[ 'user_name' ];
                sendMCQ($row[ 'fcm_token' ],$notificationTitle,$notificationMessage,$questionImage,$userName);
                $success = true;  
            }
        }
         

        if($success) {   
            $_SESSION['pushnotificationSuccess'] = "Notification Sent Successfully";
            header("location:../send-push-notifications.php");
            exit();
        } else { 
            $_SESSION['pushnotificationError'] = "Notification Failed";
            header("location:../send-push-notifications.php");
            exit();
        }
    } 
}