<?php 
include_once('dbConfig.php'); 
include_once('functions.php'); 
session_start();
if(isset($_POST['submit'])){
    $studentName = $_POST['studentName'];
    $studentEmail = $_POST['studentEmail']; 
    $studentNumber = $_POST['studentNumber']; 
    $studentCollege = $_POST['studentCollege'];
    $studentState = $_POST['studentState'];
    $studentCollegeYear = $_POST['studentCollegeYear'];
    $studentAccId = hexdec( uniqid() ); 
    $finalPlan = $_POST['subPlan'];  
    $currentAppversion = fetchData($dbconnection,'old_api_version','dashboard','1','db_id');

       
    if(isset($_POST['childPlan']))
    {
        $finalPlan = implode(",",$_POST['childPlan']); 
    } 

    $currentTimestamp = date("d-m-Y H:i:s");  

    $query = "INSERT INTO `user`(`user_account_id`, `user_name`, `user_email`,`user_mobile`,`user_last_login`,`user_college`,`user_acadamic_year`,`user_state`,`user_access_level`,`current_app_version`) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ?, ?)"; 

        $stmt = $dbconnection->prepare($query) or die($dbconnection->error);

        $stmt->bind_param("ssssssssss", $studentAccId,$studentName,$studentEmail,$studentNumber,$currentTimestamp,$studentCollege,$studentCollegeYear,$studentState,$finalPlan,$currentAppversion);   
        $result = $stmt->execute();
        
        if($result) {
            $_SESSION['studentsuccess']="Student Added Successfully";
            header("location:../student-list.php");
            exit(); 
        }
        else { 
            $_SESSION['studenterror']="Data Error Inserting";
            header("location:../student-list.php");
            exit(); 
        }
        
        $stmt->close();

}

if(isset($_POST['updateDetail'])){
    $studentAccId = $_POST['studentAccId'];
    $studentName = $_POST['studentName'];
    $studentEmail = $_POST['studentEmail']; 
    $studentNumber = $_POST['studentNumber']; 
    $studentCollege = $_POST['studentCollege'];
    $studentState = $_POST['studentState'];
    $studentCollegeYear = $_POST['studentCollegeYear'];
    $subPlan = $_POST['subPlan']; 
     
    if(isset($_POST['childPlan']))
    {
        $childPlan = $_POST['childPlan'];
    }
    $currentTimestamp = date("d-m-Y H:i:s");  

    $query = "UPDATE `user` SET  `user_name` = ? , `user_email` = ?,`user_mobile` = ?,`user_last_login` = ? ,`user_college` = ? ,`user_acadamic_year` = ? ,`user_state` = ? WHERE `user_account_id` = ? "; 

        $stmt = $dbconnection->prepare($query) or die($dbconnection->error);

        $stmt->bind_param("ssssssss", $studentName,$studentEmail,$studentNumber,$currentTimestamp,$studentCollege,$studentCollegeYear,$studentState,$studentAccId);  
        
        $result = $stmt->execute();
        if($result){
            $currentPlan = fetchData($dbconnection,'user_access_level','user',$studentAccId,'user_account_id');
              
            if($subPlan == 'S2'){ 
                if($currentPlan=='0'){
                    $final_access_level = $subPlan;
                }else{ 
                        $finalAns = implode(",",$childPlan);
                        $final_access_level = $finalAns; 
                 }
                mysqli_query($dbconnection, "UPDATE `user` SET `user_access_level`='$final_access_level' WHERE  `user_account_id`='$studentAccId'");
            }else{
              mysqli_query($dbconnection, "UPDATE `user` SET `user_access_level`='$subPlan' WHERE  `user_account_id`='$studentAccId'");  
            }
              
            $_SESSION['studentsuccess']="Student Update Successfully";
            header("location:../student-list.php");
            exit(); 
        }else{
            $_SESSION['studenterror']="Data Update Error";
            header("location:../student-list.php");
            exit(); 
        }
        
        $stmt->close();

}