<?php
include('dbConfig.php');
include('functions.php'); 
session_start();

if (isset($_POST['addMCQQuestion'])) { 
    
    $question = $_POST['Question']; 
    $mainModule = $_POST['mainModule']; 
    $subModule = $_POST['subModuleValue']; 
    $chlidModule = $_POST['subChildModuleValue'];  
    $option1 = $_POST['option1'];
    $option2 = $_POST['option2'];
    $option3 = $_POST['option3'];
    $option4 = $_POST['option4'];
    $correctoption = $_POST['correctoption'];
    $answerExplanation = $_POST['answerExplanation'];
    $ref  = $_POST["answerRef"];
    $questionImage = null; 
    $answerImage = null; 
  
    if (isset($_POST['questionImage'])){ $questionImage = $_POST['questionImage']; }
    if (isset($_POST['answerImage'])){ $answerImage = $_POST['answerImage']; }

    $arr = array($option1, $option2, $option3, $option4); 
    
    $finalAns = implode("$&", $arr); 
    $childModuleName = childNameUsingId($chlidModule,$dbconnection);
     
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/MCQ/'; 
    // $location = "../images/MCQ/".$chlidModule.$trun;
    $location = "../images/MCQ/";
    
    
   
    
    if (!empty($_FILES['questionImage']['name'])) {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image; 
        $location  .= "/" .basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $location);
        $questionImage = $imageURL.$uniquequesimage; 
    }

    if (!empty($_FILES['answerImage']['name'])) {

        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) {
            
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                $newFilePath = "../images/MCQ/".$uniquequesimage[$i]; 
                if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                {
                    $answerImage = implode(',', $uniquequesimageURL);  
                }
            }
        }
    } 

    $query = "INSERT INTO `qa`(`mcq_child_id`,`mcq_qn_desc`, `mcq_answers`, `mcq_qn_correct_answer_num`,`mcq_answer_desc`,`mcq_qn_desc_images`,`mcq_answer_desc_images`,`mcq_reference`) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? )";

    $stmt = $dbconnection->prepare($query);


    $stmt->bind_param("isssssss", $chlidModule, $question, $finalAns, $correctoption, $answerExplanation, $questionImage, $answerImage,$ref);

    $result = $stmt->execute(); 
    if ($result) {  
        mysqli_query($dbconnection,"UPDATE `qa` SET `ref_id`= CONCAT_WS('','MC', `mcq_child_id`, 'Q', `mcq_qn_id`)");  
        mysqli_query($dbconnection,"UPDATE `mcq_sub_module` SET `mcq_sm_qn_count` =(SELECT COUNT(*) FROM `qa` WHERE `qa`.`mcq_child_id` = `mcq_sub_module`.`mcq_sm_child_id` AND `qa`.`status`= 1)");  
        mysqli_query($dbconnection,"DELETE FROM `mcq_user_test_attendee` WHERE `uta_sm_child_id`='$chlidModule' AND `uta_module_status`='Paused'");
         
        $_SESSION['MCQquestionsuccess'] = "Question Added  Successfully";
        $EmainModule =  encryptData($mainModule);
        $EsubModule =  encryptData($subModule);
        $EchlidModule =  encryptData($chlidModule);
            header('Location:../add-mcqeditedquestion.php?mainModule='.$EmainModule.'&subModule='.$EsubModule.'&childModule='.$EchlidModule); 
    } else { 
        $_SESSION['MCQquestionerror'] = "Data Update Error";
        header("location:../add-newmcqquestion.php");
        exit();
    } 
    $stmt->close();
}

if(isset($_POST['deleteQuestion']))
{
    $questionId  = $_POST['questionID'];

    $delete = "DELETE FROM  `tbl_clinical_vignettes` WHERE  `cv_qn_id` ='$questionId'";
    $stmt = $dbconnection->prepare($delete) or die($dbconnection->error); 
    $result = $stmt->execute() or die($dbconnection->error);

    if($result)
    {
        echo's';
    }else
    {
        echo'f';
    }
}

if(isset($_POST['editMCQQuestion']))
{
    $questionId = $_POST['questionId'];  
    $chlidModule = $_POST['childId'];  
    $question = $_POST['Question'];  
    $option1 = $_POST['option1']; 
    $option2 = $_POST['option2']; 
    $option3 = $_POST['option3']; 
    $option4 = $_POST['option4']; 
    $correctoption = $_POST['correctoption']; 
    $answerExplanation = $_POST['answerExplanation']; 
    $mcqReference = $_POST['answerReferece']; 
    $questionImage = null; 
    $answerImage = null; 
    
    $childModuleName = childNameUsingId($chlidModule,$dbconnection);   
    $arr = array($option1,$option2,$option3,$option4);
    $finalAns = implode("$&",$arr); 

    
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/MCQ/'; 
    $location = "../images/MCQ/";
    
     
    if (!empty($_FILES['questionImage']['name'])) {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image; 
        $location  .= "/" .basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $location);
        $questionImage = $imageURL.$uniquequesimage; 
        mysqli_query($dbconnection,"UPDATE `qa` SET `mcq_qn_desc_images` = '$questionImage' WHERE `mcq_qn_id` = '$questionId' ");
    }
    
        if (isset($_FILES['answerImage'])) { 
        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) {
            
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                $newFilePath = "../images/MCQ/".$uniquequesimage[$i];  
                if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                {
                    if(!empty($_FILES['answerImage']['name'][$i])){
                        $answerImage = implode(',', $uniquequesimageURL);   
                    }
                }
            }
        }
        if(!empty($_FILES['answerImage']['name'][0]))
        { 
            mysqli_query($dbconnection,"UPDATE `qa` SET `mcq_answer_desc_images` = '$answerImage' WHERE `mcq_qn_id` = '$questionId' ");

        } 
    }  
    $query = "UPDATE `qa` SET `mcq_qn_desc` = ? , `mcq_answers` = ?,`mcq_qn_correct_answer_num` = ?, `mcq_answer_desc` = ?,`mcq_reference` = ? WHERE `mcq_qn_id` = ? "; 
        $stmt = $dbconnection->prepare($query);
        $stmt->bind_param("ssssss",$question,$finalAns,$correctoption,$answerExplanation,$mcqReference,$questionId);
        
        $result = $stmt->execute();
        if($result){  
            $_SESSION['mcqeditsuccess']="Question Update Successfully"; 
            header("location:../mcq-questions-list.php");
            exit(); 
        }else{ 
            $_SESSION['mcqediterror']="Question Update Failed";  
            header("location:../mcq-questions-list.php");

            exit(); 
        }
        
        $stmt->close();
}