<?php
include('./dbConfig.php');
include('functions.php');
session_start();
$sorting_order = ""; 

if (isset($_POST['addMCQqotdQuestion'])) { 
    
    $question = $_POST['Question'];  
    $option1 = $_POST['option1'];
    $option2 = $_POST['option2'];
    $option3 = $_POST['option3'];
    $option4 = $_POST['option4'];
    $correctoption = $_POST['correctoption'];
    $answerExplanation = $_POST['answerExplanation']; 
    $mcqReference = $_POST['answerReferece'];

    $chlidModule = 131;
    $questionImage = null; 
    $answerImage = null; 
  
    if (isset($_POST['questionImage'])){ $questionImage = $_POST['questionImage']; }
    if (isset($_POST['answerImage'])){ $answerImage = $_POST['answerImage']; }

    $arr = array($option1, $option2, $option3, $option4); 
    
    $finalAns = implode("$&", $arr); 
     
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/mcq-ofthe-day/'; 
    $location = "../images/mcq-ofthe-day/";
    if(!is_dir($location)){
        mkdir($location, 0755);
    }
   
    
    if (!empty($_FILES['questionImage']['name'])) {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image; 
        $location  .= "/" .basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $location);
        $questionImage = $imageURL .'/' .$uniquequesimage; 
    }

    if (!empty($_FILES['answerImage']['name'])) {

        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) {
            
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL. '/'.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                $newFilePath = "../images/mcq-ofthe-day/".$uniquequesimage[$i];  
                if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                {
                    if(!empty($_FILES['answerImage']['name'][$i])){
                        $answerImage = implode(',', $uniquequesimageURL);   
                    }
                }
            }
        }
    } 

    $query = "INSERT INTO `mcq_qa_of_the_day`(`mcq_child_id`,`mcq_qn_desc`, `mcq_answers`, `mcq_qn_correct_answer_num`,`mcq_answer_desc`,`mcq_qn_desc_images`,`mcq_answer_desc_images`,`mcq_reference`) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? )"; 
    
    $stmt = $dbconnection->prepare($query); 

    $stmt->bind_param("isssssss", $chlidModule, $question, $finalAns, $correctoption, $answerExplanation, $questionImage, $answerImage,$mcqReference); 
    $result = $stmt->execute(); 
    if ($result) {  
        $last_id = $dbconnection->insert_id;
        mysqli_query($dbconnection,"UPDATE `mcq_qa_of_the_day` SET `ref_id`= CONCAT_WS('','MQ', `mcq_qn_id`, 'Q', `mcq_qn_id`)  WHERE `mcq_qn_id`='$last_id'"); 
        $_SESSION['MCQquestionsuccess'] = "Question Added Successfully"; 
        header("location:../mcq-qotd-list.php");
        exit();
    } else { 
        $_SESSION['MCQquestionerror'] = "Data Update Error";
        header("location:../mcq-qotd-list.php");
        exit();
    } 
    $stmt->close();
}

if(isset($_POST['deleteQuestion']))
{
    $questionId  = $_POST['questionID'];

    $delete = "UPDATE `mcq_qa_of_the_day` SET `status` = 0 WHERE  `mcq_qn_id` ='$questionId'";
    $stmt = $dbconnection->prepare($delete); 
    $result = $stmt->execute();

    if($result)
    {
        echo's';
    }else
    {
        echo'f';
    }
}

if(isset($_POST['editMCQqotdQuestion']))
{
    $questionId = $_POST['questionId'];    
    $question = $_POST['Question'];  
    $option1 = $_POST['option1']; 
    $option2 = $_POST['option2']; 
    $option3 = $_POST['option3']; 
    $option4 = $_POST['option4']; 
    $correctoption = $_POST['correctoption']; 
    $answerExplanation = $_POST['answerExplanation']; 
    $mcqReference = $_POST['answerReferece']; 
    $questionImage = null; 
    $answerImage = null; 
  
 

    $arr = array($option1,$option2,$option3,$option4);
    $finalAns = implode("$&",$arr); 

    
    $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/mcq-ofthe-day/'; 
    
    $location = "../images/mcq-ofthe-day/";
    if(!is_dir($location)){
        mkdir($location, 0755);
    }
 
    if (!empty($_FILES['questionImage']['name'])) {
        $image = '';
        $image = $_FILES['questionImage']['name'];
        $uniquequesimage = uniqidReal() . $image; 
        $location  .= "/" .basename($uniquequesimage);
        $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $location);
        $questionImage = $imageURL .'/' .$uniquequesimage; 
        mysqli_query($dbconnection,"UPDATE `mcq_qa_of_the_day` SET `mcq_qn_desc_images` = '$questionImage' WHERE `mcq_qn_id` = '$questionId' ");
    }
    
        if (isset($_FILES['answerImage'])) { 
        $uniquequesimage = [];

        $total = count($_FILES['answerImage']['name']);

        for ($i = 0; $i < $total; $i++) {
            
            $tmpFilePath = $_FILES['answerImage']['tmp_name'][$i];

            if ($tmpFilePath != "") 
            {
                $uniquedId = uniqidReal();
                $uniquequesimage[] = $uniquedId.$_FILES['answerImage']['name'][$i];
                $uniquequesimageURL[] = $imageURL. '/'.$uniquedId.$_FILES['answerImage']['name'][$i]; 
                $newFilePath = "../images/mcq-ofthe-day/".$uniquequesimage[$i];  
                if (move_uploaded_file( $tmpFilePath , $newFilePath)) 
                {
                    if(!empty($_FILES['answerImage']['name'][$i])){
                        $answerImage = implode(',', $uniquequesimageURL);   
                    }
                }
            }
        }
        if(!empty($_FILES['answerImage']['name'][0])){ 
            mysqli_query($dbconnection,"UPDATE `mcq_qa_of_the_day` SET `mcq_answer_desc_images` = '$answerImage' WHERE `mcq_qn_id` = '$questionId' ");

        } 
    }  
    $query = "UPDATE `mcq_qa_of_the_day` SET `mcq_qn_desc` = ? , `mcq_answers` = ?,`mcq_qn_correct_answer_num` = ?, `mcq_answer_desc` = ?,`mcq_reference` = ? WHERE `mcq_qn_id` = ? "; 
        $stmt = $dbconnection->prepare($query);
        $stmt->bind_param("ssssss",$question,$finalAns,$correctoption,$answerExplanation,$mcqReference,$questionId);
        
        $result = $stmt->execute();
        if($result){  
            $_SESSION['mcqofthedaysuccess']="Question Update Successfully"; 
            header("location:../mcq-qotd-list.php");
            exit(); 
        }else{ 
            $_SESSION['mcqofthedayerror']="Question Update Failed";  
            header("location:../mcq-qotd-list.php");

            exit(); 
        }
        
        $stmt->close();
}



if (isset($_POST['addSelectedMcqQuestion'])) {  
    $testId = $_POST['gtId'];  
    $mainModule = $_POST['mainsModule'];   
    $chlidModule = 131;

    foreach ($_POST['checkbox'] as $key => $value) { 
        $select = mysqli_query($dbconnection,"SELECT * FROM `qa` WHERE `mcq_qn_id`='$value'"); 
        if($row = mysqli_fetch_array($select)){
            $question = $row['mcq_qn_desc']; 
            $finalAns = $row['mcq_answers'];
            $correctoption = $row['mcq_qn_correct_answer_num'];
            $answerExplanation = $row['mcq_answer_desc'];
            $mcqReference  = $row["mcq_reference"];
            $questionImage  = $row["mcq_qn_desc_images"];
            $answerImage  = $row["mcq_answer_desc_images"];
            $mcqchildid = null;  

            $query = "INSERT INTO `mcq_qa_of_the_day`(`mcq_child_id`,`mcq_qn_desc`, `mcq_answers`, `mcq_qn_correct_answer_num`,`mcq_answer_desc`,`mcq_qn_desc_images`,`mcq_answer_desc_images`,`mcq_reference`) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? )" or die($dbconnection->error); 
            $stmt = $dbconnection->prepare($query);
            $stmt->bind_param("isssssss", $chlidModule, $question, $finalAns, $correctoption, $answerExplanation, $questionImage, $answerImage,$mcqReference); 
            $result = $stmt->execute();
            if($result){
                $last_id = $dbconnection->insert_id;
                mysqli_query($dbconnection,"UPDATE `mcq_qa_of_the_day` SET `ref_id`= CONCAT_WS('','MQ', `mcq_qn_id`, 'Q', `mcq_qn_id`)  WHERE `mcq_qn_id`='$last_id'");  
                $success= true;
            }
           
        }
    }


    if($success){
        $_SESSION['mcqofthedaysuccess']="Question Added Successfully";   
        header("location:../mcq-qotd-list.php");
        exit(); 
    }
}