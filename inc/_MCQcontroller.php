<?php 
include('./dbConfig.php');
include('functions.php');
session_start();
$sorting_order = "";
if(isset($_POST["addNewMainModule"]))
{ 
        $mainModuleName =  $_POST["mainModuleName"];
        $mainModuleDesc = $_POST["mainModuleDesc"];
        $status = $_POST["status"];
        $setSubModuleCount = $_POST["setSubModuleCount"];
        $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/MCQ/icons/'; 
        
        
        $sorting_order = getMaxRecord('mcq_module','sorting_order',$dbconnection) + 1;
        
        
        if (!empty($_FILES['questionImage']['name'])) {
            $image = ''; 
            $image = $_FILES['questionImage']['name'];
            $uniquequesimage = uniqidReal() . $image;
            $target = "../images/MCQ/icons/" . basename($uniquequesimage);
            $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
            $mcqIcon = $imageURL . $uniquequesimage;
        } 
        
        $query = "INSERT INTO `mcq_module` (`mcq_mname`,`mcq_description`,`sorting_order`,`mcq_total_submodule`,`mcq_micon`,`status`) VALUES (?,?,?,?,?,?)"; 

        $stmt = $dbconnection->prepare($query);


        $stmt->bind_param("ssssss", $mainModuleName, $mainModuleDesc, $sorting_order , $setSubModuleCount,  $mcqIcon,$status);

        $result = $stmt->execute(); 

        if ($result) {  
            $_SESSION['newmcqsuccess'] = "New MCQ Created successfully";
            header("location:../mcq-module-list.php");
            exit();
        } else {
            $_SESSION['newmcqerror'] = "Data Update Error";
            header("location:../mcq-module-list.php");
            exit();
        }

        $stmt->close();
}
if(isset($_POST["editNewMainModule"]))
{
        $mcqId = $_POST["mainModuleID"];
        $mainModuleName =  $_POST["mainModuleName"];
        $mainModuleDesc = $_POST["mainModuleDesc"]; 
        $status = $_POST["status"];
        $setSubModuleCount = $_POST["setSubModuleCount"];
        $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/MCQ/icons/'; 
        
          if (!empty($_FILES['questionImage']['name'])) { 
            $image = ''; 
            $image = $_FILES['questionImage']['name'];
            $uniquequesimage = uniqidReal() . $image;
            $target = "../images/MCQ/icons/" . basename($uniquequesimage);
            $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
            $mcqIcon = $imageURL . $uniquequesimage;
             
            mysqli_query($dbconnection,"UPDATE `mcq_module` SET `mcq_micon` = '$mcqIcon' WHERE `mcq_mid` = '$mcqId'");
        } 
        
        
        $query = "UPDATE `mcq_module`  SET `mcq_mname` =  ? ,`mcq_description`  = ? , `status` = ?  , `mcq_total_submodule` = ? WHERE `mcq_mid` = ? "; 
        
        $stmt = $dbconnection->prepare($query);
        
    
        $stmt->bind_param("sssss", $mainModuleName, $mainModuleDesc, $status ,$setSubModuleCount, $mcqId );
        
        $result = $stmt->execute(); 
        
            if ($result) {  
                $_SESSION['newmcqsuccess'] = "New MCQ Updated successfully";
                header("location:../mcq-module-list.php");
                exit();
            } else {
                $_SESSION['newmcqerror'] = "Data Update Error";
                header("location:../mcq-module-list.php");
                exit();
            }
            $stmt->close();
}
if(isset($_POST["addNewSubModule"]))
{ 
            $mainModuleId =  $_POST["mainModuleName"]; 
            
            $subModuleData = $_POST["subModuleid"];
    
            $subChildModuleData = $_POST["subChildModuleValue"];
            
            $smtext = $_POST["smtext"];
            
            $testopen = $_POST["testopen"];
            $qncount = "0";
            $rating = "4.8";
            
            // $smChildImage="https://www.ardentmds.com/api2/image/Mcq_Module_icons/eqipment.webp";
            
            $sorting_order = getMaxRecord('mcq_module','sorting_order',$dbconnection) + 1; 
            
            $checkSubNumeric =  is_numeric($subModuleData); 
            
            if($checkSubNumeric != 1)
            { 
                $subModuleId = getSubModuleID($mainModuleId,$dbconnection) + 1;
                $subModuleName = $subModuleData;
            } 
            else
            { 
                $subModuleId =  $subModuleData; 
                $subModuleName = fetchsubModuleData($dbconnection,'mcq_sm_name','mcq_sub_module',$subModuleData,'mcq_sm_id',$mainModuleId,'mcq_module_id_ref');
            }
            
            $query = "INSERT INTO `mcq_sub_module`(`mcq_module_id_ref`, `mcq_sm_id`, `mcq_sm_name`,`mcq_sm_child_name`, `mcq_sm_qn_count`,`mcq_sm_child_rating`,`mcq_sm_text`, `mcq_test_open`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"or die($dbconnection->error); 
            
            $stmt = $dbconnection->prepare($query) or die($dbconnection->error);
            
            $stmt->bind_param("ssssssss", $mainModuleId, $subModuleId, $subModuleName , $subChildModuleData,$qncount,$rating,$smtext,$testopen)or die($dbconnection->error);
            
            $result = $stmt->execute();
            
            if ($result) {  
                $_SESSION['newsubmcqsuccess'] = "New MCQ Created successfully"; 
                header("location:../mcq-sub-module-list.php");
                exit();
            } else {
                $_SESSION['newsubmcqerror'] = "Data Update Error";
                header("location:../mcq-sub-module-list.php");
                exit();
            }
    
            $stmt->close();
            
}
if(isset($_POST["editNewSubModule"]))
{
            $childModuleId = $_POST["childModuleId"];
            $mainSubModuleName =  $_POST["mainSubModuleName"];
            $childModuleName = $_POST["childModuleName"]; 
            $mcqAccessStatus = $_POST["mcqAccessStatus"];
            $mcqActiveStatus = $_POST["mcqActiveStatus"];
            $mcqSmText = $_POST["mcqSmText"];
            $imageURL = 'https://www.ardentmds.com/ardent-dashboard/images/MCQ/subBanner/'; 
            
             if (!empty($_FILES['questionImage']['name'])) { 
                 
            $image = ''; 
            $image = $_FILES['questionImage']['name'];
            $uniquequesimage = uniqidReal() . $image;
            $target = "../images/MCQ/subBanner/" . basename($uniquequesimage);
            $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
            $mcqIcon = $imageURL . $uniquequesimage;
            mysqli_query($dbconnection,"UPDATE `mcq_sub_module` SET `mcq_sm_child_image` = '$mcqIcon' WHERE `mcq_sm_child_id` = '$childModuleId'") or die(mysqli_error($dbconnection)) ;
            } 
        
            
            $query = "UPDATE `mcq_sub_module`  SET `mcq_sm_name` =  ? ,`mcq_sm_child_name`  = ? , `mcq_status` = ?  , `mcq_sm_text` = ? , `mcq_test_open` = ? WHERE `mcq_sm_child_id` = ? "; 
            
            $stmt = $dbconnection->prepare($query);
            
        
            $stmt->bind_param("ssssss", $mainSubModuleName, $childModuleName, $mcqActiveStatus ,$mcqSmText, $mcqAccessStatus,$childModuleId);
            
            $result = $stmt->execute(); 
            
            if ($result) {  
                    $_SESSION['newmcqsuccess'] = "SUb Module Edited successfully";
                    header("location:../mcq-sub-module-list.php"); 
                    exit();
                } else {
                    $_SESSION['newmcqsuccess'] = "Data Update Error"; 
                    header("location:../mcq-sub-module-list.php");
                    exit();
                }
                $stmt->close();
}
if(isset($_POST['deleteQuestion']))
{
                $questionId  = $_POST['questionID'];
            
                $delete = "UPDATE `qa` SET `status`=0  WHERE  `mcq_qn_id` ='$questionId'";
                $stmt = $dbconnection->prepare($delete) or die($dbconnection->error); 
                $result = $stmt->execute() or die($dbconnection->error);
                mysqli_query($dbconnection,"UPDATE `mcq_sub_module` SET `mcq_sm_qn_count` =(SELECT COUNT(*) FROM `qa` WHERE `qa`.`mcq_child_id` = `mcq_sub_module`.`mcq_sm_child_id` AND `qa`.`status`= 1)"); 
            
                if($result)
                {
                    echo'success';
                }else
                {
                    echo'fail';
                }
                $stmt->close();
}
