<?php
include('dbConfig.php');
include('functions.php'); 
session_start();
if (isset($_POST['add10minQuestion'])) {
    
    $tutor_Id = $_POST['chooseTutor']; 
    $uniqueId = $_POST['videoId'];
    $videoUrl = $_POST['videoUrl'];
    
        for ($i=0; $i < count($_POST['videoTimestamp']); $i++) { 
            $arr[]  =  $_POST['videoTimestamp'][$i].'-'.$_POST['videoTimestampText'][$i]; 
            $finalTimeStamp  = implode(',',$arr);
        }
         

     
        $query = "UPDATE  `10mins_videos` SET `minvideo_url` = ?  , `tutorId` = ?,`minvideo_title_timestamp` = ? WHERE `id`= ?";

        $stmt = $dbconnection->prepare($query);
   
   
        $stmt->bind_param("ssss", $videoUrl,$tutor_Id,$finalTimeStamp, $uniqueId);
        $result = $stmt->execute();
   
     
    
    if ($result) {  
        $_SESSION['10minsuccess'] = "Updated Successfully";
        header("location:../10-min-concept.php");
        exit();
    } else {

        $_SESSION['10minerror'] = "Data Update Error";
        header("location:../10-min-concept.php");
        exit();
    }

    $stmt->close();
}