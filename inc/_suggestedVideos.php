<?php
include('dbConfig.php');
include('functions.php'); 
session_start();
$imageURL = 'https://ardentmds.com/ardent-dashboard/images/suggestedvideos/';

if (isset($_POST['editSuggestedVideos'])) {
    
    $tutor_Id = $_POST['chooseTutor']; 
    $videoName = $_POST['videoName'];  
    $uniqueId = $_POST['videoId'];  
    $videoUrl = $_POST['videoUrl'];  

    for ($i=0; $i < count($_POST['videoTimestamp']); $i++) { 
             
        if (substr($_POST['videoTimestamp'][$i], -1) == ':') 
        {  
            $_POST['videoTimestamp'][$i] = substr_replace($_POST['videoTimestamp'][$i], "", -1);  
        } 
        $arr[]  =  $_POST['videoTimestamp'][$i].'-'.$_POST['videoTimestampText'][$i]; 
        $finalTimeStamp  = implode(',',$arr);
    }

        if (!empty($_FILES['questionImage']['name'])) 
        {
            $image = '';
            $image = $_FILES['questionImage']['name'];
            $uniquequesimage = uniqidReal() . $image;
            $target = "../images/suggestedvideos/" . basename($uniquequesimage);
            $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
            $questionImage = $imageURL . $uniquequesimage;
            mysqli_query($dbconnection,"UPDATE `suggested_videos` SET `video_front_image` = '$questionImage' WHERE `id` ='$uniqueId'"); 
    
        }

        $query = "UPDATE  `suggested_videos` SET  `sm_videos_url` = ?  , `tutorId` = ?  , vedio_sm_name = ?, `video_title_timestamp` = ? WHERE `id`= ?"; 
        $stmt = $dbconnection->prepare($query); 
        $stmt->bind_param("sssss",$videoUrl,$tutor_Id,$videoName,$finalTimeStamp,$uniqueId);
        $result = $stmt->execute();
    
    
    if ($result) {   
        $_SESSION['suggestedVideosSuccess'] = "Suggested Video Updated";
        header("location:../suggested-videos-list.php");
        exit();
    } else {

        $_SESSION['suggestedVideosError'] = "Data Update Error";
        header("location:../suggested-videos-list.php");
        exit();
    }

    $stmt->close();
}
if (isset($_POST['newSuggestedVideos'])) {
     
    $videoName = $_POST['videoName']; 
    $tutorId = $_POST['chooseTutor']; 
    $finalvideoURL = $_POST['videoUrl']; 
    $imageBg = "https://www.ardentmds.com/api1/image/videos_bg.png";
    

    for ($i=0; $i < count($_POST['videoTimestamp']); $i++) { 
             
        if (substr($_POST['videoTimestamp'][$i], -1) == ':') 
        {  
            $_POST['videoTimestamp'][$i] = substr_replace($_POST['videoTimestamp'][$i], "", -1);  
        }

        $arr[]  =  $_POST['videoTimestamp'][$i].'-'.$_POST['videoTimestampText'][$i]; 
        $finalTimeStamp  = implode(',',$arr);
    }
         
        if (!empty($_FILES['questionImage']['name'])) 
        {
            $image = '';
            $image = $_FILES['questionImage']['name'];
            $uniquequesimage = uniqidReal() . $image;
            $target = "../images/suggestedvideos/" . basename($uniquequesimage);
            $success = move_uploaded_file($_FILES['questionImage']['tmp_name'], $target);
            $questionImage = $imageURL . $uniquequesimage; 
        }
 
        
        $exequery =  mysqli_query($dbconnection,"SELECT * FROM `tutordetails` WHERE `tutorId`='$tutorId'");
        if($row = mysqli_fetch_array($exequery)){
            $tutorName =  $row['tutorName'];
            $tutorDesc =  $row['tutorDesc'];
            $tutorImage =  $row['tutorImage'];
        }
         
        
        $query = "INSERT INTO `suggested_videos`( `vedio_sm_name`, `video_front_image`,  `sm_videos_url`,`tutorId`,`tutor_name`, `tutor_info`, `tutor_image`, `image_bg`, `video_title_timestamp`) VALUES ( ?, ?, ?, ?,?, ?, ?, ?, ? )" or die($dbconnection->error) ;

        $stmt = $dbconnection->prepare($query) or die($dbconnection->error) ;


        $stmt->bind_param("sssssssss", $videoName,$questionImage,$finalvideoURL,$tutorId, $tutorName,$tutorDesc, $tutorImage ,$imageBg,$finalTimeStamp ) or die($dbconnection->error) ;
        $result = $stmt->execute()or die($dbconnection->error) ;
 
    if ($result) {   
        $_SESSION['suggestedVideosSuccess'] = "Suggested Video Added";
        header("location:../suggested-videos-list.php");
        exit();
    } else { 
        $_SESSION['suggestedVideosError'] = "Data Update Error";
        // header("location:../suggested-videos-list.php");
        exit();
    }

    $stmt->close();
}