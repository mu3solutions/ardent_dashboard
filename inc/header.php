<header class="page-topbar" id="header">
      <div class="navbar "> 
        <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple no-shadow">
         
          <div class="nav-wrapper">
            <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>
            <form action="./search.php" method="GET">
              <input class="header-search-input z-depth-2" type="text" name="q" placeholder="Search Question By Reference ID" value="<?php if (isset($_GET['q'])) {
                echo $_GET['q'];
              } ?>">
            </form>
              <ul class="search-list collection display-none"></ul>
            </div>
            <ul class="navbar-list right">

              <li class="hide-on-med-and-down">
                <a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li> 
              <li>
                <a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="images/avatar/avatar-7.png" alt="avatar"><i></i></span></a>
              </li> 
            </ul>
             
            <ul class="dropdown-content" id="profile-dropdown">
              <li><a class="grey-text text-darken-1" href="user-profile-page.html"><i class="material-icons">person_outline</i> Profile</a></li>

              <li><a class="grey-text text-darken-1" href="./logout.php"><i class="material-icons">keyboard_tab</i> Logout</a></li>
            </ul>
          </div> 
        </nav>
      </div>
    </header>  