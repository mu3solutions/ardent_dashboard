<!-- BEGIN: SideNav-->
<?php $activePage = basename($_SERVER['PHP_SELF']); ?>
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-rounded">
        <div class="brand-sidebar">
            <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="home-dashboard.php"><img class="hide-on-med-and-down" src="images/logo/014.png" alt="materialize logo" /><img class="show-on-medium-and-down hide-on-med-and-up" src="images/logo/014.png" alt="materialize logo" /><span class="logo-text hide-on-med-and-down">Ardent</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
        </div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
            <li class="navigation-header"><a class="navigation-header-text">Applications</a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="bold <?php if($activePage =='home-dashboard.php'){echo 'active';} ?>"><a class=" <?php if($activePage =='home-dashboard.php'){echo 'active';} ?> waves-effect waves-cyan " href="home-dashboard.php"><i class="material-icons">settings_input_svideo</i><span class="menu-title">Dashboard</span></a>
            </li>
             <li class="navigation-header"><a class="navigation-header-text">  Home Screen </a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="bold   <?php if($activePage =='top-banner-list.php' || $activePage =='add-new-banner.php'){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan "  ><i class="material-icons">wallpaper
            </i><span class="menu-title" >Top Slider</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li class="<?php if($activePage =='top-banner-list.php'){echo 'active';} ?>"  ><a class="<?php if($activePage =='top-banner-list.php'){echo 'active';} ?>"   href="top-banner-list.php"><i class="material-icons">radio_button_unchecked</i><span>Top Slider List</span></a>
                        </li> 
                        <li    class="<?php if($activePage =='add-new-banner.php'){echo 'active';} ?>" ><a class="<?php if($activePage =='add-new-banner.php'){echo 'active';} ?>"  href="add-new-banner.php"><i class="material-icons">radio_button_unchecked</i><span>Add New Slider </span></a>
                        </li>
                    </ul>
                </div>
            </li>
            
            <li class="navigation-header"><a class="navigation-header-text">Pages </a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="bold <?php if($activePage =='student-list.php' || $activePage =='add-new-student.php' || $activePage =='edit-student.php'){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan " ><i class="material-icons">face</i><span class="menu-title" >Student Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li class="<?php if($activePage =='student-list.php'){echo 'active';} ?>"><a class="<?php if($activePage =='student-list.php'){echo 'active';} ?>" href="student-list.php"><i class="material-icons">radio_button_unchecked</i><span>View Student's
                                    List</span></a>
                        </li>
                        <li class="<?php if($activePage =='add-new-student.php'  || $activePage =='edit-student.php'){echo 'active';} ?>"><a class="<?php if($activePage =='add-new-student.php' || $activePage =='edit-student.php'){echo 'active';} ?>" href="add-new-student.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Student</span></a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="bold <?php if($activePage =='3-min-challenges.php' || $activePage =='add-new3minchallenge.php' || $activePage =='cliniccal-vignets-list.php' || $activePage  == 'add-new-clinical.php' || $activePage == '10-min-concept.php' || $activePage == 'trending-videos.php' || $activePage == 'quote-list.php' || $activePage == 'add-new-quote.php' || $activePage =='edit-clinical.php' || $activePage =='edit-trendingvideos.php'  || $activePage =='edit-10minconcept.php' || $activePage =='add-3minedited.php'  || $activePage == 'edit-quote.php'  ){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan " ><i class="material-icons">photo_filter</i><span class="menu-title" data-i18n="Menu levels">Ardent Nuggets</span></a>
                <div class="collapsible-body" >
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a class="collapsible-header waves-effect waves-cyan" ><i class="material-icons">radio_button_unchecked</i><span data-i18n="3 Min Challenge">3 Min Challenge</span></a>
                            <div class="collapsible-body">
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li class="<?php if($activePage =='3-min-challenges.php'){echo 'active';} ?>"><a class="<?php if($activePage =='3-min-challenges.php'){echo 'active';} ?>" href="3-min-challenges.php"><i class="material-icons">radio_button_unchecked</i><span>3-Min-Challenge List</span></a>
                                    </li>
                                    <li class="<?php if($activePage =='add-new3minchallenge.php' || $activePage =='add-3minedited.php'  ){echo 'active';} ?>"><a class="<?php if($activePage =='add-new3minchallenge.php'|| $activePage =='add-3minedited.php'  ){echo 'active';} ?>" href="add-new3minchallenge.php" ><i class="material-icons">radio_button_unchecked</i><span data-i18n="Third level">Add New 3-min Challenge</span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect waves-cyan" ><i class="material-icons">radio_button_unchecked</i><span data-i18n="Second level child">Clinical Vignettes</span></a>
                            <div class="collapsible-body" >
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li class="<?php if($activePage =='cliniccal-vignets-list.php'){echo 'active';} ?>"><a class="<?php if($activePage =='cliniccal-vignets-list.php'){echo 'active';} ?>" href="cliniccal-vignets-list.php" ><i class="material-icons">radio_button_unchecked</i><span> Clinical Vignettes List </span></a>
                                    </li>
                                    <li class="<?php if($activePage =='add-new-clinical.php' || $activePage =='edit-clinical.php'){echo 'active';} ?>"><a class="<?php if($activePage =='add-new-clinical.php'  || $activePage =='edit-clinical.php'){echo 'active';} ?>" href="add-new-clinical.php"><i class="material-icons">radio_button_unchecked</i><span>Add Clinical Vignettes</span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class=" bold <?php if($activePage =='10-min-concept.php' || $activePage =='edit-10minconcept.php' ){echo 'active';} ?>"><a  class=" collapsible-header waves-effect waves-cyan" ><i class="material-icons">radio_button_unchecked</i><span>10 Min Concept</span></a>
                            <div class="collapsible-body" > 
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li  class="<?php if($activePage =='10-min-concept.php' || $activePage =='edit-10minconcept.php'){echo 'active';} ?>"><a  class="<?php if($activePage =='10-min-concept.php' || $activePage =='edit-10minconcept.php'){echo 'active';} ?>" href="10-min-concept.php"><i class="material-icons">radio_button_unchecked</i><span>Edit 10 Mins Concept</span></a>
                                    </li> 
                                </ul>
                            </div>
                        </li>

                        <li class="bold <?php if($activePage =='trending-videos.php'  || $activePage =='edit-trendingvideos.php'){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan" ><i class="material-icons">radio_button_unchecked</i><span>Trending Videos</span></a>
                            <div class="collapsible-body"> 
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li class="<?php if($activePage =='trending-videos.php'   || $activePage =='edit-trendingvideos.php'  ){echo 'active';} ?>"><a class="<?php if($activePage =='trending-videos.php'  || $activePage =='edit-trendingvideos.php' ){echo 'active';} ?>" href="trending-videos.php"><i class="material-icons">radio_button_unchecked</i><span>Trending Videos List</span></a>
                                    </li> 
                                </ul>
                            </div>
                        </li>

                        <li  class="bold"><a class="collapsible-header waves-effect waves-cyan" ><i class="material-icons">radio_button_unchecked</i><span>Quote Of The Day</span></a>
                            <div class="collapsible-body"> 
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li class="<?php if($activePage =='quote-list.php'  || $activePage == 'edit-quote.php'){echo 'active';} ?>"><a class="<?php if($activePage =='quote-list.php'  || $activePage == 'edit-quote.php' ){echo 'active';} ?>"  href="quote-list.php"><i class="material-icons">radio_button_unchecked</i><span>Quote Of The Day List</span></a>
                                    </li> 
                                    <li  class="  <?php if($activePage =='add-new-quote.php'){echo 'active';} ?> " ><a  class="  <?php if($activePage =='add-new-quote.php'){echo 'active';} ?> " href="add-new-quote.php"><i class="material-icons">radio_button_unchecked</i><span>Add New Quote</span></a>
                                    </li> 
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="bold <?php if($activePage =='mcq-module-list.php' || $activePage =='mcq-sub-module-list.php' || $activePage =='add-new-mcq-questions.php' || $activePage =='add-mcqeditedquestion.php'  || $activePage =='edit-mcq-question.php'   || $activePage =='mcq-questions-list.php'  ){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan " ><i class="material-icons">assignment</i><span class="menu-title" >MCQ Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li  class="  <?php if($activePage =='mcq-module-list.php'){echo 'active';} ?>" ><a  class="  <?php if($activePage =='mcq-module-list.php'){echo 'active';} ?>" href="mcq-module-list.php"><i class="material-icons">radio_button_unchecked</i><span>MCQ Main Module</span></a>
                        </li>
                        <li class="  <?php if($activePage =='mcq-sub-module-list.php'){echo 'active';} ?>" ><a class="  <?php if($activePage =='mcq-sub-module-list.php'){echo 'active';} ?>" href="mcq-sub-module-list.php"><i class="material-icons">radio_button_unchecked</i><span>MCQ Sub Module</span></a>
                        </li>
                        <li class="  <?php if($activePage =='add-new-mcq-questions.php'  || $activePage =='add-mcqeditedquestion.php'){echo 'active';} ?>"><a class="  <?php if($activePage =='add-new-mcq-questions.php'  || $activePage =='add-mcqeditedquestion.php'){echo 'active';} ?>" href="add-new-mcq-questions.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Questions</span></a>
                        </li>
                        <li class="  <?php if($activePage =='edit-mcq-question.php' ||  $activePage =='mcq-questions-list.php'){echo 'active';} ?>"><a class="  <?php if($activePage =='edit-mcq-question.php'  || $activePage =='mcq-questions-list.php'){echo 'active';} ?>" href="mcq-questions-list.php"><i class="material-icons">radio_button_unchecked</i><span>Edit MCQ Questions</span></a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="bold  <?php if($activePage =='grand-test-list.php' || $activePage =='add-new-gtquestions.php' || $activePage =='add-new-grand-test.php' || $activePage =='add-gteditedquestion.php' ){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan " ><i class="material-icons">assignment_turned_in</i><span class="menu-title" >Grand Test Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li  class="<?php if($activePage =='grand-test-list.php'){echo 'active';} ?>" ><a  class="<?php if($activePage =='grand-test-list.php'){echo 'active';} ?>" href="grand-test-list.php"><i class="material-icons">radio_button_unchecked</i><span>Grand Test List</span></a>
                        </li> 
                        <li  class="<?php if($activePage =='add-new-grand-test.php'){echo 'active';} ?>" ><a  class="<?php if($activePage =='add-new-grand-test.php'){echo 'active';} ?>" href="add-new-grand-test.php"><i class="material-icons">radio_button_unchecked</i><span>Add New Grand Test</span></a>
                        </li> 
                        <li  class="<?php if($activePage =='add-new-gtquestions.php'  || $activePage =='add-gteditedquestion.php'){echo 'active';} ?>" ><a  class="<?php if($activePage =='add-new-gtquestions.php'  || $activePage =='add-gteditedquestion.php'){echo 'active';} ?>" href="add-new-gtquestions.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Questions</span></a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="bold <?php if($activePage =='suggested-videos-list.php' || $activePage =='add-new-suggested-video.php' || $activePage =='edit-suggestvideo.php' ){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan " ><i class="material-icons">ondemand_video
            </i><span class="menu-title" >Suggested Videos</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li class="<?php if($activePage =='suggested-videos-list.php' || $activePage =='edit-suggestvideo.php' ){echo 'active';} ?>"><a class="<?php if($activePage =='suggested-videos-list.php' || $activePage =='edit-suggestvideo.php'){echo 'active';} ?>"  href="suggested-videos-list.php"><i class="material-icons">radio_button_unchecked</i><span>Suggested Videos List</span></a>
                        </li> 
                        <li  class="<?php if($activePage =='add-new-suggested-video.php'){echo 'active';} ?>" ><a  class="<?php if($activePage =='add-new-suggested-video.php'){echo 'active';} ?>" href="add-new-suggested-video.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Suggested Video</span></a>
                        </li>
                    </ul>
                </div>
            </li> 
            <li class="bold <?php if($activePage =='mcq-qotd-list.php' || $activePage =='add-new-mcq-qotd.php' || $activePage =='edit-mcqqotd.php'){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan " ><i class="material-icons">assignment
            </i><span class="menu-title" >MCQ of the day</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li class="<?php if($activePage =='mcq-qotd-list.php' || $activePage =='edit-mcqqotd.php'){echo 'active';} ?>"><a class="<?php if($activePage =='mcq-qotd-list.php'|| $activePage =='edit-mcqqotd.php'){echo 'active';} ?>"  href="mcq-qotd-list.php"><i class="material-icons">radio_button_unchecked</i><span> MCQ Of the day List </span></a>
                        </li> 
                        <li  class="<?php if($activePage =='add-new-mcq-qotd.php'){echo 'active';} ?>" ><a  class="<?php if($activePage =='add-new-mcq-qotd.php'){echo 'active';} ?>" href="add-new-mcq-qotd.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                        New MCQ Of the day</span></a>
                        </li>
                    </ul>
                </div>
            </li>  
            <li class="bold <?php if($activePage =='videos-module-list.php' || $activePage =='video-sub-module-list.php' || $activePage =='add-new-videos.php' || $activePage =='edit-video-sub-module.php'){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan " ><i class="material-icons">ondemand_video</i><span class="menu-title" >Video Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li  class="  <?php if($activePage =='videos-module-list.php'){echo 'active';} ?>" ><a  class="  <?php if($activePage =='videos-module-list.php'){echo 'active';} ?>" href="videos-module-list.php"><i class="material-icons">radio_button_unchecked</i><span>Video Main Module</span></a>
                        </li>
                        <li class="  <?php if($activePage =='video-sub-module-list.php'|| $activePage =='edit-video-sub-module.php'){echo 'active';} ?>" ><a class="  <?php if($activePage =='video-sub-module-list.php' || $activePage =='edit-video-sub-module.php'){echo 'active';} ?>" href="video-sub-module-list.php"><i class="material-icons">radio_button_unchecked</i><span>Videos Module List</span></a>
                        </li>
                        <li class="  <?php if($activePage =='add-new-videos.php'){echo 'active';} ?>"><a class="  <?php if($activePage =='add-new-videos.php'){echo 'active';} ?>" href="add-new-videos.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Video</span></a>
                        </li> 
                    </ul>
                </div>
            </li>
            <!--<li class="bold <?php if($activePage =='microcourses-list.php' || $activePage =='edit-micro-course.php' || $activePage =='add-new-micro-course.php' ){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan " ><i class="material-icons">ondemand_video</i><span class="menu-title" >Micro Courses Module</span> </a>-->
            <!--    <div class="collapsible-body">-->
            <!--        <ul class="collapsible collapsible-sub" data-collapsible="accordion">-->
            <!--            <li  class="<?php if($activePage =='microcourses-list.php' || $activePage =='edit-micro-course.php'){echo 'active';} ?>" ><a  class="  <?php if($activePage =='microcourses-list.php' || $activePage =='edit-micro-course.php'){echo 'active';} ?>" href="microcourses-list.php"><i class="material-icons">radio_button_unchecked</i><span>Micro-Courses List</span></a>-->
            <!--            </li> -->
            <!--            <li class="<?php if( $activePage =='add-new-micro-course.php' ){echo 'active';} ?>"><a class="  <?php if( $activePage =='add-new-micro-course.php' ){echo 'active';} ?>" href="add-new-micro-course.php"><i class="material-icons">radio_button_unchecked</i><span>Add-->
            <!--                        New Micro Course</span></a>-->
            <!--            </li> -->
            <!--        </ul>-->
            <!--    </div>-->
            <!--</li>-->
            <li class="bold <?php if($activePage =='subscription-plan-list.php' || $activePage =='edit-plan-module.php' ){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan"><i class="material-icons">subscriptions</i><span class="menu-title" >Subscription Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li  class="  <?php if($activePage =='subscription-plan-list.php' || $activePage =='edit-plan-module.php'){echo 'active';} ?>" ><a  class="  <?php if($activePage =='subscription-plan-list.php' || $activePage =='edit-plan-module.php'){echo 'active';} ?>" href="subscription-plan-list.php"><i class="material-icons">radio_button_unchecked</i><span>Subscription Plan List</span></a>
                        </li> 
                    </ul>
                </div>
            </li> 
            <li class="bold <?php if($activePage =='coupons-list.php' || $activePage =='add-new-coupon.php' || $activePage =='edit-coupon.php'){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan"><i class="material-icons">chrome_reader_mode
            </i><span class="menu-title" >Coupons Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li class="<?php if($activePage =='coupons-list.php' || $activePage =='edit-coupon.php'){echo 'active';} ?>" ><a class="<?php if($activePage =='coupons-list.php'  || $activePage =='edit-coupon.php'){echo 'active';} ?>" href="coupons-list.php"><i class="material-icons">radio_button_unchecked</i><span>Coupons List</span></a>
                        </li> 
                        <li class="<?php if($activePage =='add-new-coupon.php'){echo 'active';} ?>"><a class="<?php if($activePage =='add-new-coupon.php'){echo 'active';} ?>" href="add-new-coupon.php"><i class="material-icons">radio_button_unchecked</i><span>Add New Coupon</span></a>
                        </li> 
                    </ul>
                </div>
            </li>
            <li class="bold <?php if($activePage =='tutor-list.php' || $activePage =='add-new-tutor.php'){echo 'active';} ?>"><a class="collapsible-header waves-effect waves-cyan"><i class="material-icons">account_circle
            </i><span class="menu-title" >Tutor Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li   class="<?php if($activePage =='tutor-list.php'){echo 'active';} ?>" ><a class="<?php if($activePage =='tutor-list.php'){echo 'active';} ?>" href="tutor-list.php"><i class="material-icons">radio_button_unchecked</i><span>Tutor List</span></a>
                        </li> 
                        <li class="<?php if($activePage =='add-new-tutor.php'){echo 'active';} ?>"><a class="<?php if($activePage =='add-new-tutor.php'){echo 'active';} ?>" href="add-new-tutor.php"><i class="material-icons">radio_button_unchecked</i><span>Add New Tutor  </span></a>
                        </li>
                    </ul>
                </div>
            </li>
            
            <li class="navigation-header"><a class="navigation-header-text"> notification </a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="bold <?php if($activePage =='edit-live-quiz.php' || $activePage =='ff-quiz.php' || $activePage == 'add-new-ff-questions.php'  || $activePage ==  'ff-results-list.php'   || $activePage ==  'view-ff-results.php' ){echo 'active';} ?>" ><a class="collapsible-header waves-effect waves-cyan " ><i class="material-icons">wifi_tethering
              </i><span class="menu-title" >Live Quiz</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li class="<?php if($activePage =='edit-live-quiz.php'){echo 'active';} ?>"><a class="<?php if($activePage =='edit-live-quiz.php'){echo 'active';} ?>"  href="edit-live-quiz.php"><i class="material-icons">radio_button_unchecked</i><span> Edit Live Quiz </span></a>
                        </li>
                        <li class="<?php if($activePage =='ff-quiz.php'){echo 'active';} ?>" ><a class="bold <?php if($activePage =='ff-quiz.php'){echo 'active';} ?>"  href="ff-quiz.php"><i class="material-icons">radio_button_unchecked</i><span>FF Questions List</span></a>
                        </li> 
                        <li class="<?php if($activePage == 'add-new-ff-questions.php'){echo 'active';} ?>"><a class="<?php if($activePage == 'add-new-ff-questions.php'){echo 'active';} ?>"  href="add-new-ff-questions.php"><i class="material-icons">radio_button_unchecked</i><span>Add FF Questions</span></a>
                        </li> 
                        <li class="<?php if($activePage == 'view-ff-results.php' || $activePage ==  'ff-results-list.php' ){echo 'active';} ?>"><a class="<?php if($activePage == 'view-ff-results.php' || $activePage ==  'ff-results-list.php' ){echo 'active';} ?>"  href="ff-results-list.php"><i class="material-icons">radio_button_unchecked</i><span>View FF Results</span></a>
                        </li> 
                    </ul>
                </div>
            </li>
            <li class="<?php if($activePage =='send-push-notifications.php'){echo 'active';} ?> "><a class=" <?php if($activePage =='send-push-notifications.php'){echo 'active';} ?> waves-effect waves-cyan " href="send-push-notifications.php"><i class="material-icons">notifications</i><span class="menu-title">Send Notification</span></a>
            </li>
              <li class="bold <?php if($activePage =='faq-list.php'  || $activePage =='add-new-faq.php'){echo 'active';} ?>" ><a class="collapsible-header waves-effect waves-cyan " ><i class="material-icons">description
              </i><span class="menu-title" >FAQ </span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li class="<?php if($activePage =='faq-list.php' ){echo 'active';} ?>"><a class="<?php if($activePage =='faq-list.php'){echo 'active';} ?>"  href="faq-list.php"><i class="material-icons">radio_button_unchecked</i><span> FAQ List </span></a>
                        </li>
                        <li class="<?php if($activePage =='add-new-faq.php'){echo 'active';} ?>" ><a class="bold <?php if($activePage =='add-new-faq.php'){echo 'active';} ?>"  href="add-new-faq.php"><i class="material-icons">radio_button_unchecked</i><span>Add New FAQ</span></a>
                        </li>  
                    </ul>
                </div>
            </li> 
           
        </ul>
        <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
  </aside>
    <!-- END: SideNav-->