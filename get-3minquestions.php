<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Clinical Vignets | Ardent MDS</title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
     
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/sweetalert/sweetalert.css">
     
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="css/pages/data-tables.min.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->

    <style>
    .odd{
        border-bottom : 1px solid #ccc;
    }
    </style>
</head>
<!-- END: Head-->

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">

                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>3 Mins Challenge List</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                                </li>
                                <li class="breadcrumb-item active">3 Mins Challenge List
                                </li>
                            </ol>
                        </div>
                        <div class="col s3 m3 right">
                            <a href="add-new3minchallenge.php"> 
                                <button class=" btn btn-block waves-effect waves-light amber darken-4">
                                    Add New Question
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables"> 
                        <div class="row">
                            <div class="col s12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="row">
                                            <div class="row">
                    <?php if (isset($_SESSION['nuggetssuccess'])) {?>
                        <div class="col m12">
                            <div class="card-alert card green">
                                    <div class="card-content white-text">
                                <p><?php echo $_SESSION['nuggetssuccess']; ?></p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                </div>
                <?php
                }
                unset($_SESSION['nuggetssuccess']);
                if (isset($_SESSION['nuggetserror'])) {
                ?> 
                <div class="col m6">
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p><?php echo $_SESSION['nuggetserror']; ?></p>
                        </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                </div>
                                <?php
                                }
                                unset($_SESSION['quotelisterror']);
                                ?>
                                </div>
                                <div class="col s12">
                                    <table id="page-length-option" class="table">
                                    <thead>
                                        <tr>
                                            <th>Add Question</th>
                                            <th>Test Date</th> 
                                            <th>Test Id</th> 
                                            <th>Question Count</th> 
                                            <th>Test Name</th>  
                                        </tr>
                                    </thead>
                                <tbody>
        <?php
        $answers = array();
        $selectQuery = "SELECT * FROM `tbl_nuggets_qa` GROUP BY `test_date` ORDER BY `nuggets_qn_id` DESC";
        $exeQuery  = mysqli_query($dbconnection, $selectQuery);
        $i = mysqli_num_rows($exeQuery);
        
        while ($row = mysqli_fetch_array($exeQuery)) {
        $answers = explode(',', $row['nuggets_answers']);
        $questionCount =  nuggetsQuestionCount($row['test_date'],$dbconnection);
        if($questionCount < 10)
        { 
            $balanceQuestion =  10 - $questionCount ; 
            $chipColor  = 'chip red lighten-5';
            $chipTextColor  = 'red-text';
            $chipText  = 'Warning : Only ' .$questionCount.' questions present , Add '. $balanceQuestion. ' More .';
        }else if ($questionCount >= 10) 
        {
            $chipColor  = 'chip green lighten-5';
            $chipTextColor  = 'green-text';
            $chipText  = 'All Ok : 10 questions present'; 
        }
                                
        ?>
            <tr>
                <td>
                <a href="get-3minquestions.php?testDate=<?php echo $row['test_date']; ?>"><i class="material-icons">edit</i> </a> 
                </td> 

                
                <td> 
                    <?php echo truncate($row['test_date'],30); ?>
                </td>
                <td><?php echo truncate($row['nuggets_testId'],30); ?></td>  
                <td>
                    <a href="add-3minedited.php?testDate=<?php echo $row['test_date']; ?> "> 
                    <span class="<?php echo $chipColor; ?>"><span class="<?php echo $chipTextColor; ?>"><?php echo $chipText; ?></span></span>
                    </a> 
                </td> 
                <td><?php echo  'Nuggets Test ' .$i. ' (From Q Bank)'; ?> </td>
                </tr>
        <?php $i--; }   ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- START RIGHT SIDEBAR NAV -->
                    <!-- END RIGHT SIDEBAR NAV -->
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div> 
    <script src="js/vendors.min.js"></script>
    <script src="js/plugins.min.js"></script> 
    <script src="js/scripts/data-tables.min.js"></script>   
    <script src="vendors/data-tables/js/jquery.dataTables.min.js"></script> 
    <script src="vendors/sweetalert/sweetalert.min.js"></script> 
    <script src="js/scripts/ui-alerts.min.js"></script> 
    <script src="js/custom/custom-script.min.js"></script> 
    <script>
    $(function(){
        
        $("#page-length-option").DataTable({
            responsive: false,
            ordering: true,
            scrollX: true,
            scrollY: false 
        })
    });
       
    </script> 
</body> 
</html>