<?php 
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$childModule='';
if($_GET['mid']){
    $childModule =  decryptData($_GET['mid']);  
}
?>

<!DOCTYPE html>   
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> 
    <title>Edit Sub Module | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head>
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->
  <!-- END: SideNav-->


    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Edit Sub Module</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="mcq-sub-module-list.php"> Sub Module List</a>
                  </li>
                  <li class="breadcrumb-item active">Edit Main Module
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
          <div class="row"> 
          <form action="./inc/_MCQcontroller.php" method="POST" enctype="multipart/form-data"> 
            <?php 
            if(!empty($childModule)) {
                $sql = "SELECT * FROM `mcq_sub_module`  WHERE `mcq_sm_child_id` = '$childModule'";
                $result = mysqli_query($dbconnection, $sql);
                if(mysqli_num_rows($result) > 0){

                  if  ($row = mysqli_fetch_array($result)) {  
                
            ?>   
            <div class="row"> 
            <input type="hidden" name="childModuleId" value="<?php echo $childModule; ?>">
                <div class="input-field col m6 s12">
                    <input id="mainSubModuleName" type="text" name="mainSubModuleName" value="<?php echo $row['mcq_sm_name']; ?>" required>
                    <label for="mainSubModuleName">Sub Module Name</label>
                </div>
                <div class="input-field col m6 s12">
                    <input id="childModuleName" type="text" name="childModuleName" value="<?php echo $row['mcq_sm_child_name']; ?>" required>
                    <label for="childModuleName">Child Module Name</label>
                </div>
               
            </div>  

            <div class="row mt-3">  
            <div class="col m6 s12">
                    <label>Set Access Status</label>
                      <select name="mcqAccessStatus" required>
        <option value="0" <?php if($row['mcq_test_open']=='0'){echo 'selected';}?> > Opened </option>
        <option value="1" <?php if($row['mcq_test_open']=='1'){echo 'selected';}?> > Locked </option> 
                      </select>
                </div>
                <div class="col m6 s12">
                    <label>Set Active Status</label>
                      <select name="mcqActiveStatus" required>
        <option value="1" <?php if($row['mcq_status']=='1'){echo 'selected';}?> > Active </option>
        <option value="0" <?php if($row['mcq_status']=='0'){echo 'selected';}?> > Inactive </option> 
                      </select>
                </div>
            </div>  
            <div class="row mt-3">  
                <div class="input-field col s12">
                    <textarea id="mcqSmText" class="materialize-textarea" name="mcqSmText" required><?php echo $row['mcq_sm_text']; ?></textarea>
                    <label for="mcqSmText">Child Module Description</label>
                </div> 
            </div>
            <div class="row ">  
                    <div class="col m2 s12 mt-3">
                        <label>
                            <span class="black-text"> Upload Banner Image : </span>
                        </label>
                        
                    </div> 
                    <div class="col m6 s12 file-field inline input-field">  
                      <div class="btn float-right inline ardent-orange">
                            <span>Choose Image</span>
                            <input type="file" id="file" name="questionImage" onchange="readImagURL(this,1184,776,600,300);" accept="image/x-png,image/jpeg" />
                      </div>
                      <div class="file-path-wrapper">
                            <input class="file-path validate valid" type="text" id="fileWrapper">
                            <span class="helper-text ardent-text" >( Image Width and Height must be 1184 * 776 in pixels  )</span>
                      </div>
                    </div>
            </div>    
            
             <div class="row"> 
                    <div class="col m6">
                      <div class="wrapper ">
                        <p>Current Image : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="<?php  echo $row['mcq_sm_child_image'];?>" height="300" width="600" alt="No Image"  /> 
                      </div>
                    </div>
              </div>
            
            <div class="row">   
                <div class="input-field col s12 center"> 
                  <button class="btn ardent-orange waves-effect waves-light" type="submit" name="editNewSubModule">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div>
            <?php }  ?> 
            <?php  }else { ?>
             <div class="row center-align">  
               <div class="col m12 s12">
                   <h3 class="text-center"> Your Link is Broken</h3>
                   <img class="z-depth-4 circle" width="100" height="100" src="./images/favicon/no-access-icon.png " alt="">
               </div> 
           </div>
            <?php }  } else { ?>
            <div class="row center-align">  
               <div class="col m12 s12">
                   <h3 class="text-center"> Your Link is Broken</h3>
                   <img class="z-depth-4 circle" width="100" height="100" src="./images/favicon/no-access-icon.png " alt="">
               </div> 
           </div>

            <?php } ?>
          </form>    
        </div>
      </div>
    </div>
  </div>
       
    </div>
 
    <!-- END: Footer-->
   <!-- BEGIN VENDOR JS-->
   <script src="js/vendors.min.js"></script>   
    <script src="js/plugins.min.js"></script> 
    <script src="vendors/sweetalert/sweetalert.min.js"></script>
    <script src="js/custom/custom-script.min.js"></script>
  </body> 
</html>