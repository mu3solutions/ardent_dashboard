<?php 
include('./inc/dbConfig.php');
include('./inc/functions.php');  
include('./inc/authenticate.php');  
$date = Date('d-m-Y');
?>

<!DOCTYPE html>
  
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> 
    <title>Home Dashboard Screen | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css"> 
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
    <link rel="stylesheet" type="text/css" href="css/pages/dashboard-modern.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css"> 
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns" data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Welcome Dr.Karthikeyan , </span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li> 
                  <li class="breadcrumb-item active">Dashboard
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div> 
        <div class="row"> 
        <div class="col s12 m6 l6 xl3">
            <div class="card  z-depth-3  gradient-shadow min-height-100 black-text animate fadeLeft border-radius-10 ">
               <div class="padding-4">
                  <div class="row">
                     <div class="col s7 m7">
                        <h5 class="mb-0 black-text"> # <?php echo getMaxCount('user',null,$dbconnection); ?></h5> 
                        <p>Registered Users</p>
                     </div>
                     <div class="col s5 m5 right-align">
                        <i class="material-icons background-round ardent-text mt-5">perm_identity</i>
                      </div>
                  </div>
               </div>
            </div>
         </div> 
         <div class="col s12 m6 l6 xl3">
            <div class="card  z-depth-3  gradient-shadow min-height-100 black-text animate fadeLeft border-radius-10 ">
               <div class="padding-4">
                  <div class="row">
                     <div class="col s7 m7">
                        <h5 class="mb-0 black-text"> # <?php echo getMaxCount('qa',null,$dbconnection); ?></h5> 
                        <p>MCQ Questions Count</p>
                     </div>
                     <div class="col s5 m5 right-align">
                        <i class="material-icons background-round ardent-text mt-5">timeline</i>
                      </div>
                  </div>
               </div>
            </div>
         </div> 
         <div class="col s12 m6 l6 xl3">
            <div class="card  z-depth-3  gradient-shadow min-height-100 black-text animate fadeRight border-radius-10 ">
               <div class="padding-4">
                  <div class="row">
                     <div class="col s7 m7">
                        <h5 class="mb-0 black-text"> # <?php echo getMaxCount('qa_grand_test',null,$dbconnection); ?></h5> 
                        <p>Grand Test Questions</p>
                     </div>
                     <div class="col s5 m5 right-align">
                        <i class="material-icons background-round ardent-text mt-5">assignment_turned_in</i>
                      </div>
                  </div>
               </div>
            </div>
         </div> 
         <div class="col s12 m6 l6 xl3">
            <div class="card  z-depth-3  gradient-shadow min-height-100 black-text animate fadeRight border-radius-10 ">
               <div class="padding-4">
                  <div class="row">
                     <div class="col s7 m7">
                        <h5 class="mb-0 black-text font-weight-700"> # <?php echo getMaxCount('tbl_nuggets_qa',null,$dbconnection); ?></h5> 
                        <p>3 Min Nuggets Questions</p>
                     </div>
                     <div class="col s5 m5 right-align">
                        <i class="material-icons background-round ardent-text mt-5">av_timer</i>
                      </div>
                  </div>
               </div>
            </div>
         </div> 

        </div>
        <div class="divider mt-2"></div>
        <div class="container"> 
                <div id="card-with-analytics" class="section">
                        <h4 class="header ml-2">Questions Needed</h4>
                        <div class="row">
                        <div class="col s12 m6 card-width">
                                <div class="card border-radius-6">
                                <div class="card-content center-align">
                                <i class="material-icons amber-text small-ico-bg mb-5">check</i>
                                <p class="font-weight-700 mt-1 mb-1 ">Clinical Questions</p> 
                                <h5 class="m-0"> Last Date :  <b><?php echo  getMaxClinical($dbconnection);?></b></h5>
                                <?php if(checkTodayClinical($dbconnection,$date) == 0){ ?> 
                                <span class="chip red lighten-5 mt-2">
                                        <span class="red-text">Warning : No Question Today - <a href="add-new-clinical.php" class="red-text"> Click to Add  </a>  </span>
                                </span>
                                <?php }; ?> 
                                </div>
                                </div>
                        </div>
                        <div class="col s12 m6 card-width">
                                <div class="card border-radius-6">
                                <div class="card-content center-align">
                                <i class="material-icons amber-text small-ico-bg mb-5">check</i>
                                <p class="font-weight-700 mt-1 mb-1 ">3 Min Nuggets Questions</p> 
                                <h5 class="m-0"> Last Date :  <b><?php list($nugDate,$nugCount) =  getMaxNuggets($dbconnection); echo $nugDate;?></b></h5>
                                <?php if($nugCount  < 10) { ?>
                                        <span class="chip red lighten-5 mt-2">
                                        <span class="red-text">Warning : Only <?php echo $nugCount; ?> Quetions  Present for this date <?php echo $nugDate; ?> - <a href="add-3minedited.php?testDate=<?php echo encryptData($nugDate); ?>" class="red-text"> Click to Add  </a>  </span>
                                </span>
                                <?php }  ?> 
                                <?php if( $curentCount =  checkTodayNuggets($dbconnection,$date) < 10){ ?> 
                                <span class="chip red lighten-5 mt-2">
                                        <span class="red-text">Warning : Only <?php echo $curentCount ; ?> Present Today - <a href="add-3minedited.php?testDate=<?php echo encryptData($date); ?>" class="red-text"> Click to Add  </a>  </span>
                                </span>
                                <?php }; ?> 
                                </div>
                                </div>
                        </div> 
                        </div>
                </div>   
        </div>
        <div class="divider mt-2"></div>
        <div class="row"> 
                <div class="col s12 m4 l5">
                        <div class="card subscriber-list-card animate fadeLeft border-radius-10 ">
                        <div class="card-content ">
                        <h3 class="card-title mb-0">Last Week Top Ranker  - 3 Min Nuggets </h3>
                        <!-- <div class="divider mt-1 mb-1"></div> -->
                        </div>
                        <table class="centered  highlight " >
                        <thead>
                                <tr>
                                <th>Rank</th>
                                <th>Name</th> 
                                <th>Image</th>
                                </tr>
                        </thead>
                        <tbody style="padding-left:10px;border-radius:30px;">
                        <?php 
                       $sql = "SELECT *, SUM(`nuggets_final_score`) AS `totalScore` FROM `nuggets_test_user_attendee` WHERE yearweek(DATE(uta_time_stamp), 1) = yearweek(CURDATE() - INTERVAL 1 WEEK, 0) GROUP BY `nuggets_user_id` ORDER BY `totalScore` DESC , `uta_time_stamp` ASC LIMIT 3";
                       $exeQuery  = mysqli_query($dbconnection, $sql);
                        $sl_no = 0;
                        $rankArray = ['1st','2nd','3rd'];
                        while ($row = mysqli_fetch_array($exeQuery)) { 
                                $userPic ='';
                                $userPic  = fetchData($dbconnection,'user_profile_pic','user',$row['nuggets_user_id'],'user_account_id') ;
                                if(empty($userPic)){
                                    $userPic = './images/user_icon.png';   
                                }
                         ?>
                                <tr>
                                <td><?php echo $rankArray[$sl_no]; ?></td>  
                                <td class="text-capitalize"> <?php echo $row['nuggets_user_name']; ?></td>  
                                        <td class="center-align">
                                                <img  src="<?php echo $userPic; ?>"  class="z-depth-2 circle" height="52" width="52"> 
                                        </td>
                                </tr> 
                                <?php $sl_no++; } ?>
                        </tbody>
                        </table>
                        </div>
                </div>  
                <div class="col s12 m8 l7">
                        <div class="card subscriber-list-card animate fadeRight border-radius-10 ">
                        <div class="card-content ">
                        <h4 class="card-title mb-0">Top 5 Rated Videos </h4>
                        <!-- <div class="divider mt-1 mb-1"></div> -->
                        </div>
                        <table class="centered  highlight " >
                        <thead>
                                <tr>
                                <th>Sl No</th>
                                <th>Name</th> 
                                <th>Average Rating</th>
                                <th>Rating Count</th>
                                </tr>
                        </thead>
                        <tbody>
                        <?php 
                       $sql = "SELECT `video_id`, COUNT(`video_id`) AS `value_occurrence` ,  AVG(`video_rating`) AS `videoRating`  FROM tbl_video_rating_attendees GROUP BY `video_id` ORDER BY `value_occurrence` DESC LIMIT 5  ";
                       $exeQuery  = mysqli_query($dbconnection, $sql);
                        $sl_no = 1;
                        while ($row = mysqli_fetch_array($exeQuery)) { 
                         ?>
                                <tr>
                                        <td><?php echo $sl_no; ?></td>  
                                        <td class="text-capitalize"> 
                                                <?php echo  fetchData($dbconnection,'vedio_sm_child_name','vedio_sub_module',$row['video_id'],'vedio_sm_child_id');  ?>
                                        </td>  
                                        <td>
                                        <?php echo $row['videoRating']; ?>
                                        </td>   
                                    
                                        <td class="center-align">
                                                <?php echo $row['value_occurrence']; ?> 
                                        </td>
                                </tr> 
                                <?php $sl_no++; } ?>
                        </tbody>
                        </table>
                        </div>
                </div>  
        </div>

      </div>
    </div>
 
    <footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
      <div class="footer-copyright">
        <div class="container"><span>&copy; 2021 <a href="#" target="_blank"> Ardent MDS </a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="#"> MU3 Interactive </a></span></div>
      </div>
    </footer>
    <script src="js/vendors.min.js"></script> 
    <script src="vendors/jquery-validation/jquery.validate.min.js"> </script> 
    <script src="js/plugins.min.js"></script>    
    <script src="vendors/introjs/js/introjs.js"></script> 
    <script src="js/custom/custom-script.min.js"></script>   
  </body> 
</html>