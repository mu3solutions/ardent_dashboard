<?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
?>

<!DOCTYPE html>   
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> 
    <title>Add New Videos Module | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head>
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->
  <!-- END: SideNav-->


    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add Video Module</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="videos-module-list.php"> Video Module List</a>
                  </li>
                  <li class="breadcrumb-item active">Add Video Module
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
          <div class="row">  
                <form action="./inc/_videoController.php" method="POST"  enctype="multipart/form-data">
                    <div class="row">  
                        <div class="input-field col m5 s12">
                            <input id="videoModuleName" type="text" name="videoModuleName" required>
                            <label for="videoModuleName">Main Module Name</label>
                        </div>
                        <div class="input-field col m3 s12">
                            <input id="setSubModuleCount" type="number" name="setSubModuleCount" required>
                            <label for="setSubModuleCount">Set Sub-Module Count</label>
                        </div>
                        <div class=" col m4 s12">
                        <label>Set Module Status</label>
                            <select name="status" required>
                                <option value="1" >Active</option>
                                <option value="0"  >Inactive</option> 
                            </select>
                        </div>
                    </div>  
                    
                    
                      <div class="row ">  
                    <div class="col m2 s12 mt-3">
                        <label>
                            <span class="black-text"> Upload Video Icon : </span>
                        </label>
                    </div> 
                    <div class="col m6 s12 file-field inline input-field">  
                        <div class="btn float-right inline ardent-orange">
                        <span>Choose Image</span>
                        <input type="file" id="questionImage" name="questionImage" onchange="readImagURL(this,192,192,192,192);"  accept="image/x-png,image/jpeg"  required/>
                        
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id="fileWrapper">
                        <span class="helper-text ardent-text" >(Image Width and Height must be 192 * 192 )</span>
                      </div>
                    </div> 
                     <div class="col m4 s12 file-field inline input-field ">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image" />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
              </div>


                    <div class="row">  
                        <div class="input-field col s12">
                            <textarea id="videoModuleDesc" class="materialize-textarea" name="videoModuleDesc" required></textarea>
                            <label for="videoModuleDesc">Main Module Description</label>
                        </div> 
                    </div>  
            
            <div class="row">   
                <div class="input-field col s12 center"> 
                  <button class="btn ardent-orange waves-effect waves-light" type="submit" name="addNewvideoModule">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div>
 
          </form>    
        </div>
      </div>
    </div>
  </div>
       
    </div>
 
    <!-- END: Footer-->
   <!-- BEGIN VENDOR JS-->
   <script src="js/vendors.min.js"></script>   
    <script src="js/plugins.min.js"></script>  
  </body> 
</html>