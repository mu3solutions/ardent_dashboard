<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Clinical Vignets | Ardent MDS</title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
     
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/sweetalert/sweetalert.css">
     
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="css/pages/data-tables.min.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <!-- <?php include_once('inc/header.php'); ?> -->
    <header class="page-topbar" id="header">
        <div class="navbar navbar-fixed">
            <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple no-shadow">

                <div class="nav-wrapper">
                    <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>
                        <form action="./search.php" method="GET">
                            <input class="header-search-input z-depth-2" type="text" name="Search" placeholder="Explore Ardent Dashboard">
                        </form>
                        <ul class="search-list collection display-none"></ul>
                    </div>
                    <ul class="navbar-list right">

                        <li class="hide-on-med-and-down">
                            <a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a>
                        </li>
                        <li>
                            <a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="images/avatar/avatar-7.png" alt="avatar"><i></i></span></a>
                        </li>
                    </ul>

                    <ul class="dropdown-content" id="profile-dropdown">
                        <li><a class="grey-text text-darken-1" href="user-profile-page.html"><i class="material-icons">person_outline</i> Profile</a></li>

                        <li><a class="grey-text text-darken-1" href="user-login.html"><i class="material-icons">keyboard_tab</i> Logout</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- End: Header-->

   <!-- BEGIN: SideNav-->
   <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-rounded">
        <div class="brand-sidebar">
            <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="index.html"><img class="hide-on-med-and-down" src="images/logo/014.png" alt="materialize logo" /><img class="show-on-medium-and-down hide-on-med-and-up" src="images/logo/014.png" alt="materialize logo" /><span class="logo-text hide-on-med-and-down">Ardent</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
        </div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
            <li class="navigation-header"><a class="navigation-header-text">Applications</a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="bold"><a class="waves-effect waves-cyan " href="home-dashboard.php"><i class="material-icons">settings_input_svideo</i><span class="menu-title">Dashboard</span></a>
            </li>
            <li class="navigation-header"><a class="navigation-header-text">Pages </a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">face</i><span class="menu-title" data-i18n="User">Student Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a href="student-list.php"><i class="material-icons">radio_button_unchecked</i><span>View Student's
                                    List</span></a>
                        </li>
                        <li><a href="add-new-student.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Student</span></a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="bold "><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">photo_filter</i><span class="menu-title" data-i18n="Menu levels">Ardent Nuggets</span></a>
                <div class="collapsible-body" >
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span data-i18n="3 Min Challenge">3 Min Challenge</span></a>
                            <div class="collapsible-body">
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li><a href="3-min-challenges.php"><i class="material-icons">radio_button_unchecked</i><span>3-Min-Challenge List</span></a>
                                    </li>
                                    <li><a href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Third level">Add New 3-min Challenge</span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Second level child">Clinical Vignettes</span></a>
                            <div class="collapsible-body" >
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li><a href="cliniccal-vignets-list.php" ><i class="material-icons">radio_button_unchecked</i><span> Clinical Vignettes List </span></a>
                                    </li>
                                    <li><a href="add-new-clinical.php"><i class="material-icons">radio_button_unchecked</i><span>Add Clinical Vignettes</span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li  ><a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span>10 Min Concept</span></a>
                            <div class="collapsible-body" > 
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li><a href="10-min-concept.php"><i class="material-icons">radio_button_unchecked</i><span>Edit 10 Mins Concept</span></a>
                                    </li> 
                                </ul>
                            </div>
                        </li>

                        <li class="bold open"><a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span>Trending Videos</span></a>
                            <div class="collapsible-body"> 
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li><a  href="trending-videos.php"><i class="material-icons">radio_button_unchecked</i><span>Trending Videos List</span></a>
                                    </li> 
                                </ul>
                            </div>
                        </li>

                        <li  class="bold"><a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span>Quote Of The Day</span></a>
                            <div class="collapsible-body"> 
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li><a href="quote-list.php"><i class="material-icons">radio_button_unchecked</i><span>Quote Of The Day List</span></a>
                                    </li> 
                                    <li><a href="add-new-quote.php"><i class="material-icons">radio_button_unchecked</i><span>Add New Quote</span></a>
                                    </li> 
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="bold active"><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">face</i><span class="menu-title" data-i18n="User">MCQ Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a  href="mcq-module-list.php"><i class="material-icons">radio_button_unchecked</i><span>MCQ Main Module</span></a>
                        </li>
                        <li><a href="mcq-sub-module-list.php"><i class="material-icons">radio_button_unchecked</i><span>MCQ Sub Module</span></a>
                        </li>
                        <li><a class="active"  href="add-new-mcq-questions.php"><i class="material-icons">radio_button_unchecked</i><span>MCQ Questions List</span></a>
                        </li>
                        <li><a   href="add-new-mcq-questions.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Questions</span></a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>
    <!-- END: SideNav-->

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">

                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>MCQ Questions List</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                                </li>
                                <li class="breadcrumb-item active">MCQ Questions List
                                </li>
                            </ol>
                        </div>
                        <div class="col s3 m3 right">
                            <a href="add-new-mcq-questions.php"> <button class=" btn btn-block waves-effect waves-light amber darken-4" value="Add New Students">
                                    Add New Question
                                </button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">
                      <div class="row">
                        <div class="col s12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="row">
                                        <div class="row">
                                <?php if (isset($_SESSION['success'])) { ?>
                                <div class="col m6">
                                    <div class="card-alert card green">
                                        <div class="card-content white-text">
                                            <p><?php echo $_SESSION['success']; ?></p>
                                        </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                            </div>
                </div>
                <?php
                }   
                unset($_SESSION['success']);
                if (isset($_SESSION['error'])) {
                ?> 
                <div class="col m6">
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p><?php echo $_SESSION['error']; ?></p>
                        </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                </div>
                                <?php
                                }
                                unset($_SESSION['error']);
                                ?>
                                </div>
                                <div class="col s12">
                                    <table id="page-length-option" class="display highlight">
                                    <thead>
                                        <tr>
                                            <th>Edit</th>
                                            <th>Delete</th> 
                                            <th>Module Name</th>
                                            <th>Question</th>
                                            <th>Option 1</th>
                                            <th>Option 2</th>
                                            <th>Option 3</th>
                                            <th>Option 4</th>
                                        </tr>
                                    </thead>
                                <tbody>
                                <?php
                                    $answers = array();
                                    $selectQuery = "SELECT * FROM `qa` ORDER BY `mcq_qn_id` ASC";
                                    $exeQuery  = mysqli_query($dbconnection, $selectQuery);
                                    while ($row = mysqli_fetch_array($exeQuery)) {
                                    $answers = explode('$&', $row['mcq_answers']);
                                    $row['mcq_qn_desc'];
                                    ?>
                                <tr>
                                    <td>
                                        <a href="edit-clinical.php?qid=<?php echo $row['mcq_qn_id']; ?>"><i class="material-icons">edit</i></a> 
                                    </td>
                                    <td> 
                                        <span id="deleteQuestion" onclick="deletedQuestion(<?php echo $row['mcq_qn_id']; ?>)"><i class="material-icons cursor-pointer materialize-red-text">delete</i></span>
                                    </td> 
                                    <td><?php echo truncate($row['mcq_qn_desc'], 40); ?></td>
                                    <td><?php echo truncate($row['mcq_qn_desc'], 40); ?></td>
                                    <td><?php echo truncate($answers[0], 25); ?></td>
                                    <td><?php echo truncate($answers[1], 25); ?></td>
                                    <td><?php echo truncate($answers[2], 25); ?></td>
                                    <td><?php echo truncate($answers[3], 25); ?></td>
                                </tr>
                                <?php }   ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- START RIGHT SIDEBAR NAV -->
                    <!-- END RIGHT SIDEBAR NAV -->
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div> 
    <script src="js/vendors.min.js"></script>
    <script src="vendors/data-tables/js/jquery.dataTables.min.js"></script>
    <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/sweetalert/sweetalert.min.js"></script> 
    <script src="js/plugins.min.js"></script> 
    <script src="js/scripts/data-tables.min.js"></script>   
    <script src="js/custom/custom-script.min.js"></script> 
    <script>
        function deletedQuestion(id)
		{   
                 
            swal({
                title: "Are you sure?",
                text: "You will not be able to redo this action!",
                icon: 'warning',
                dangerMode: true,
                buttons: {
                cancel: 'No, Please!',
                delete: 'Yes, Delete It'
                }
            }).then(function (willDelete) {
                if (willDelete) 
                {
                    $.ajax({
                        url: "./inc/_insertClinical.php",
                        data:{deleteQuestion: 1,questionID:id},
                        cache: !1,
                        type: 'post', 
                        success:function(data)
                        { 
                            swal("Question Deleted Successfully", {
                                icon: "success",
                            }).then(function(){
                                location.reload();
                            });    
                         }
                    }); 
                } 
                else  {  return false  }
            });
		}
        
        $(function() {
            $("#page-length-option").DataTable({
                responsive: false,
                ordering: true,
                scrollX: true,
                scrollY: false,
                "columnDefs": 
                [ 
                    {
                    "targets": 0,
                    "orderable": !1
                    },
                    {
                        "targets": 1,
                        "orderable": !1
                    } 
                ]
            })
        });
    </script> 
</body> 
</html>