<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$testDate = '';
if (isset($_GET['date'])) {
  $testDate = $_GET['date'];
}
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title>Add New Grand Test | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
  <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
  <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
  <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
  <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
  <link rel="stylesheet" href="vendors/introjs/css/introjs.css"> 
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
  <!-- END: Custom CSS-->
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add New Grand Test</span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                <li class="breadcrumb-item"><a href="grand-test-list.php">Grand Test List </a>
                <li class="breadcrumb-item active">Create New Grand Test
                </li>
              </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
            <div class="card-content" id="singleQuestion">
              <div class="row">
                <form action="./inc/_grandTestController.php" method="POST" enctype="multipart/form-data">
                  <div class="row"> 
                    <div class="input-field col m4 s4"> 
                      <input type="text" id="testEnd" name="testEnd" required>
                      <label for="testEnd">Test Expiry Date</label>
                    </div>  
                  </div>

                  <div class="row">
                    <div class="input-field col m5 s12">
                      <input type="text" id="testName"  name="testName" required></input>
                      <label for="testName">Test Name</label>
                    </div>
                    <div class="input-field col m2 s12">
                      <input id="totalQuestions" class="number" type="number"  min="1" name="totalQuestions" required>
                      <label for="totalQuestions">Total Questions</label>
                    </div>
                    <div class="input-field col m5 s12">
                      <input id="totalTime"  class="number" type="number" name="totalTime" required placeholder="60" required>
                      <label for="totalTime">Test Duration</label>
                      <span class="helper-text">Note : Duration Should be in Minutes ( Eg : 1 Hour = 60 Mins) </span>
                    </div>
                  </div> 
                  <div class="row">
                    <div class="input-field col m3 s12">
                      <input id="correctAnsMark" class="number" type="number" step="any" min="1" name="correctAnsMark" required>
                      <label for="correctAnsMark">Correct Answer Mark</label>
                    </div>
                    <div class="input-field col m3 s12">
                    <input id="negMark" class="number" type="number" step="any" min="1" name="negMark" required>
                      <label for="negMark">Negative Mark</label>
                    </div> 
                    <div class="switch col m5 s12 mt-4">
                        <label>
                        Wish to place this test in first order ? &nbsp; : 
                           &nbsp;&nbsp;
                            No
                            <input type="checkbox" name="sortorder" value="1">
                            <span class="lever"></span>
                            Yes
                            </label>
                    </div>
                  </div> 

                <div class="row mt-3">  
                    <div class="col m6 s12">
                        <label>Set Access Status</label>
                        <select name="grandAccessStatus" required>
                            <option value="0"> Opened </option>
                            <option value="1"> Locked </option> 
                        </select>
                    </div>
                    <div class="col m6 s12">
                        <label>Set Active Status</label>
                        <select name="grandActiveStatus" required>
                            <option value="0"> In-Active </option> 
                        </select>
                        <span class="helper-text ardent-text" >Test will be set to inactive untill questions are uploaded</span>
                    </div>
                </div> 
 
                  <div class="row"> 
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="addNewGrandTest">Submit
                          <i class="material-icons right">send</i>
                        </button>
                      </div>  
                  </div> 
                </form>
              </div>
            </div> 
          </div>
        </div> 
      </div> 
      <script src="js/vendors.min.js"></script> 
      <script src="js/plugins.min.js"></script> 
      <script src="js/custom/custom-script.min.js"></script>   
     <script> 
     $(function(){  
        $('#testStart,#testEnd').datepicker(
        {
            autoClose: !0,
            minDate: new Date(),
            format: "yyyy-mm-dd"    
        }); 
     })
     </script>
</body> 
</html>