<?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
$mainModule= "";
if(isset($_GET['vid'])){
$mainModule = decryptData($_GET['vid']); 
}
?>

<!DOCTYPE html>   
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> 
    <title>Edit Videos Module | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head>
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->
  <!-- END: SideNav-->


    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Edit Video Module</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="videos-module-list.php"> Video Module List</a>
                  </li>
                  <li class="breadcrumb-item active">Edit Video Module
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
          <div class="row">  
                <form action="./inc/_videoController.php" method="POST">  
                <?php 
          if(!empty($mainModule)) {
          $sql = "SELECT * FROM `vedio_module`  WHERE `mvid` = '$mainModule'";
                $result = mysqli_query($dbconnection, $sql);
                if(mysqli_num_rows($result) > 0){

                  if  ($row = mysqli_fetch_array($result)) {  
                
                  ?>   
                  <input type="hidden" name="uniqueId" value="<?php echo $row['mvid']; ?>">
                    <div class="row">  
                        <div class="input-field col m5 s12">
                            <input id="videoModuleName" type="text" name="videoModuleName" value="<?php echo $row['mvname']; ?>" required>
                            <label for="videoModuleName">Video Module Name</label>
                        </div>
                        <div class="input-field col m3 s12">
                            <input id="setSubModuleCount" type="number" name="setSubModuleCount" value="<?php  echo $row['mv_total_sub_modules'];?>" required>
                            <label for="setSubModuleCount">Edit Sub-Module Count</label>
                        </div>
                        <div class=" col m4 s12">
                        <label>Edit Module Status</label>
                            <select name="status" required>
                                <option value="1" <?php if($row['mv_status']=='1'){echo 'selected';}  ?>>Active</option>
                                <option value="0" <?php if($row['mv_status']=='0'){echo 'selected';}  ?>>Inactive</option>  
                            </select>
                        </div>
                    </div>  

                    <div class="row">  
                        <div class="input-field col s12">
                            <textarea id="videoModuleDesc" class="materialize-textarea" name="videoModuleDesc" required><?php echo $row['mvdesc']; ?></textarea>
                            <label for="videoModuleDesc">Video Module Description</label>
                        </div> 
                    </div>  
            
            <div class="row">   
                <div class="input-field col s12 center"> 
                  <button class="btn ardent-orange waves-effect waves-light" type="submit" name="editmicroCorouse">Save Changes
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div>
            <?php }  ?> 
            <?php  }else { ?>
             <div class="row center-align">  
               <div class="col m12 s12">
                   <h3 class="text-center"> Your Link is Broken</h3>
                   <img class="z-depth-4 circle" width="100" height="100" src="./images/favicon/no-access-icon.png " alt="">
               </div> 
           </div>
            <?php }  } else { ?>
            <div class="row center-align">  
               <div class="col m12 s12">
                   <h3 class="text-center"> Your Link is Broken</h3>
                   <img class="z-depth-4 circle" width="100" height="100" src="./images/favicon/no-access-icon.png " alt="">
               </div> 
           </div>

            <?php } ?>
          </form>    
        </div>
      </div>
    </div>
  </div>
       
    </div>
 
    <!-- END: Footer-->
   <!-- BEGIN VENDOR JS-->
   <script src="js/vendors.min.js"></script>   
    <script src="js/plugins.min.js"></script>  
  </body> 
</html>