<?php
require_once('./inc/dbConfig.php');
if(isset($_POST['submit'])){ 
    $adminemail =   mysqli_real_escape_string($dbconnection,$_POST['adminemail']);
    $password = mysqli_real_escape_string($dbconnection,$_POST['password']); 
    $haspassword = md5($password); 
    $sql = "SELECT * FROM admin_login WHERE admin_email = '$adminemail' AND admin_password = '$haspassword'";

    $res = mysqli_query($dbconnection, $sql);
        if(mysqli_num_rows($res) == 1){
            if($row = mysqli_fetch_array($res)){

			$admin_status = $row['admin_status'];
			if ($admin_status=='1') 
			{
			$admin_id  = $row['admin_id'];
		  $update = "UPDATE admin_login SET `last_logged_in`= NOW() WHERE  admin_id='$admin_id'";
			$exeUpdate = mysqli_query($dbconnection,$update);
            session_start();
            $_SESSION["authenticateds"] = true; 
            header('location:home-dashboard.php');
			}
			else{
				session_start(); 
				$msg = 'Your Access has been revoked by admin';
				$msgclass = 'red'; 
			}

			
        }
       
    }else{
		session_start(); 
		 $msg = 'Incorrect Username or Password.';
		 $msgclass = 'red';
	}
}
?>
<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> 
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> 
    <title>Admin Login | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css"> 
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="css/pages/login.css"> 
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css"> 
  </head>
  <!-- END: Head-->
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 1-column login-bg   blank-page blank-page" data-open="click" data-menu="vertical-modern-menu" data-col="1-column">
    <div class="row">
      <div class="col s12">

        <div class="container">
          <div id="login-page" class="row">
            <div class="col s12 m6 l4 z-depth-5 card-panel border-radius-6 login-card bg-opacity-8">
              <form class="login-form" method="POST" action="">
                <div class="row">
                  <div class="input-field col s12">
         
          <h5 class="ml-4 center-align">Sign In</h5>
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">person_outline</i>
          <input id="username" name="adminemail"  type="text" required>
          <label for="username" class="center-align">Admin Email </label>
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">lock_outline</i>
          <input id="password" name="password" type="password" required> 
          <label for="password">Admin Password</label>
        </div>
      </div>
       
      <div class="row">
        <div class="input-field col s12">
        <?php if(isset($msg)) {?>
                  <p style="color:<?php echo $msgclass; ?>"><?php echo $msg; ?></p>
                <?php } ?>  
          <button  type="submit" name="submit" class="btn ardent-orange waves-effect waves-light border-round  col s12">Login</button>
        </div>
      </div> 
    </form>
  </div>
</div>
        </div>
        <div class="content-overlay"></div>
      </div>
    </div> 
    <script src="js/vendors.min.js"></script> 
    <script src="js/plugins.min.js"></script>
    <script src="js/search.min.js"></script>
    <script src="js/custom/custom-script.min.js"></script> 
  </body> 
</html>