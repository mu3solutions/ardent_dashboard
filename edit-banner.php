<?php 
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
$Sid = '';

if(isset($_GET['Sid'])){
    $Sid = decryptData($_GET['Sid']);
}
?>

<!DOCTYPE html>
  
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Edit Slider Details | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css"> 
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/sweetalert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->


    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add New Slider Image</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="top-banner-list.php"> Slider Image List</a>
                  </li>
                  <li class="breadcrumb-item active">Add New Slider Image
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
            <form action="./inc/_bannerController.php" method="POST"  enctype="multipart/form-data"> 
            <div class="row"> 
          <?php  
          if(!empty($Sid)) {
				$sql = "SELECT * FROM `topbanner` WHERE `bId` = '$Sid'";
				$result = mysqli_query($dbconnection, $sql);
        if(mysqli_num_rows($result) > 0){

          if  ($row = mysqli_fetch_array($result)) {  
        
				?>  

                <input type="hidden" name="bannerId" value="<?php echo $row['bId']; ?>">


            <div class="row">  
                <div class="input-field col s12">
                    <input type="url" id="bannerLink" class="materialize-textarea" name="bannerLink" value="<?php  echo $row['banner_link'];?>"  > 
                    <label for="bannerLink">Banner Link</label>
                </div> 
            </div>
            
            <div class="row ">  
                    <div class="col m2 s12 mt-3">
                        <label>
                            <span class="black-text"> Upload Banner Image : </span>
                        </label>
                        
                    </div> 
                    <div class="col m6 s12 file-field inline input-field">  
                      <div class="btn float-right inline ardent-orange">
                            <span>Choose Image</span>
                            <input type="file" id="file" name="questionImage" onchange="readImagURL(this,1348,576,600,300);" accept="image/x-png,image/jpeg" />
                      </div>
                      <div class="file-path-wrapper">
                            <input class="file-path validate valid" type="text" id="fileWrapper">
                            <span class="helper-text ardent-text" >( Image Width and Height must be 1348 * 576  )</span>
                      </div>
                    </div>

                    <div class="col m4 s12">
                      <label>Set Slider Image Status</label>
                      <select name="bannerStatus" required>
                        <option value="1" <?php if($row["status"] == '1'){echo "selected"; } ?>> Active </option> 
                        <option value="0" <?php if($row["status"] == '0'){echo "selected"; } ?>> In-Active </option> 
                      </select>
                    </div>

            </div>

                  <div class="row"> 
                    <div class="col m8">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>New Selected Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image"  />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
                  </div>

                  <div class="row"> 
                    <div class="col m6">
                      <div class="wrapper ">
                        <p>Current Image : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="<?php  echo $row['bannerImage'];?>" height="300" width="600" alt="No Image"  /> 
                      </div>
                    </div>
                  </div>
            
            <div class="row">   
                <div class="input-field col s12 center"> 
                  <button class="btn ardent-orange waves-effect waves-light" type="submit" name="editBanner">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div> 
            <?php }  ?> 
             <?php  }else { ?>
              <div class="row center-align">  
                <div class="col m12 s12">
                    <h3 class="text-center"> Your Link is Broken</h3>
                    <img class="z-depth-4 circle" width="80" height="80" src="./images/favicon/no-access-icon.png " alt="">
                </div> 
            </div>
             <?php }  } else { ?>
             <div class="row center-align">  
                <div class="col m12 s12">
                    <h3 class="text-center"> Your Link is Broken</h3>
                    <img class="z-depth-4 circle" width="80" height="80" src="./images/favicon/no-access-icon.png " alt="">
                </div> 
            </div>

             <?php } ?>
          </form>    
        </div>
      </div>
    </div>
  </div>
</div>
 
    <!-- END: Footer-->
   <!-- BEGIN VENDOR JS-->
   <script src="js/vendors.min.js"></script>   
    <script src="js/plugins.min.js"></script> 
    <script src="vendors/formatter/jquery.formatter.min.js"></script> 
    <script src="vendors/sweetalert/sweetalert.min.js"></script> 
    <script src="js/custom/custom-script.min.js"></script>  
  </body> 
</html>