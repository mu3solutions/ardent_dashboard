<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$questionId = "";

if(isset($_GET["qid"])){
    $questionId = decryptData($_GET["qid"]);
}
?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> 
    <title> Fastest Finger Quiz | Ardent MDS</title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css"> 
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="css/pages/data-tables.min.css"> 
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css"> 
</head> 

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">

                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>Fastest Finger Quiz Results </span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Fastest Finger Quiz Results
                                </li>
                            </ol>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">
                    <div class="row">
                        <div class="col s12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="row"> 
                                            <div class="col s12"> 
                                                <table class="bordered centered" id="refreshbody">
                                                    <thead>
                                                        <tr>
                                                        <th>Position</th>
                                                        <th>Student Name</th>
                                                        <th>Student Image</th> 
                                                        <th>Student Mark</th> 
                                                        <th>Time Taken</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php 
                                                        $selectQuery = "SELECT * FROM `tbl_ff_user_attendee` WHERE `ff_q_id` ='$questionId' ORDER BY `ff_time_taken` ASC ";
                                                        $exeQuery  = mysqli_query($dbconnection, $selectQuery);
                                                        $sl_no = 1;
                                                        while ($row = mysqli_fetch_array($exeQuery)) { 
                                                            $userPic ='';
                                                            $userPic  = fetchData($dbconnection,'user_profile_pic','user',$row['ff_user_id'],'user_account_id') ;
                                                            
                                                            if(empty($userPic)){
                                                                $userPic = 'https://www.ardentmds.com/ardent-dashboard/images/user_icon.png';   
                                                            }

                                                            if($row['ff_q_result'] == 'F')
                                                            {  
                                                                $chipColor  = 'chip red lighten-5';
                                                                $chipTextColor  = 'red-text';
                                                                $chipText  = 'Fail';
                                                            }else if ($row['ff_q_result'] == 'P') 
                                                            {
                                                                $chipColor  = 'chip green lighten-5';
                                                                $chipTextColor  = 'green-text';
                                                                $chipText  = 'Pass'; 
                                                            }
                                                                    ?>
                                                                    <tr> 
                                                                        <td>    
                                                                            <?php echo $sl_no; ?>      
                                                                        </td>
                                                                        <td>
                                                                            <?php echo fetchData($dbconnection,'user_name','user',$row['ff_user_id'],'user_account_id') ?>
                                                                </td>
                                                                <td> 
                                                                    <img  src="<?php echo $userPic; ?>"  class="z-depth-2 circle" height="40" width="40"> 
                                                                </td>
                                                                <td>
                                                                    <span class="<?php echo $chipColor; ?>"><span class="<?php echo $chipTextColor; ?>"><?php echo $chipText; ?></span></span>
                                                                </td> 
                                                                <td><?php echo   $row['ff_time_taken'] .' Sec'; ?></td>  
                                                            </tr>
                                                            <?php $sl_no++ ; } ?>
                                                            </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- START RIGHT SIDEBAR NAV -->
                    <!-- END RIGHT SIDEBAR NAV -->
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div> 
    <script src="js/vendors.min.js"></script>
    <script src="vendors/data-tables/js/jquery.dataTables.min.js"></script>
    <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script> 
    <script src="js/plugins.min.js"></script> 
    <script src="js/custom/custom-script.min.js"></script> 
    <script src="js/scripts/data-tables.min.js"></script>   
    <script> 
        $(function() {
            $("#page-length-option").DataTable({
                responsive: false,
                ordering: true,
                scrollX: true,
                scrollY: false
            })

            function refreshTable(name){
                $("#refreshbody").load(location.href+" #refreshbody>*",""); 
            }; 
            var intervalId = window.setInterval(function(){
                refreshTable('sura'); 
            }, 2000);

        });
    </script> 
</body> 
</html>