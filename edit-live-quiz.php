 <?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
?>

<!DOCTYPE html>
  
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Edit Live Quiz Details | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css"> 
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->


    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Edit Live Quiz </span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="ff-quiz.php">Fastest Finger Question List</a>
                  </li>
                  <li class="breadcrumb-item active">Edit Live Quiz
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
        
          <div class="row"> 
                <div class="row">
                                <?php if (isset($_SESSION['liveQuizSuccess'])) { ?>
                                <div class="col m6">
                                    <div class="card-alert card green">
                                        <div class="card-content white-text">
                                            <p><?php echo $_SESSION['liveQuizSuccess']; ?></p>
                                        </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                            </div>
                </div>
                <?php
                }   
                unset($_SESSION['liveQuizSuccess']);
                if (isset($_SESSION['liveQuizError'])) {
                ?> 
                <div class="col m6">
                    <div class="card-alert card red">
                        <div class="card-content white-text">
                            <p><?php echo $_SESSION['liveQuizError']; ?></p>
                        </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                </div>
                                <?php
                                }
                                unset($_SESSION['liveQuizError']);
                                ?>
                                </div> 
                          <form action="./inc/_liveController.php" method="POST"  enctype="multipart/form-data">
                          <?php  
                            $sql = "SELECT * FROM `tbl_live`";
                            $result = mysqli_query($dbconnection, $sql);
                            if ($row = mysqli_fetch_array($result)) {  
                                    $liveDate=array();
                                    $upcomingDate =  array();
                                    $liveDate = explode( '@', $row['live_class_date']) ;
                                    $upcomingDate = explode( '@', $row['live_upcoming_date']) ;
                          ?>     
            <div class="row">  
                <div class="input-field col m6 s12">
                    <input type="text" id="liveName" class="materialize-textarea" name="liveName" value="<?php  echo $row['liveName'];?>" required> 
                    <label for="liveName">Live Name</label>
                </div> 
                <div class="col m4 s12  ">
                        <label>Choose Tutor</label>
                        <select name="chooseTutor" id="chooseTutor" required>
                            <?php 
                            $sql = "SELECT * FROM `tutordetails`";
                            $result = mysqli_query($dbconnection, $sql);
                            while ($row1 = mysqli_fetch_array($result)) 
                            {?>
                            <option value="<?php echo $row1['tutorId']; ?>" <?php  if($row1['tutorId'] == $row['live_tutor_id']){echo'selected';} ?> ><?php echo $row1['tutorName']; ?></option>    
                            <?php } ?>
                        </select>
                    </div>
                <!--<div class="input-field col s12">-->
                <!--    <input type="url" id="liveUrl" class="materialize-textarea" name="liveUrl" value="<?php  echo $row['live_video_url'];?>" required> -->
                <!--    <label for="liveUrl">Live URL</label>-->
                <!--</div> -->
            </div>
            
            <div class="row">  
                <div class="input-field col s12">
                <textarea id="liveDesc" class="materialize-textarea" name="liveDesc" required><?php  echo $row['liveDesc'];?></textarea>
                    <label for="liveDesc">Live Description </label>
                </div> 
            </div>
           
           
            <div class="row">  
                <div class="input-field col s12">
                        <textarea id="liveDesc" class="materialize-textarea" name="livedetails" required><?php  echo $row['exam_details'];?></textarea>
                        <label for="liveDesc">Benefits of Live Class </label>
                    <span class="helper-text ardent-text" >(Seperate multiple lines with "semicolon ;")</span>
                </div> 
            </div>
            
            
            <div class="row mt-2" >  
                <div class="input-field col m2 s12">
                <input type="text" class="examdate" value="<?php  echo $liveDate[0];?>" name="liveDate" required> 
                    <label for="liveDate">Current Live Date</label>
                </div> 
                <div class="input-field col m2 s12">
                    <input type="text" value="<?php  echo $liveDate[1];?>" name="liveTime" required class="timepicker"> 
                    <label for="liveTime">Current Live Time</label>
                </div> 
                <div class="input-field col m2 s12">
                    <input type="number" id="liveduration" value="<?php  echo $row["live_duration"];?>" name="liveduration" required> 
                    <label for="liveduration">Live Duration</label>
                </div> 
                <div class="input-field col m2 s12">
                    <input type="number" id="liveAttendies" value="<?php  echo $row["live_attendies"];?>"  name="liveAttendies" required> 
                    <label for="liveAttendies">Live Attendies </label>
                </div> 
                <div class="input-field col m2 s12">
                    <input type="text" id="upcommingliveDate" class="examdate" value="<?php  echo $upcomingDate[0];?>"  name="upcommingliveDate" required> 
                    <label for="upcommingliveDate">Upcomming Live Date</label>
                </div> 
                <div class="input-field col m2 s12">
                    <input type="text" id="upcommingliveTime" value="<?php  echo $upcomingDate[1];?>"  name="upcommingliveTime" required class="timepicker"> 
                    <label for="upcommingliveTime">Upcomming Live Time</label>
                </div> 
            </div>
            
                <div class="row mt-2 ">       
                    <div class="col m4 s12  mt-2">
                      <label>Set Live Status</label>
                      <select name="status" required id="liveStatus" > 
                      <option value="1" <?php if($row["status"] == '1'){echo "selected"; } ?>> Active </option> 
                        <option value="0" <?php if($row["status"] == '0'){echo "selected"; } ?>> In-Active </option> 
                      </select>
                    </div>
                    <?php if($row["status"]==0){ ?>
                    <div class=" col m8 s12  mt-2" id="upcomminMessage">
                    <label for="upcoming">Set In Active Message </label>
                        <textarea id="upcoming" class="materialize-textarea" name="upcoming" required><?php  echo $row['upcoming'];?></textarea>
                    </div> 
                    <?php } ?>
                </div> 
 
            
            <div class="row">   
                <div class="input-field col s12 center"> 
                  <button class="btn ardent-orange waves-effect waves-light" type="submit" name="editLive">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div> 
            <?php } ?>
          </form>    
        </div>
      </div>
    </div>
  </div>
</div>
 
    <script src="js/vendors.min.js"></script> 
    <script src="vendors/jquery-validation/jquery.validate.min.js"> </script> 
    <script src="js/plugins.min.js"></script>    
    <script src="js/scripts/ui-alerts.min.js"></script>  
    <script src="js/custom/custom-script.min.js"></script>   
    <script>
     $(function(){  
      $('#liveStatus').on('change', function() 
        {
          var liveStatus = $(this).val(); 
          var select = $('#upcomminMessage');
          if(liveStatus =='0')
          {  
            $('#upcomminMessage').show('500'); 
            select.attr('required', true);
          }else{
            $('#upcomminMessage').hide('500'); 
            select.attr('required', false);
          }
         });
    });
    </script>
  </body> 
</html>