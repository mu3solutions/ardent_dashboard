 <?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title>Add MCQ of the Day | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
  <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
  <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
  <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
  <link rel="stylesheet" href="vendors/introjs/css/introjs.css"> 
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
  <!-- END: Custom CSS-->
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

   <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

  <!-- BEGIN: Page Main-->
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m8 l8">
            <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add New MCQ of the day Question</span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                <li class="breadcrumb-item"><a href="mcq-qotd-list.php">MCQ of the day Question List </a>
                <li class="breadcrumb-item active">Add New MCQ of the day Question
                </li>
              </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
                <ul class="tabs">
                    <li class="tab col m6"><a class="active"  href="#singleQuestion">Upload Single Question</a></li>
                    <li class="tab col m6"><a  href="#excelImport" > Add Questions From Question Bank  </a></li>
                </ul>
            <div class="card-content" id="singleQuestion">
              <div class="row ">
                <form action="./inc/_mcqofthedayController.php" method="POST" enctype="multipart/form-data"> 
                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="Question" class="materialize-textarea" name="Question" required></textarea>
                      <label for="Question">Question</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="option1" type="text" name="option1" required>
                      <label for="option1">Option 1</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="option2" type="text" name="option2" required>
                      <label for="option2">Option 2</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="option3" type="text" name="option3" required>
                      <label for="option3">Option 3</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="option4" type="text" name="option4" required>
                      <label for="option4">Option 4</label>
                    </div>
                  </div>

                  <div class="row ">
                    <div class="col m2 s12">
                      <label>Set Correct Answer</label>
                      <select name="correctoption" required>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                        <option value="4">Option 4</option>
                      </select>
                    </div>

                    <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Question Image : </span>
                      </label>
                    </div>


                    <div class="col m4 s12 file-field inline input-field"> 
                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="questionImage" name="questionImage" onchange="readURL(this);" accept="image/x-png,image/jpeg" />
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id="fileWrapper">
                      </div>
                    </div>

                    <div class="col m4 s12 file-field inline input-field ">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image" />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
                  </div>


                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="answerExplanation" name="answerExplanation" class="materialize-textarea"></textarea>
                      <label for="answerExplanation">Answer Explanation</label>
                    </div>
                  </div>
                
                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="answerRef" name="answerRef" class="materialize-textarea"></textarea>
                      <label for="answerRef">Answer Reference</label>
                    </div>
                  </div>


                  <div class="row ">
                    <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Answer Image : </span>
                      </label>
                    </div>


                    <div class="col m6 s12 file-field inline input-field">

                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="answerImage" name="answerImage[]" multiple accept="image/x-png,image/jpeg" />
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text">
                      </div>
                    </div>
                  </div>
                  <div id="preview"></div>


                  <div class="row">
                    <div class="row">
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="addMCQqotdQuestion"> Submit <i class="material-icons right">send</i>
                        </button>
                      </div>
                    </div>
                  </div>
                </form>

              </div>
            </div>

            <div class="card-content pb-4" id="excelImport">
              <form action="./inc/_mcqofthedayController.php" method="POST" enctype="multipart/form-data" id="forms">
              <input type="hidden" name="gtId" value="<?php echo $testID; ?>">
                 <div class="row ">
                 <div class="col m6 mt-3 s12">
                            
                    </div> 
                    <div class="col mt-3 m6 s12">
                          <label>Select Main Module</label>
                          <?php 
                              $query = "SELECT * FROM `mcq_module` ORDER BY `mcq_mid` ASC";
                              $result = mysqli_query($dbconnection, $query);
                              ?>
                              <select name="mainsModule"  class="select2 browser-default" id="multi_search_filter" required>
                              <option value="">Select Module first</option>     
                                  <?php
                                  if (mysqli_num_rows($result) > 0) {
                                    while ($row = mysqli_fetch_array($result)) 
                                    {
                                    echo "<option value='".$row['mcq_mid']."'>".$row['mcq_mname']."</option>";
                                    }
                                  } 
                                  else 
                                  {
                                    echo "<option>'Module Not Available'</option>";
                                  }
                                  ?>
                             </select>
                      </div> 
                  </div>

                  <div class="cools"  style="position:sticky;top:0;background-color:#fff;z-index:999;">
                    <div class="row displayafter display-none"> 
                        <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="addSelectedMcqQuestion"> Add Selected Questions <i class="material-icons right">send</i>
                          </button>
                        </div> 
                    </div>
                    
                    <div class="row displayafter display-none"> 
                        <div class="input-field col m4 s12 right">
                          <input type="text" id="search" placeholder="Search Questions"> 
                        </div> 
                    </div>

                  </div>

                   

                  <div class="Wrapper displayafter display-none">
                    <table class="centered bordered"  id="page-length-option">
                      <thead>
                          <tr>
                            <th style="min-width: 50px;">Action</th>
                            <th style="min-width: 300px;">Question</th>
                            <th>Option 1</th>
                            <th>Option 2</th>  
                            <th>Option 3</th>  
                            <th>Option 4</th>  
                          </tr>
                      </thead>
                          <tbody> 
                          </tbody>
                    </table>
                  </div>
              </form> 
            </div>
             
          </div>
        </div> 
      </div> 
      <script src="js/vendors.min.js"></script>  
      <script src="js/plugins.min.js"></script> 
      <script src="vendors/select2/select2.full.min.js"></script>
      <script src="js/scripts/form-select2.min.js"></script>
      <script src="js/custom/custom-script.min.js"></script>  
      <script>
      $(function(){ 

          $(".select2c").select2(); 

          load_data();                     
              
              function load_data(query='')
              {
                      $.ajax({
                      url:"inc/fetchQBank.php",
                      method:"POST",
                      data:{search:query,fetchQuestions:1},
                      success:function(data)
                      { 
                        if(data != 'false')
                        {  
                          $('tbody').html(data);
                        }
                      }
                      })
              }

              $('#multi_search_filter').change(function(){  
                $('.displayafter').show();
                var query = $('#multi_search_filter').val()
                load_data(query);
                
              var sforms = $("input[type='checkbox']").length; 
              });
              

              var forms = $("input[type='checkbox']"); 
              $(".change").change(function(){ 
                  var countCheckedCheckboxes = forms.filter(':checked').length; 
                  $('#count-checked-checkboxes').text(countCheckedCheckboxes);
              });


      });  
 

      $("#search").on("keyup", function() {
                var value = $(this).val(); 
                $("table tr").each(function(index) { 
                    if (index !== 0) { 
                        $row = $(this); 
                        var id = $row.find("td:nth-child(2)").text(); 
                        if (id.indexOf(value) !== 0){ $row.hide(); }
                        else { $row.show(); }
                    }
                  });
            }); 
    </script> 
</body> 
</html>