<?php 
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php'); 
?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
     
    <title>Student Details | Ardent MDS</title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css"
        href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="css/pages/data-tables.min.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->

<body
    class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   "
    data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->


    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">

                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>Student's List</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Student's List
                                </li>
                            </ol>
                        </div>
                        <div class="col s3 m3 right">
                            <a href="add-new-student.php"> <button
                                    class=" btn btn-block waves-effect waves-light amber darken-4"
                                    value="Add New Students">
                                    Add New Student
                                </button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">

                        <!-- Page Length Options -->
                        <div class="row">

                            <div class="col s12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="row">
                                            <div class="row">
                                                <?php if(isset($_SESSION['studentsuccess']))
                                                {    ?>
                                                <div class="col m6">
                                                    <div class="card-alert card green">
                                                        <div class="card-content white-text">
                                                            <p><?php echo $_SESSION['studentsuccess']; ?></p>
                                                        </div>
                                                        <button type="button" class="close white-text"
                                                            data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
							                	}
                              unset($_SESSION['studentsuccess']);
                              if(isset($_SESSION['studenterror']))
                              {
                              ?> <div class="col m6">
                                                    <div class="card-alert card green">
                                                        <div class="card-content white-text">
                                                            <p><?php echo $_SESSION['studenterror']; ?></p>
                                                        </div>
                                                        <button type="button" class="close white-text"
                                                            data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                              }
                              unset($_SESSION['studenterror']);
                            ?>
                                            </div>
            <div class="col s12">
                <table id="page-length-option" class="display highlight">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>Name</th>
                            <th>Email</th>  
                            <th>User Access</th>
                        </tr>
                    </thead>
                <tbody>
                    <?php
                        $userAccessLevel = array();
                        $finalArray = array(); 
                        $selectQuery = "SELECT * FROM `user` ORDER BY `uid` DESC";
                        $exeQuery  = mysqli_query($dbconnection , $selectQuery);
                        while($row = mysqli_fetch_array($exeQuery))
                        { 
                            $userAccessLevel = $row['user_access_level'];  
                            if(!empty($userAccessLevel))
                            { 
                                $var = explode(',',$userAccessLevel);
                                $count = count($var);
                                for ($i=0; $i < $count; $i++) {  
                                    $var[$i] = getplanName($dbconnection , $var[$i]); 
                                }
                                $var = implode(",", $var); 
                            }
                        ?>
                        <tr>
                            <td>
                                <a href="edit-student.php?userID=<?php echo $row['user_account_id']; ?>"><i class="material-icons">edit</i></a>
                            </td    >
                            <td><?php echo $row['user_name']; ?></td>
                            <td><?php echo $row['user_email']; ?></td> 
                            <td>
                        <?php if(empty($userAccessLevel))
                        {
                           echo 'No Plan';
                        }
                        else
                        {  
                           echo $var; 
                        } 
                        ?>
                        </td>
                      
                        </tr>
                        <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
</div>
<div class="content-overlay"></div>
</div>
</div>
</div> 
    <footer
        class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
        <div class="footer-copyright">
            <div class="container"><span>&copy; 2021 <a href="#" target="_blank"> Ardent MDS </a> All rights
                    reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="#"> MU3
                        Interactive </a></span></div>
        </div>
    </footer>

    <script src="js/vendors.min.js"></script>
    <script src="vendors/data-tables/js/jquery.dataTables.min.js"></script>
    <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/data-tables/js/dataTables.select.min.js"></script>
    <script src="js/plugins.min.js"></script>
    <script src="js/search.min.js"></script> 
    <script src="js/scripts/data-tables.min.js"></script>
    <script src="js/scripts/ui-alerts.min.js"></script>

    <script>
    $(document).ready(function() {
        $("#page-length-option").DataTable({
                responsive: false,
                ordering: true,
                scrollX: true,
                scrollY: false,
                columnDefs: 
                [ 
                    {
                    "targets": 0,
                    "orderable": !1
                    } 
                ]
            })
    });
    </script>
</body>

</html>