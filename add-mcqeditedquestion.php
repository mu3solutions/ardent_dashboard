<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$mainModule = '';
$subModule = '';
$childModule = '';
if (isset($_GET['mainModule'])) {
    $mainModule = decryptData($_GET['mainModule']);

}
if (isset($_GET['subModule'])) {
  $subModule = decryptData($_GET['subModule']);
}
if (isset($_GET['childModule'])) {
  $childModule =  decryptData($_GET['childModule']);
}
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title> Add New MCQ Questions | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
  <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
  <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/sweetalert/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
  <!-- END: Custom CSS-->
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

  <!-- BEGIN: Page Main-->
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper"> 
        <div class="container">
          <div class="row"> 
            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add New MCQ Question</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  <li class="breadcrumb-item"><a href="mcq-questions-list.php">MCQ Question List </a>
                  <li class="breadcrumb-item active">Add New Question
                  </li>
                </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
            <div class="card-content" id="singleQuestion">
              <div class="row ">
                <form action="./inc/_insertMCQquestions.php"  name="testForm"  method="POST" enctype="multipart/form-data">

                <div class="row">
                <div class="col m6  s12">
            <?php if (isset($_SESSION['MCQquestionsuccess'])) { ?> 
                                    <div class="card-alert card green">
                                        <div class="card-content white-text">
                                            <p><?php echo $_SESSION['MCQquestionsuccess']; ?></p>
                                        </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                            </div> 
                <?php
                }   
                unset($_SESSION['MCQquestionsuccess']);
                if (isset($_SESSION['MCQquestionerror'])) {
                ?>  
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p><?php echo $_SESSION['MCQquestionerror']; ?></p>
                        </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                    </div> 
                                <?php
                                }
                                unset($_SESSION['MCQquestionerror']);
                                ?>
            </div>
                </div>
                  <div class="row">
                      <div class="col mt-2 m4 s12">
                          <label>Select Main Module</label> 
                              <select name="mainModule" class="select2c browser-default"  id="MainModule" required>     
                                  <?php
                                  $query = "SELECT * FROM `mcq_module` ORDER BY `mcq_mid` ASC";
                                  $result = mysqli_query($dbconnection, $query);
                                  if (mysqli_num_rows($result) > 0) {
                                    while ($row = mysqli_fetch_array($result)) 
                                    { ?>
                                    <?php echo $row['mcq_mid'] ; ?>
                                      <option value="<?php echo $row['mcq_mid']; ?>"
                                      <?php
                                      if($row['mcq_mid'] == $mainModule){
                                          echo 'selected';
                                      }
                                      ?> ><?php 
                                      echo  $row['mcq_mname']; ?></option> <?php
                                    }
                                  } 
                                  else 
                                  {
                                    echo "<option>'Module Not Available'</option>";
                                  }
                                  ?>
                             </select>
                      </div>
                <div class="col mt-2 m4 s12">
                    <label>Select Sub Module</label>
                          <select  id="SubModuleSelect" class="select2c browser-default" name="subModuleValue" required>  
                          <?php 
                          $result1 = mysqli_query($dbconnection, "SELECT * FROM `mcq_sub_module` WHERE `mcq_module_id_ref` = '$mainModule' GROUP BY `mcq_sm_id` ORDER BY `mcq_sm_id` ASC");
                              if (mysqli_num_rows($result1) > 0) {
                              while ($row1 = mysqli_fetch_array($result1)) 
                              { ?>
                            <option value="<?php echo $row1['mcq_sm_id']; ?>"<?php if($row1['mcq_sm_id'] == $subModule){ echo 'selected'; } ?> ><?php  echo  $row1['mcq_sm_name']; ?></option> <?php }  }  else  {
                              echo "<option>'Sub Module Not Available'</option>";
                          }
                        ?> 
                          </select>
                      </div>
                     
                      <div class="col mt-2 m4 s12">
                          <label>Select Child Category</label>
                          <select  id="SubChildModule" class="select2c browser-default" name="subChildModuleValue" required>  
                        <?php 
                        
                        $result2 = mysqli_query($dbconnection, "SELECT * FROM `mcq_sub_module` WHERE `mcq_module_id_ref` = '$mainModule' AND `mcq_sm_id` = '$subModule' ORDER BY `mcq_sm_child_id` ASC");
                        if (mysqli_num_rows($result2) > 0) {
                        while ($row2 = mysqli_fetch_array($result2)) 
                        { 
                        ?>
                        <h2><?php echo $childModule; ?></h2>
                        <option value="<?php echo $row2['mcq_sm_child_id']; ?>" <?php
                            if($row2['mcq_sm_child_id'] == $childModule) { echo 'selected'; } ?> 
                        > 
                            <?php  echo  $row2['mcq_sm_child_name']; ?></option> <?php
                            }
                        } 
                        else 
                            {
                                echo "<option>'Sub Module Not Available'</option>";
                            }
                        ?> 
                          </select> 
                      </div>
                  </div>

                  <div class="row mt-2"> 
                    <div class="input-field col s12">
                      <textarea id="Question" class="materialize-textarea" name="Question" required></textarea>
                      <label for="Question">Question</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="option1" type="text" name="option1" required>
                      <label for="option1">Option 1</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="option2" type="text" name="option2" required>
                      <label for="option2">Option 2</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="option3" type="text" name="option3" required>
                      <label for="option3">Option 3</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="option4" type="text" name="option4" required>
                      <label for="option4">Option 4</label>
                    </div>
                  </div>

                  <div class="row ">
                    <div class="col m2 s12">
                      <label>Set Correct Answer</label>
                      <select name="correctoption" required>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                        <option value="4">Option 4</option>
                      </select>
                    </div>

                    <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Question Image : </span>
                      </label>
                    </div>


                    <div class="col m4 s12 file-field inline input-field">

                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="questionImage" name="questionImage" onchange="readURL(this);" accept="image/x-png,image/gif,image/jpeg" />
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id="fileWrapper">
                      </div>
                    </div>

                    <div class="col m4 s12 file-field inline input-field ">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image" />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
                  </div>


                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="answerExplanation" name="answerExplanation" class="materialize-textarea"></textarea>
                      <label for="answerExplanation">Answer Explanation</label>
                    </div>
                  </div>

                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="answerRef" name="answerRef" class="materialize-textarea"></textarea>
                      <label for="answerRef">Answer Reference</label>
                    </div>
                  </div>

                  <div class="row "> 
                   <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Answer Image : </span>
                      </label>
                    </div>
                    
                    <div class="col m6 s12 file-field inline input-field">  
                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="answerImage" name="answerImage[]" multiple accept="image/x-png,image/gif,image/jpeg" />
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text">
                      </div>
                    </div>
                  </div>

            
                <input type="hidden" name="addMCQQuestion" value="1">

                  <div id="preview"></div>

                  <div class="row">
                    <div class="row">
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="addMCQQuestion" id="submitButton"> Submit <i class="material-icons right">send</i>
                        </button>
                      </div>
                    </div>
                  </div>


              </div>
            </div>
            <!-- <div class="card-content pb-4" id="excelImport">
              <div id="inline-form">
                <div class="card-content"> 
                  
                  <div class="row s12 mt-4 center-align" >
                    <div class="col">
                      <label>
                        <span class="black-text"> Choose File : </span>
                      </label> 
                    </div>  
                    <div class="col s6 file-field inline input-field">
                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="excelfileImport" name="excelfileImport">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id='fileWrapper'>
                      </div>
                    </div> 
                  </div>
                  <div class="row" data-title="Step 2"  data-intro='Excel Content Should be in this format.' data-position='bottom'>
                    <div class="col m12 s12" >
                        <img src="images/exceLImage/cilnicaldemoExcel.png" alt="" height="150px" width="950px">
                        <p class="center-align-md center-align-lg center-align-sm"> (Excel Sheet should be in this format)</p>
                      </div>
                    </div>
                    <div class="row center-align" style="margin: auto;width:100%" data-title="Step 3"  data-intro='Finally Click Upload' data-position='right'>
                      <div class="input-field col s12 center-align">
                        <button class="btn cyan waves-effect waves-light ardent-orange" type="submit" name="excelImportMCQ">
                          <i class="material-icons left">insert_drive_file</i> Upload File</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div> -->
          </div>
        </div> 
      </div> 
      <script src="js/vendors.min.js"></script>  
      <script src="js/plugins.min.js"></script> 
      <script src="vendors/select2/select2.full.min.js"></script>
      <script src="js/scripts/form-select2.min.js"></script>
      <script src="js/scripts/ui-alerts.min.js"></script>
      <script src="vendors/sweetalert/sweetalert.min.js"></script>
      <script src="js/custom/custom-script.min.js"></script> 
       <script>
      $(function() 
      { 

          $(".select2c").select2(); 

      });   

    </script>   
</body> 
</html>