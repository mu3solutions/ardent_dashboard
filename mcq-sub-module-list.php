<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php'); 
?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title> MCQ Module | Ardent MDS</title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
     
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/sweetalert/sweetalert.css">
     
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="css/pages/data-tables.min.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">

                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>MCQ Sub-Module List</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                                </li>
                                <li class="breadcrumb-item active">MCQ Sub-Module  List
                                </li>
                            </ol>
                        </div>
                        <div class="col s3 m3 right">
                            <a href="add-new-mcq-submodule.php"> <button class=" btn btn-block waves-effect waves-light amber darken-4" value="Add New Students">
                                    Add New Sub Module 
                                </button></a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">
                      <div class="row">
                        <div class="col s12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="row">
                                    <div class="row">
                                <?php if (isset($_SESSION['newmcqsuccess'])) { ?>
                                <div class="col m6">
                                    <div class="card-alert card green">
                                        <div class="card-content white-text">
                                            <p><?php echo $_SESSION['newmcqsuccess']; ?></p>
                                        </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                            </div>
                </div>
                <?php
                }   
                unset($_SESSION['newmcqsuccess']);
                if (isset($_SESSION['newmcqerror'])) {
                ?> 
                <div class="col m6">
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p><?php echo $_SESSION['newmcqerror']; ?></p>
                        </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                </div>
                                <?php
                                }
                                unset($_SESSION['newmcqerror']);
                                ?>
                                </div> 
                                <div class="col s12">
                                    <table id="page-length-option" class="display highlight">
                                    <thead>
                                        <tr>
                                            <th>Edit</th>  
                                            <th>Main Module Name</th>
                                            <th>Sub Module Name</th>
                                            <th>Child Module Name</th> 
                                            <th>MCQ Status</th>
                                            <th>MCQ Access Status</th>
                                        </tr>
                                    </thead>
                                <tbody>
                                <?php
                                    $answers = array();
                                    $selectQuery = "SELECT `mcq_module`.`mcq_mid`, `mcq_sub_module`.`mcq_module_id_ref` , `mcq_module`.`mcq_mname`,`mcq_sub_module`.`mcq_sm_name`,`mcq_sub_module`.`mcq_sm_child_name`,`mcq_sub_module`.`mcq_test_open` , `mcq_sub_module`.`mcq_sm_child_id`, `mcq_sub_module`.`mcq_status` FROM `mcq_sub_module` JOIN `mcq_module` ON `mcq_module`.`mcq_mid` = `mcq_sub_module`.`mcq_module_id_ref` ORDER BY `mcq_sm_child_id` DESC ";
                                    $exeQuery  = mysqli_query($dbconnection, $selectQuery);
                                    while ($row = mysqli_fetch_array($exeQuery)) { 

                                        if($row['mcq_test_open'] == '0')
                                        {  
                                            $chipColor  = 'chip green lighten-5';
                                            $chipTextColor  = 'green-text';
                                            $chipText  = 'Opened';
                                        }else if ($row['mcq_test_open'] == '1') 
                                        {
                                            $chipColor  = 'chip red lighten-5';
                                            $chipTextColor  = 'red-text';
                                            $chipText  = 'Locked'; 
                                        }
                                        
                                        if($row['mcq_status'] == '1')
                                        {  
                                            $chipColor1  = 'chip green lighten-5';
                                            $chipTextColor1  = 'green-text';
                                            $chipText1  = 'Active';
                                        }else if ($row['mcq_status'] == '0') 
                                        {
                                            $chipColor1  = 'chip red lighten-5';
                                            $chipTextColor1  = 'red-text';
                                            $chipText1  = 'In-Active'; 
                                        }
                                    ?>
                                <tr>
                                    <td>
                                        <a href="edit-sub-module.php?mid=<?php echo encryptData($row['mcq_sm_child_id']); ?>"><i class="material-icons">edit</i></a> 
                                    </td>  
                                    <td><?php echo fetchData($dbconnection,'mcq_mname','mcq_module',$row['mcq_module_id_ref'],'mcq_mid'); ?></td>
                                    <td><?php echo $row['mcq_sm_name']; ?></td>
                                    <td><?php echo truncate($row['mcq_sm_child_name'] ,30) ; ?></td> 
                                    <td>
                                        <span class="<?php echo $chipColor1; ?>">
                                            <span class="<?php echo $chipTextColor1; ?>">
                                                <?php echo $chipText1; ?>
                                            </span>
                                        </span>
                                    </td> 
                                    <td>
                                        <span class="<?php echo $chipColor; ?>">
                                            <span class="<?php echo $chipTextColor; ?>">
                                                <?php echo $chipText; ?>
                                            </span>
                                        </span>
                                    </td> 
                                </tr>
                                <?php }   ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- START RIGHT SIDEBAR NAV -->
                    <!-- END RIGHT SIDEBAR NAV -->
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div> 
    <script src="js/vendors.min.js"></script>
    <script src="js/plugins.min.js"></script> 
    <script src="vendors/data-tables/js/jquery.dataTables.min.js"></script> 
    <script src="js/scripts/ui-alerts.min.js"></script> 
    <script src="js/custom/custom-script.min.js"></script> 
     <script> 
        $(function() {
            $("#page-length-option").DataTable({
                responsive: false,
                pageLength: 50,
                ordering: true,
                scrollX: true,
                scrollY: false 
            })
        });
    </script> 
</body> 
</html>