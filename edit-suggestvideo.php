
<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$videoID = '';
if (isset($_GET['vid'])) {
  $videoID =  decryptData($_GET['vid']);
}
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title>Edit Suggested Videos | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/ardentlogo.png">
  <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
  <link rel="stylesheet" type="text/css" href="vendors/sweetalert/sweetalert.css">

  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
  <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
  <link rel="stylesheet" href="vendors/introjs/css/introjs.css"> 
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
  <!-- END: Custom CSS-->
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns" data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

   <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

  <!-- BEGIN: Page Main-->
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span>Edit Suggested Videos </span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                <li class="breadcrumb-item"><a href="suggested-videos-list.php">View Suggested Video List </a>
                <li class="breadcrumb-item active">Edit Suggested Videos
                </li>
              </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
            <div class="card-content padding-3" id="singleQuestion">
              <div class="row">
                <form action="./inc/_suggestedVideos.php" method="POST" enctype="multipart/form-data">
                <?php
                  if(!empty($videoID)) {
                $sql = "SELECT * FROM `suggested_videos` WHERE `id`='$videoID'";
                $result = mysqli_query($dbconnection, $sql);
                if(mysqli_num_rows($result) > 0){

                  if($row = mysqli_fetch_array($result)) {  
                 $timestamp = explode(",",$row['video_title_timestamp']);  
                 ?>  

                <input type="hidden" name="videoId" value="<?php echo $row['id']; ?>">
                <div class="row "> 
                  <div class="col m5 s12">
                      <label>Choose Tutor</label>
                        <select name="chooseTutor" id="chooseTutor" required>
                          <option value="" disabled selected >Choose Tutor</option>
                            <?php 
                            $sqls = "SELECT * FROM `tutordetails`";
                            $results = mysqli_query($dbconnection, $sqls);
                            while ($rows = mysqli_fetch_array($results)) 
                            {?>
                            <option value="<?php echo $rows['tutorId']; ?>" <?php if($rows['tutorId'] == $row['tutorId']){ echo 'selected'; } ?> ><?php echo $rows['tutorName']; ?></option>    
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col m7 s12 ">
                      <label for="videoUrl">Video URL</label> 
                      <input id="videoUrl" type="url" name="videoUrl" value="<?php echo $row['sm_videos_url']; ?>" required> 
                    </div>   
                    
                  </div>  
                   <div class="row mt-3 mb-3 "> 
                      <div class="col m7 s12">
                            <label for="videoName">Video Name</label> 
                              <input id="videoName" type="text" name="videoName" value="<?php echo $row['vedio_sm_name']; ?>" required> 
                      </div>  
                  </div>
                  
                  <div class="row mt-3 mb-3">
                        <div class="col m3 s12 mt-3">
                            <label>
                              <span class="black-text"> Upload Video Banner Image : </span>
                            </label>
                        </div>


                        <div class="col m6 s12 file-field inline input-field">
                            <div class="btn float-right inline ardent-orange">
                                <span>Choose File</span>
                                <input type="file" id="questionImage" name="questionImage" accept="image/x-png,image/gif,image/jpeg"  onchange="readImagURL(this,555,531,400,200);">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate valid" type="text" id="fileWrapper">
                                <span class="helper-text ardent-text" >( Image Width and Height must be 555 * 531  )</span> 
                            </div>
                        </div> 
                    </div>

                    <div class="row"> 
                    <div class="col m8">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>New Selected Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image"  />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
                  </div>


                <div class="row "> 
                
                      <div class="col m6 s4" id="timeStamp-div">  
                      <br>
                        <label >Video Timestamp</label>
                        <?php for ($i=0; $i < count($timestamp); $i++) {   ?> 
                          <div class="row" >
                            <div class="removethisDiv">
                            <div class="col m3">
                                <input id="videoTimestamp" class="formatting" type="text" name="videoTimestamp[]" 
                                value="<?php   
                                $output = explode('-',$timestamp[$i]);
                                echo trim($output[0]);  ?>" placeholder="00:00:00" required>
                              </div>
                              <div class="col m9 "> 
                                <input id="videoTimestampText" type="text" placeholder="Topic Name" name="videoTimestampText[]" value="<?php echo trim($output[1]);?>" required>
                              </div> 
                            </div>
                              
                          </div>
                        <?php } ?> 
 
                    </div>
                    <div class="col m5 video-prev">
                      <label for="">Current Video : </label>
                      <br>
                      <video src="<?php echo $row['sm_videos_url'];?>" controls width="400" class="border-radius-10 "></video> 
                    </div> 
                </div>
                <div class="row ">  
                <div class="col  s12 ">
                        <button class="btn ardent-orange waves-effect waves-light green accent-4" type="button" id="addNewBtn" name="editClinical">Add Timestamp
                        <i class="material-icons right">add</i>
                        </button>
                        <button class="btn btn-warning waves-effect waves-light red accent-3" type="button" id="addNewRemove" name="editClinical">Remove Timestamp
                        <i class="material-icons right">close</i>
                        </button>
                    </div>
                </div>
                 

                <div class="divider  mt-3 "></div>
                    
                        <?php if(!empty($row['video_front_image']))
                        {?> 
                            <div class="row ">  
                                <div class="col m3 s12 mt-3">
                                    <p>Current Video Banner Image:</p>
                                </div>
                                <div class="col m6 mt-3 mb-2 s12 "> 
                                    <img src="<?php  echo $row['video_front_image'];?>" class="border-radius-10" alt="" width="400"   >
                                </div>
                            </div>
                        <div class="divider mb-2"></div>
                      <?php } ?>
 
                  <div class="row mt-3">
                    <div class="input-field col s12 center">
                      <button class="btn ardent-orange waves-effect waves-light" type="submit" name="editSuggestedVideos">Save Changes
                        <i class="material-icons right">send</i>
                      </button>
                    </div> 
                  </div>
                  
                  <?php }  ?> 
             <?php  }else { ?>
              <div class="row center-align">  
                <div class="col m12 s12">
                    <h3 class="text-center"> Your Link is Broken</h3>
                    <img class="z-depth-4 circle" width="80" height="80" src="./images/favicon/no-access-icon.png " alt="">
                </div> 
            </div>
             <?php }  } else { ?>
             <div class="row center-align">  
                <div class="col m12 s12">
                    <h3 class="text-center"> Your Link is Broken</h3>
                    <img class="z-depth-4 circle" width="80" height="80" src="./images/favicon/no-access-icon.png " alt="">
                </div> 
            </div>

             <?php } ?>
                </form>
              </div>
            </div> 
          </div>
        </div> 
      </div> 
    </div>
  </div>
  <!---ENd MAin -->
      <script src="js/vendors.min.js"></script> 
      <script src="js/plugins.min.js"></script>     
      <script src="vendors/formatter/jquery.formatter.min.js"></script> 
      <script src="vendors/sweetalert/sweetalert.min.js"></script> 
      <script src="js/custom/custom-script.min.js"></script>
      <script>
        $(function(){
          $('.formatting').formatter({
            'pattern': '{{99}}:{{99}}:{{99}}'
          });
        }); 
      </script>   
</body> 
</html>