<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> 
    <title> Suggested Videos | Ardent MDS</title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css"> 
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="css/pages/data-tables.min.css"> 
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css"> 
</head> 

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">

                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>Suggested Videos List </span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Suggested Videos List
                                </li>
                            </ol>
                        </div>
                        <div class="col s3 m4 right">
                            <a href="add-new-suggested-video.php"> <button class=" btn btn-block waves-effect waves-light amber darken-4" value="Add New Students">
                                    Add New Suggested Video
                                </button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">
                    <div class="row">
                        <div class="col s12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="row">
                                            <div class="row">
        <?php if (isset($_SESSION['suggestedVideosSuccess'])) {
        ?>
            <div class="col m6">
                <div class="card-alert card green">
                    <div class="card-content white-text">
                        <p><?php echo $_SESSION['suggestedVideosSuccess']; ?></p>
                    </div>
                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                </div>
                <?php
                }
                unset($_SESSION['suggestedVideosSuccess']);
                if (isset($_SESSION['suggestedVideosError'])) {
                ?> 
                <div class="col m6">
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p><?php echo $_SESSION['suggestedVideosError']; ?></p>
                        </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                </div>
                                <?php
                                }
                                unset($_SESSION['suggestedVideosError']);
                                ?>
                                </div>
                                <div class="col s12"> 
                                <table class="bordered centered">
                                    <thead>
                                        <tr>
                                        <th>Edit</th>
                                        <th>Video</th>
                                        <th>Video Name</th> 
                                        <th>Tutor Name</th> 
                                        <th>Tutor Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                        $selectQuery = "SELECT * FROM `suggested_videos` ";
                        $exeQuery  = mysqli_query($dbconnection, $selectQuery);
                        while ($row = mysqli_fetch_array($exeQuery)) {
                            $tutorId = $row['tutorId'];
                            $exequery =  mysqli_query($dbconnection,"SELECT * FROM `tutordetails` WHERE `tutorId`='$tutorId'");
                            if($rows = mysqli_fetch_array($exequery)){
                                $tutorName =  $rows['tutorName']; 
                                $tutorDesc =  $rows['tutorDesc']; 
                            } 
                                    ?>
                                    <tr>
                                        <td>   <a href="edit-suggestvideo.php?vid=<?php echo encryptData($row['id']);  ?>"><i class="material-icons">edit</i>Edit</a> </td>
                                        <td>
                                            <video src="<?php echo $row['sm_videos_url']; ?>"  controls width="300" class="border-radius-10"></video>
                                        </td>
                                        <td> <?php echo $row['vedio_sm_name']; ?> </td>
                                        <td> <?php echo $tutorName; ?> </td>
                                        <td><?php echo  truncate($tutorDesc,80); ?></td>  
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- START RIGHT SIDEBAR NAV -->
                    <!-- END RIGHT SIDEBAR NAV -->
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div> 
    <script src="js/vendors.min.js"></script>
    <script src="vendors/data-tables/js/jquery.dataTables.min.js"></script>
    <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="js/scripts/ui-alerts.min.js"></script> 
    <script src="js/plugins.min.js"></script> 
    <script src="js/custom/custom-script.min.js"></script> 
    <script src="js/scripts/data-tables.min.js"></script>   
    <script> 
        $(function() {
            $("#page-length-option").DataTable({
                responsive: false,
                ordering: true,
                scrollX: true,
                scrollY: false
            })
        });
    </script> 
</body> 
</html>