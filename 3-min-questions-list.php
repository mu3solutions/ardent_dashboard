<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$testDate = ''; 
if (isset($_GET['testDate'])) {
  $testDate = decryptData($_GET['testDate']) ;
} 
?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title> 3 Min Challenges Questions | Ardent MDS</title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
     
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/sweetalert/sweetalert.css">
     
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="css/pages/data-tables.min.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">

                    <div class="row">
                        <div class="col s10 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>3 Min Challenges Questions List</span></h5>
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                                </li> 
                                <li class="breadcrumb-item active">3 Min Challenges Questions List
                                </li>
                            </ol>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">
                      <div class="row">
                        <div class="col s12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="row">
                                        <div class="row">
                                <?php if (isset($_SESSION['ntquestionsuccess'])) { ?>
                                <div class="col m6">
                                    <div class="card-alert card green">
                                        <div class="card-content white-text">
                                            <p><?php echo $_SESSION['ntquestionsuccess']; ?></p>
                                        </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                            </div>
                </div>
                <?php
                }   
                unset($_SESSION['ntquestionsuccess']);
                if (isset($_SESSION['ntquestionerror'])) {
                ?> 
                <div class="col m6">
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p><?php echo $_SESSION['ntquestionerror']; ?></p>
                        </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                </div>
                                <?php
                                }
                                unset($_SESSION['ntquestionerror']);
                                ?>
                                </div> 
                                <div class="col s12">
                                    <table id="page-length-option" class="display highlight">
                                    <thead>
                                        <tr>
                                            <th>Edit</th>  
                                            <th>Q No</th>
                                            <th>Test Date</th>
                                            <th>Question</th>
                                            <th>Option 1</th>
                                            <th>Option 2</th>
                                            <th>Option 3</th>
                                            <th>Option 4</th>
                                        </tr>
                                    </thead>
                                <tbody>
                        <?php
                        $slno = 1;
                            $answers = array();
                            $selectQuery = "SELECT * FROM `tbl_nuggets_qa` WHERE `test_date` = '$testDate' ORDER BY `nuggets_qn_id` ASC";
                            $exeQuery  = mysqli_query($dbconnection, $selectQuery);
                            while ($row = mysqli_fetch_array($exeQuery)) {
                            $answers = explode('$&', $row['nuggets_answers']);  
                            ?>
                                <tr>
                                    <td>
                                        <a href="edit-3-min-questions.php?qid=<?php echo encryptData($row['nuggets_qn_id']); ?>"><i class="material-icons">edit</i></a> 
                                    </td>  
                                    <td><?php echo $slno; ?></td>
                                    <td><?php echo $row['test_date']; ?></td>
                                    <td><?php echo truncate($row['nuggets_qn_desc'], 40); ?></td>
                                    <td><?php echo truncate($answers[0], 25); ?></td>
                                    <td><?php echo truncate($answers[1], 25); ?></td>
                                    <td><?php echo truncate($answers[2], 25); ?></td>
                                    <td><?php echo truncate($answers[3], 25); ?></td>
                                </tr>
                                <?php $slno++; }   ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- START RIGHT SIDEBAR NAV -->
                    <!-- END RIGHT SIDEBAR NAV -->
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div> 
    <script src="js/vendors.min.js"></script>
    <script src="vendors/data-tables/js/jquery.dataTables.min.js"></script>
    <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/sweetalert/sweetalert.min.js"></script> 
    <script src="js/plugins.min.js"></script> 
    <script src="js/scripts/data-tables.min.js"></script>   
    <script src="js/custom/custom-script.min.js"></script> 
    <script src="js/scripts/ui-alerts.min.js"></script>
    <script> 
        $(function() {
            $("#page-length-option").DataTable({
                responsive: false,
                ordering: true,
                scrollX: true,
                scrollY: false,
                pageLength:100
            })
        });
    </script> 
</body> 
</html>