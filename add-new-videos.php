<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php'); 
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title>Add New Video | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/ardentlogo.png">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css"> 
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/sweetalert/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
  <!-- END: Custom CSS-->
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns" data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

   <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

  <!-- BEGIN: Page Main-->
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add New Videos </span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                <li class="breadcrumb-item"><a href="video-sub-module-list.php">View Video List </a>
                <li class="breadcrumb-item active">Add New Videos 
                </li>
              </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
            <div class="card-content padding-3" id="singleQuestion">
              <div class="row">
                <form action="./inc/_videoController.php" method="POST" enctype="multipart/form-data">   
                <div class="row "> 
                    <div class="col m5 s12 mt-2">
                    <select name="chooseTutor" id="chooseTutor" required>
                        <option value="" disabled selected >Choose Tutor</option>
                        <?php 
                          $sqls = "SELECT * FROM `tutordetails`";
                          $results = mysqli_query($dbconnection, $sqls);
                          while ($rows = mysqli_fetch_array($results)) 
                          {
                        ?>
                            <option value="<?php echo $rows['tutorId']; ?>"><?php echo $rows['tutorName']; ?></option>    
                        <?php 
                          } 
                        ?>
                        </select>
                    </div>  

                    <div class="col m7 s12">
                      <label for="videoUrl">Video URL</label>
                      <input type="url" id="videoUrl" class="materialize-textarea" name="videoUrl" required> 
                    </div>   
                  </div>


                  <div class="row mt-2">
                    
                  <div class="col m4 s12">
                  <label for="videoName">Select Main Module</label> 
                    <select name="mainModule" id="mainModule" required>
                        <option value="" disabled selected >Choose Main Module</option>
                        <?php 
                        $sqlsv = "SELECT * FROM `vedio_module`";
                        $resultsv = mysqli_query($dbconnection, $sqlsv);
                        while ($rowsv = mysqli_fetch_array($resultsv)) 
                        {?>
                        <option value="<?php echo $rowsv['mvid']; ?>"><?php echo $rowsv['mvname']; ?></option>    
                        <?php } ?>
                        </select>
                    </div> 
                    
                    <div class="col m4 s12">
                        <label for="videoName">Video Name</label> 
                        <input id="videoName" type="text" name="videoName" required> 
                    </div> 
                    
                    <div class="col m3 s12">
                        <label for="videoLength">Video length</label> 
                        <input id="videoLength" type="text" name="videoLength" required> 
                    </div>  
                  </div>

                    <div class="row mt-3">  
                        <div class="col m6 s12">
                            <label>Set Access Status</label>
                            <select name="video_status" required>
                                <option value="0"> Opened </option>
                                <option value="1"> Locked </option> 
                            </select>
                        </div>
                        <div class="col m6 s12">
                            <label>Set Active Status</label>
                            <select name="status" required>
                                <option value="0"> In-Active </option>
                                <option value="1"> Active </option> 
                            </select>
                        </div>
                    </div>  
  

                  <div style="display:none;" class='video-prev' class="pull-right">
                        <video height="200" width="300" class="video-preview" controls="controls"  >
                  </div>

                <div class="row "> 
                
                      <div class="col m6 s4" id="timeStamp-div">  
                      <br>
                         <label >Video Timestamp</label> 
                          <div class="row" >
                            <div class="removethisDiv">
                                <div class="col m3">
                                  <input id="videoTimestamp" class="formatting" type="text" name="videoTimestamp[]" 
                                  placeholder="00:00:00" required>
                                </div>
                                <div class="col m9 "> 
                                  <input id="videoTimestampText" type="text" placeholder="Topic Name" name="videoTimestampText[]" required>
                                </div> 
                            </div> 
                          </div>  
                    </div>
                    <div class="col m5 video-prev mt-2"> 
                      <div class="display-none mt-3" id="videoDiv">
                        <label for="">Selected  Video  :</label>
                        <br>
                        <video  controls width="400" class="border-radius-10 " id="video-preview" data-setup='{ "inactivityTimeout": 0 }'></video> 
                      </div>
                    </div> 
                </div>
                <div class="row">  
                <div class="col s12">
                        <button class="btn ardent-orange waves-effect waves-light green accent-4" type="button" id="addNewBtn" name="editClinical">Add Timestamp
                            <i class="material-icons right">add</i>
                        </button>
                        <button class="btn btn-warning waves-effect waves-light red accent-3" type="button" id="addNewRemove" name="editClinical">Remove Timestamp
                            <i class="material-icons right">close</i>
                        </button>
                    </div>
                </div>
                 

 
                  <div class="row mt-3">
                    <div class="input-field col s12 center">
                      <button class="btn ardent-orange waves-effect waves-light" type="submit" name="newVideos">Add Video
                        <i class="material-icons right">send</i>
                      </button>
                    </div> 
                  </div> 
                </form>
              </div>
            </div> 
          </div>
        </div> 
      </div> 
    </div>
  </div>
  <!---ENd MAin -->
      <script src="js/vendors.min.js"></script> 
      <script src="js/plugins.min.js"></script>     
      <script src="vendors/formatter/jquery.formatter.min.js"></script>  
      <script src="vendors/sweetalert/sweetalert.min.js"></script>
      <script src="js/custom/custom-script.min.js"></script>
      <script>
        $(function(){
          $('.formatting').formatter({
            'pattern': '{{99}}:{{99}}:{{99}}'
          });
        });  
      </script>   
</body> 
</html>