 <?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
?>

<!DOCTYPE html>
  
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Add New Student | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
    <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
    <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head>
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->


    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add New Student</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="student-list.php">Student's List</a>
                  </li>
                  <li class="breadcrumb-item active">Add New Student
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
          <div class="row"> 
          <form action="./inc/_insertStudentDetail.php" method="POST"> 
            <div class="row">
              <div class="input-field col m4 s12"> 
                <input id="username" type="text" name="studentName" required  class="validate"> 
                <label for="username">Student Name</label>
              </div>
              <div class="input-field col m4 s12"> 
                <input id="userEmail" type="email" name="studentEmail" required>
                <label for="userEmail">Student Email</label>
              </div>
              <div class="input-field col m4 s12"> 
              <input id="userNumber"  name="studentNumber" type="tel" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" required>
                <label for="userNumber">Student Mobile Number</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col m5 s12"> 
                <input id="userCollege" type="text" name="studentCollege" required>
                <label for="userCollege">College Name</label>
              </div>
              <div class="input-field col m5 s12"> 
                <input id="userState" type="text" name="studentState" required>
                <label for="userState">College State</label>
              </div> 
              <div class="input-field col m2 s12"> 
                <input id="userCollegeYear" type="text" name="studentCollegeYear" required>
                <label for="userCollegeYear">College Academic Year</label>
              </div>
            </div>
            <div class="row">
              <div class="col m6 s12">
                      <label>Select Subscription Plan</label>
                  <div class="input-field"> 
                      <select class="select2 browser-default" name="subPlan" id="subPlan">
                      <option value="" selected disabled>Select Plan</option>
                        <?php 
                        $selectPlans = "SELECT * FROM `user_plans` ";
                        $exeSelect   = mysqli_query($dbconnection , $selectPlans);
                        while($row = mysqli_fetch_array($exeSelect)) { 
                        ?>
                        <option value="<?php echo $row['user_plan_type']; ?>" >  
                          <?php echo $row['plan_name'].' - '.' ₹ '.$row['plan_cost'];    ?> 
                        </option> 
                        <?php } ?>
                      </select>
                  </div>
              </div> 

              <div class="col m6 s12  display-none" id='childPlan'>
               <label>Select Subscription Plan</label>
                  <div class="input-field"> 
                    <select class="select2 browser-default" multiple="multiple" name="childPlan[]" id="childPlanSelect">  
                      <?php 
                      $selectPlans = "SELECT * FROM `user_sub_plans` WHERE `plan_id`=2";
                      $exeSelect  = mysqli_query($dbconnection , $selectPlans);
                      while ($rowss = mysqli_fetch_array($exeSelect)) {  ?> 
                        <option value="<?php echo $rowss['user_plan_type']; ?>"> 
                            <?php echo $rowss['plan_name'] .' - '.'₹ '.$rowss['plan_cost'];  ?> 
                        </option>   
                      <?php } ?> 
                    </select>
                  </div>
              </div>
              
            </div>  
            
              <div class="row">   
                <div class="input-field col s12 center"> 
                  <button class="btn ardent-orange waves-effect waves-light" type="submit" name="submit">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>

              </form>    
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

    <footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
      <div class="footer-copyright">
        <div class="container"><span>&copy; 2021 <a href="#" target="_blank"> Ardent MDS </a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="#"> MU3 Interactive </a></span></div>
      </div>
    </footer> 


    <script src="js/vendors.min.js"></script> 
    <script src="vendors/select2/select2.full.min.js"></script> 
    <script src="js/plugins.min.js"></script>
    <script src="js/search.min.js"></script>
    <script src="js/custom/custom-script.min.js"></script> 
    <script src="js/scripts/form-select2.min.js"></script> 
    <script> 
    $(function(){  
      $('#subPlan').on('change', function() 
        {
          var subPlan = $(this).val(); 
          var select = $('#childPlanSelect');
          if(subPlan =='S2')
          {  
            $('#childPlan').show('500'); 
            select.attr('required', true);
          }else{
            $('#childPlan').hide('500'); 
            select.attr('required', false);
          }
         });
    });
    </script>
  </body> 
</html>