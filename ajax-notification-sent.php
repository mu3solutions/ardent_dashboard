<?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php'); 
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title> Send Push Notification | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css"> 

  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
  <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
  <link rel="stylesheet" href="vendors/introjs/css/introjs.css"> 

  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">

</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

  <!-- BEGIN: Header-->
  <?php include_once('inc/header.php'); ?>
  <!-- End: Header-->


  <!-- BEGIN: SideNav-->
  <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-rounded">
        <div class="brand-sidebar">
            <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="index.html"><img class="hide-on-med-and-down" src="images/logo/014.png" alt="materialize logo" /><img class="show-on-medium-and-down hide-on-med-and-up" src="images/logo/014.png" alt="materialize logo" /><span class="logo-text hide-on-med-and-down">Ardent</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
        </div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
            <li class="navigation-header"><a class="navigation-header-text">Applications</a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="bold"><a class="waves-effect waves-cyan " href="home-dashboard.php"><i class="material-icons">settings_input_svideo</i><span class="menu-title">Dashboard</span></a>
            </li>
            <li class="navigation-header"><a class="navigation-header-text">Pages </a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">face</i><span class="menu-title" >Student Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a href="student-list.php"><i class="material-icons">radio_button_unchecked</i><span>View Student's
                                    List</span></a>
                        </li>
                        <li><a href="add-new-student.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Student</span></a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="bold "><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">photo_filter</i><span class="menu-title" data-i18n="Menu levels">Ardent Nuggets</span></a>
                <div class="collapsible-body" >
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span data-i18n="3 Min Challenge">3 Min Challenge</span></a>
                            <div class="collapsible-body">
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li><a href="3-min-challenges.php"><i class="material-icons">radio_button_unchecked</i><span>3-Min-Challenge List</span></a>
                                    </li>
                                    <li><a href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Third level">Add New 3-min Challenge</span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Second level child">Clinical Vignettes</span></a>
                            <div class="collapsible-body" >
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li><a href="cliniccal-vignets-list.php" ><i class="material-icons">radio_button_unchecked</i><span> Clinical Vignettes List </span></a>
                                    </li>
                                    <li><a href="add-new-clinical.php"><i class="material-icons">radio_button_unchecked</i><span>Add Clinical Vignettes</span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li  ><a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span>10 Min Concept</span></a>
                            <div class="collapsible-body" > 
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li><a href="10-min-concept.php"><i class="material-icons">radio_button_unchecked</i><span>Edit 10 Mins Concept</span></a>
                                    </li> 
                                </ul>
                            </div>
                        </li>

                        <li class="bold open"><a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span>Trending Videos</span></a>
                            <div class="collapsible-body"> 
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li><a  href="trending-videos.php"><i class="material-icons">radio_button_unchecked</i><span>Trending Videos List</span></a>
                                    </li> 
                                </ul>
                            </div>
                        </li>

                        <li  class="bold"><a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i class="material-icons">radio_button_unchecked</i><span>Quote Of The Day</span></a>
                            <div class="collapsible-body"> 
                                <ul class="collapsible" data-collapsible="accordion">
                                    <li><a href="quote-list.php"><i class="material-icons">radio_button_unchecked</i><span>Quote Of The Day List</span></a>
                                    </li> 
                                    <li><a href="add-new-quote.php"><i class="material-icons">radio_button_unchecked</i><span>Add New Quote</span></a>
                                    </li> 
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">assignment</i><span class="menu-title" >MCQ Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a href="mcq-module-list.php"><i class="material-icons">radio_button_unchecked</i><span>MCQ Main Module</span></a>
                        </li>
                        <li><a href="mcq-sub-module-list.php"><i class="material-icons">radio_button_unchecked</i><span>MCQ Sub Module</span></a>
                        </li>
                        <li><a href="add-new-mcq-questions.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Questions</span></a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">assignment_turned_in</i><span class="menu-title" >Grand Test Module</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a href="grand-test-list.php"><i class="material-icons">radio_button_unchecked</i><span>Grand Test List</span></a>
                        </li> 
                        <li><a href="add-new-gtquestions.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Questions</span></a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">ondemand_video
            </i><span class="menu-title" >Suggested Videos</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a  href="suggested-videos-list.php"><i class="material-icons">radio_button_unchecked</i><span>Suggested Videos List</span></a>
                        </li> 
                        <li><a href="add-new-suggested-video.php"><i class="material-icons">radio_button_unchecked</i><span>Add
                                    New Suggested Video</span></a>
                        </li>
                    </ul>
                </div>
            </li> 
            <li class="navigation-header"><a class="navigation-header-text">  notification </a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="bold active"><a class="active waves-effect waves-cyan " href="send-push-notifications.php"><i class="material-icons">notifications</i><span class="menu-title">Send Notification</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">ondemand_video
            </i><span class="menu-title" >Fastest Finger</span> </a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a  href="suggested-videos-list.php"><i class="material-icons">radio_button_unchecked</i><span>Add FF Questions</span></a>
                        </li> 
                        <li><a href="add-new-suggested-video.php"><i class="material-icons">radio_button_unchecked</i><span> Send Quiz</span></a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>
    <!-- END: SideNav-->

  <!-- BEGIN: Page Main-->
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span>Send Mobile Notification</span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a> 
                <li class="breadcrumb-item active">Send Mobile Notification
                </li>
              </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
            <div class="card-content" id="singleQuestion">
              <div class="row ">
                <!-- <form action="./inc/_sendNotification.php" method="POST" enctype="multipart/form-data">  -->
                <form class="formValidate" id="formValidate" >
                    <div class="row">
                      <div class="col m6  s12 display-none" id="showDiv"> 
                        <div class="card-alert card green">
                          <div class="card-content white-text">
                              <p>Notification Sent Successfully</p>
                          </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>   
                      </div> 
                    </div>     
                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <label for="notificationTitle">Notification Title</label>
                      <input id="notificationTitle" name="notificationTitle" type="text" data-error=".errorTxt1"> 
                      <small class="errorTxt1"></small>
                    </div>
                  </div>
                  
                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="notificationMessage" class="materialize-textarea" name="notificationMessage"  data-error=".errorTxt7" required></textarea>
                      <label for="notificationMessage">Notification Message</label>
                      <small class="errorTxt7"></small>
                    </div>
                  </div> 

                  <div class="row "> 

                    <div class="col m3 s12 mt-3">
                      <label>
                        <span class="black-text"> Choose Notification Image : </span>
                      </label>
                    </div>


                    <div class="col m4 s12 file-field inline input-field"> 
                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="questionImage" name="questionImage" onchange="readURL(this);">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id="fileWrapper">
                      </div>
                    </div>

                    <div class="col m4 s12 file-field inline input-field ">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image" />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
                  </div>  


                  <div class="row"> 
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light"        type="button" name="sendNotification" id="sendNotification" > Submit <i class="material-icons right">send</i>
                        </button>
                      </div> 
                  </div>


              </div>
            </div> 
          </div>
        </div> 
      </div> 
      <script src="js/vendors.min.js"></script>  
      <script src="js/plugins.min.js"></script>  
      <script src="vendors/jquery-validation/jquery.validate.min.js"></script>
      <script src="js/custom/custom-script.min.js"></script>   
      <script>
$(function(){

        
    $("#formValidate").validate({
          rules: {
            notificationTitle: {
            required: true, 
          },
            notificationMessage : {
            required: true
          } 
    },
    //For custom messages
      messages: {
        notificationTitle: {
          required: "Enter a Notification Title"
        },
        notificationMessage : {
          required: "Enter a Notification Message"
        },
        curl: "Enter your website",
      },
      errorElement: 'div',
      errorPlacement: function (error, element) {
        var placement = $(element).data('error');
        if (placement) {
          $(placement).append(error)
        } else {
          error.insertAfter(element);
        }
      }
    });
  
  $("#sendNotification").click(function(){
     var yes = $("#formValidate").valid();
    //  var notificationTitle = $("#notificationTitle").val();
    //  var notificationMessage = $("#notificationMessage").val();
     var jform = new FormData();
      jform.append('notificationTitle',$('#notificationTitle').val());
      jform.append('notificationMessage',$('#notificationMessage').val());
      jform.append('questionImage',$('#questionImage').get(0).files[0]); // Here's the important bit

     if(yes){
            $.ajax({
                url: "./inc/_sendNotification.php", 
                type: 'POST',
                data: jform,
                mimeType: 'multipart/form-data', // this too
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() { 
                   $("#sendNotification").html('Please Wait...'); 
                 },
                success:function(data)
                {    
                    // if(data=='sent'){ 
                      alert(data);
                        $('#name').val(''); 
                        $('#subject').val(''); 
                        $('#showDiv').show();
                        $("#sendNotification").html('<span>Submitted</span>');
                        
                    // }
                }
        });
     } 
    });
  
  
});  
</script>
</body> 
</html>