<?php 
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$planId= '';
$planType = '';
if(isset($_GET['pid'])){
$planId = decryptData($_GET['pid']); 
}
if(isset($_GET['ptype'])){
  $planType = decryptData($_GET['ptype']); 
} 
?>

<!DOCTYPE html>   
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> 
    <title>Edit Plan Module | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head>
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->
  <!-- END: SideNav-->


    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Edit Subscription Plan </span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="subscription-plan-list.php"> Subscription Plan List</a>
                  </li>
                  <li class="breadcrumb-item active">Edit Subscription Plan 
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
          <div class="row"> 
          <form action="./inc/_subscriptionPlan.php" method="POST"> 
          <?php 
          if(!empty($planId) && !empty($planType)) { 
            if($planType == 'type1')
            {
              $sql = "SELECT * FROM `user_plans`  WHERE `plan_id` = '$planId'"; 
            }
            else if($planType == 'type2')
            {
              $sql = "SELECT * FROM `user_sub_plans`  WHERE `sl_no` = '$planId'"; 
            }

                $result = mysqli_query($dbconnection, $sql);
                if(mysqli_num_rows($result) > 0){ 
                  if  ($row = mysqli_fetch_array($result)) {  
                    switch ($planType) {
                      case 'type1': 
                        $planName = $row['plan_name'];
                        $plancost = $row['plan_cost'];
                        $planExpiry = $row['plan_expiry'];
                        $planDesc = $row['plan_description'];
                        $planFullDesc = $row['plan_full_description'];
                        $cancelPrice = $row['cancelled_price'];
                        break;

                      case 'type2': 
                        $planName = $row['plan_name'];
                        $plancost = $row['plan_cost'];
                        $planExpiry = $row['plan_expiry'];
                        $planDesc = $row['plan_description'];
                        $planFullDesc = $row['plan_full_description'];
                        $cancelPrice = $row['cancelled_price'];
                        break;
                    }
                  ?>   
            <div class="row"> 
            <input type="hidden" name="uniqueId" value="<?php echo $planId; ?>">
            <input type="hidden" name="planType" value="<?php echo $planType; ?>">
                <div class="input-field col m6 s12">
                    <input id="planName" type="text" name="planName" value="<?php echo $planName; ?>" required>
                    <label for="planName">Plan Name</label>
                </div>
                <div class="input-field col m2 s12">
                    <input id="planCancelledCost"  type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10"  name="planCancelledCost" value="<?php echo $cancelPrice; ?>" required>
                    <label for="planCancelledCost">Plan Actual Cost</label>
                </div> 
                <div class="input-field col m2 s12">
                    <input id="planDiscountCost" type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" name="planDiscountCost" value="<?php echo $plancost; ?>" required>
                    <label for="planDiscountCost">Plan Discount Cost</label>
                </div> 
                <div class="input-field col m2 s12">
                    <input id="planexpiryMonth" type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="5" name="planexpiryMonth" value="<?php echo $planExpiry; ?>" required>
                    <label for="planexpiryMonth">Plan Validity Months</label>
                </div> 
            </div>  

            <div class="row">  
                <div class="input-field col s12">
                    <textarea id="planDesc" class="materialize-textarea" name="planDesc" required><?php echo $planDesc; ?></textarea>
                    <label for="planDesc">Plan Short Description</label>
                </div> 
            </div>  

            <div class="row">  
                <div class="input-field col s12">
                    <textarea id="planFullDesc" class="materialize-textarea" name="planFullDesc" required><?php echo $planFullDesc; ?></textarea>
                    <label for="planFullDesc">Plan Full Description</label>
                </div> 
            </div>  
            
            <div class="row">   
                <div class="input-field col s12 center"> 
                  <button class="btn ardent-orange waves-effect waves-light" type="submit" name="editSubscriptionPlan">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div>
            <?php }  ?> 
            <?php  }else { ?>
             <div class="row center-align">  
               <div class="col m12 s12">
                   <h3 class="text-center"> Your Link is Broken</h3>
                   <img class="z-depth-4 circle" width="80" height="80" src="./images/favicon/no-access-icon.png " alt="">
               </div> 
           </div>
            <?php }  } else { ?>
            <div class="row center-align">  
               <div class="col m12 s12">
                   <h3 class="text-center"> Your Linsk is Broken</h3>
                   <img class="z-depth-4 circle" width="80" height="80" src="./images/favicon/no-access-icon.png " alt="">
               </div> 
           </div>

            <?php } ?>
          </form>    
        </div>
      </div>
    </div>
  </div>
       
    </div>
 
    <!-- END: Footer-->
   <!-- BEGIN VENDOR JS-->
   <script src="js/vendors.min.js"></script>   
    <script src="js/plugins.min.js"></script>  
  </body> 
</html>