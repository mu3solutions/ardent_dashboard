<?php 
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$couponId= ''; 
if(isset($_GET['cid'])){
$couponId = decryptData($_GET['cid']); 
} 
?>

<!DOCTYPE html>   
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> 
    <title>Edit Coupon | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head>
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->
  <!-- END: SideNav-->


    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Edit Coupon  </span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="coupons-list.php"> Coupon  List</a>
                  </li>
                  <li class="breadcrumb-item active">Edit Coupon  
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
          <div class="row"> 
          <form action="./inc/_couponController.php" method="POST"> 
          <?php 
          if(!empty($couponId)) {  
              $sql = "SELECT * FROM `tbl_coupons`  WHERE `coupon_id` = '$couponId'";  
                $result = mysqli_query($dbconnection, $sql);
                if(mysqli_num_rows($result) > 0){ 
                  if  ($row = mysqli_fetch_array($result)) {   
                  ?>   
            <div class="row"> 
            <input type="hidden" name="uniqueId" value="<?php echo $couponId; ?>"> 
                <div class="input-field col m4 s12">
                    <input id="couponName" type="text" name="couponName" value="<?php echo $row['coupon_name']; ?>" required>
                    <label for="couponName">Coupon Name</label>
                </div>
                <div class="input-field col m2 s12">
                    <input id="couponDiscout"  type="text"  name="couponDiscout" value="<?php echo $row['coupon_offer']; ?>"  maxlength="3" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  required>
                    <label for="couponDiscout">Coupon Discount</label>
                </div> 
                <div class="input-field col m2 s12">
                  <input type="text" id="examdate" name="couponExpiry" value="<?php echo $row['coupon_expiry']; ?>" >
                  <label for="examdate">Coupon Expiry Date</label>
                </div> 
                <div class="col m4 s12">
                    <label>Set Coupon Status</label>
                    <select name="coupon_status" required>
                        <option value="1" <?php if($row['coupon_status']=='1'){echo 'selected';}?> > Active </option>
                        <option value="0" <?php if($row['coupon_status']=='0'){echo 'selected';}?> > In Active </option> 
                    </select>
                  </div> 
            </div>   
  
            <div class="row">   
                <div class="input-field col s12 center"> 
                  <button class="btn ardent-orange waves-effect waves-light" type="submit" name="editCoupon">Save Changes
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div>
            <?php }  ?> 
            <?php  }else { ?>
             <div class="row center-align">  
               <div class="col m12 s12">
                   <h3 class="text-center"> Your Link is Broken</h3>
                   <img class="z-depth-4 circle" width="80" height="80" src="./images/favicon/no-access-icon.png " alt="">
               </div> 
           </div>
            <?php }  } else { ?>
            <div class="row center-align">  
               <div class="col m12 s12">
                   <h3 class="text-center"> Your Linsk is Broken</h3>
                   <img class="z-depth-4 circle" width="80" height="80" src="./images/favicon/no-access-icon.png " alt="">
               </div> 
           </div>

            <?php } ?>
          </form>    
        </div>
      </div>
    </div>
  </div>
       
    </div>
 
    <!-- END: Footer-->
   <!-- BEGIN VENDOR JS-->
   <script src="js/vendors.min.js"></script>   
    <script src="js/plugins.min.js"></script>  
    <script src="js/custom/custom-script.min.js"></script>   
  </body> 
</html>