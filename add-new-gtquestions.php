<?php
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title>Add New GT Questions | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
  <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
  <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
  <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
  <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
  <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
  <link rel="stylesheet" type="text/css" href="css/pages/data-tables.min.css"> 
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
  <!-- END: Custom CSS-->
  <style>
  .Wrapper{
    overflow-y: scroll;
  }
  .dataTables_length > label {
    display: none !important;
  
  }  
  .page-length-option_filter  > label {
    position : absolute !important;
    right : 40px !important;
  }
  .dataTables_wrapper .dataTables_filter {
    float: right;
    text-align: left !important;
    margin-top:20px; 
  }    
  </style>
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

   <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->   
 
  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add New GT Question</span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                <li class="breadcrumb-item"><a href="grand-test-list.php">Grand Test List </a>
                <li class="breadcrumb-item active">Add New Question
                </li>
              </ol>
            </div>
             
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
          <div class="col s12">
              <ul class="tabs">
                  <li class="tab col m6"><a class="active"  href="#singleQuestion">Upload Single Question</a></li>
                  <li class="tab col m6"><a href="#excelImport" > Add Questions From Question Bank  </a></li>
              </ul>
          </div>
            <div class="card-content " id="singleQuestion">
              <div class="row ">
                <form action="./inc/_grandTestController.php" method="POST" enctype="multipart/form-data">

                <div class="row ">
                    <div class="col m6 mt-3 s12">
                      <label>Select Grand Test</label>
                      <select name="gtId"   class="select2 browser-default" required>
                      <?php   
                          $selectQuery = "SELECT * FROM `grand_test` ORDER BY `gt_id` DESC";
                          $exeQuery  = mysqli_query($dbconnection, $selectQuery);
                          while ($row = mysqli_fetch_array($exeQuery)) 
                          {  ?>
                              <option value="<?php echo $row["testid"]; ?>"><?php echo $row["gt_test_name"]; ?></option> 
                        <?php } ?>
                      </select>
                    </div> 
                    <div class="col mt-3 m6 s12">
                          <label>Select Main Module</label>
                              <?php 
                              $query = "SELECT * FROM `mcq_module` ORDER BY `mcq_mid` ASC";
                              $result = mysqli_query($dbconnection, $query);
                              ?>
                              <select name="mainModule"  class="select2 browser-default" required>
                              <option value="">Select Module first</option>     
                                  <?php
                                  if (mysqli_num_rows($result) > 0) {
                                    while ($row = mysqli_fetch_array($result)) 
                                    {
                                    echo "
                                      <option value='".$row['mcq_mid']."'>".$row['mcq_mname']."</option>
                                      ";
                                    }
                                  } 
                                  else 
                                  {
                                    echo "<option>'Module Not Available'</option>";
                                  }
                                  ?>
                             </select>
                      </div> 
                  </div> 

                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="Question" class="materialize-textarea" name="Question" required></textarea>
                      <label for="Question">Question</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="option1" type="text" name="option1" required>
                      <label for="option1">Option 1</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="option2" type="text" name="option2" required>
                      <label for="option2">Option 2</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="option3" type="text" name="option3" required>
                      <label for="option3">Option 3</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="option4" type="text" name="option4" required>
                      <label for="option4">Option 4</label>
                    </div>
                  </div>

                  <div class="row ">
                    <div class="col m2 s12">
                      <label>Set Correct Answer</label>
                      <select name="correctoption" required>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                        <option value="4">Option 4</option>
                      </select>
                    </div>

                    <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Question Image : </span>
                      </label>
                    </div>


                    <div class="col m4 s12 file-field inline input-field">

                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="questionImage" name="questionImage" onchange="readURL(this);">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id="fileWrapper">
                      </div>
                    </div>

                    <div class="col m4 s12 file-field inline input-field ">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image" />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
                  </div>


                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="answerExplanation" name="answerExplanation" class="materialize-textarea"></textarea>
                      <label for="answerExplanation">Answer Explanation</label>
                    </div>
                  </div>

                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="answerRef" name="answerRef" class="materialize-textarea"></textarea>
                      <label for="answerRef">Answer Reference</label>
                    </div>
                  </div>

                  <div class="row ">
                    <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Answer Image : </span>
                      </label>
                    </div>


                    <div class="col m6 s12 file-field inline input-field">
                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="answerImage" name="answerImage[]" multiple>
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text">
                      </div>
                    </div>
                  </div>


                  <div class="row"> 
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="addGtQuestion"> Submit <i class="material-icons right">send</i>
                        </button>
                      </div> 
                  </div>

                </form>
              </div>
            </div> 

            <div class="card-content pb-4" id="excelImport">
            <form action="./inc/_grandTestController.php" method="POST" enctype="multipart/form-data">
                 <div class="row ">
                    <div class="col m6 mt-3 s12">
                      <label>Select Grand Test</label>
                      <select name="gtId"  required>
                      <?php   
                          $selectQuery = "SELECT * FROM `grand_test` ORDER BY `gt_id` DESC";
                          $exeQuery  = mysqli_query($dbconnection, $selectQuery);
                          while ($row = mysqli_fetch_array($exeQuery)) 
                          {  ?>
                              <option value="<?php echo $row["testid"]; ?>"><?php echo $row["gt_test_name"]; ?></option> 
                        <?php } ?>
                      </select>
                    </div> 
                    <div class="col mt-3 m6 s12">
                          <label>Select Main Module</label>
                          <?php 
                              $query = "SELECT * FROM `mcq_module` ORDER BY `mcq_mid` ASC";
                              $result = mysqli_query($dbconnection, $query);
                              ?>
                              <select name="mainsModule"  class="select2 browser-default" id="multi_search_filter" required>
                              <option value="">Select Module first</option>     
                                  <?php
                                  if (mysqli_num_rows($result) > 0) {
                                    while ($row = mysqli_fetch_array($result)) 
                                    {
                                    echo "
                                      <option value='".$row['mcq_mid']."'>".$row['mcq_mname']."</option>
                                      ";
                                    }
                                  } 
                                  else 
                                  {
                                    echo "<option>'Module Not Available'</option>";
                                  }
                                  ?>
                             </select>
                      </div> 
                  </div>

                  <div class="cools"  style="position:sticky;top:0;background-color:#fff;z-index:999;">
                    <div class="row displayafter display-none"> 
                        <div class="input-field col s12 center">
                          <button class="btn ardent-orange waves-effect waves-light" type="submit" name="addSelectedGtQuestion"> Add Selected Questions <i class="material-icons right">send</i>
                          </button>
                        </div> 
                    </div>
                    
                    <div class="row displayafter display-none"> 
                        <div class="input-field col m4 s12 right">
                        <input type="text" id="search" placeholder="Search Questions">
                        </div> 
                    </div>

                  </div>

                   

                  <div class="Wrapper displayafter display-none">
                    <table class="centered bordered"  id="page-length-option">
                      <thead>
                          <tr>
                            <th style="min-width: 50px;">Action</th>
                            <th style="min-width: 300px;">Question</th>
                            <th>Option 1</th>
                            <th>Option 2</th>  
                            <th>Option 3</th>  
                            <th>Option 4</th>  
                          </tr>
                      </thead>
                          <tbody> 
                          </tbody>
                    </table>
                  </div>
              </form> 
            </div>
          </div>
        </div> 
      </div> 
      <script src="js/vendors.min.js"></script>
      <script src="vendors/data-tables/js/jquery.dataTables.min.js"></script>
      <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script> 
      <script src="js/plugins.min.js"></script> 
      <script src="js/scripts/data-tables.min.js"></script>   
      <script src="js/scripts/ui-alerts.min.js"></script>
      <script src="vendors/select2/select2.full.min.js"></script>
      <script src="js/scripts/form-select2.min.js"></script>
      <script src="js/custom/custom-script.min.js"></script>  
      <script>
            $(document).ready(function(){ 
                    
              load_data();                     
              
              function load_data(query='')
              {
                      $.ajax({
                      url:"inc/fetchQBank.php",
                      method:"POST",
                      data:{search:query,fetchQuestions:1},
                      success:function(data)
                      { 
                        if(data != 'false')
                        {  
                          $('tbody').html(data);
                        }
                      }
                      })
              }

              $('#multi_search_filter').change(function(){ 
                $('.displayafter').show();
                var query = $('#multi_search_filter').val()
                load_data(query);
              });
            
            });

            $("#search").on("keyup", function() {
                var value = $(this).val(); 
                $("table tr").each(function(index) { 
                    if (index !== 0) { 
                        $row = $(this); 
                        var id = $row.find("td:nth-child(2)").text(); 
                        if (id.indexOf(value) !== 0){ $row.hide(); }
                        else { $row.show(); }
                    }
                  });
            }); 
      </script> 
</body> 
</html>