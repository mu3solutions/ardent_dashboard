<?php 
include('./inc/dbConfig.php');
include('./inc/functions.php'); 
include('./inc/authenticate.php');
$Fid = '';

if(isset($_GET['fid'])){
    $Fid = decryptData($_GET['fid']);
}
?>

<!DOCTYPE html>
  
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Add New Student | Ardent MDS </title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css"> 
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css"> 
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
    <!-- END: Custom CSS-->
  </head> 
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

     <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->


    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add New FAQ </span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="faq-list.php"> FAQ List</a>
                  </li>
                  <li class="breadcrumb-item active">Add New FAQ 
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content"> 
          <div class="row"> 
          <form action="./inc/_faqController.php" method="POST"  enctype="multipart/form-data">
          <?php  
				$sql = "SELECT * FROM `tbl_FAQs` WHERE `FAQ_id` = '$Fid'";
				$result = mysqli_query($dbconnection, $sql);
				if ($row = mysqli_fetch_array($result)) {  
				?>    
                <input type="hidden" name="FAQ_id" value="<?php  echo $row['FAQ_id'];?>">
            <div class="row">  
                <div class="input-field col s12">
                    <textarea type="url" id="faqQuestion" class="materialize-textarea" name="faqQuestion" required><?php  echo $row['FAQ_questions'];?></textarea>
                    <label for="faqQuestion">FAQ Question</label>
                </div> 
            </div>
            
            <div class="row">  
                <div class="input-field col s12">
                <textarea type="url" id="faqAns" class="materialize-textarea" name="faqAns" required><?php  echo $row['FAQ_Answer'];?></textarea>
                    <label for="faqAns">FAQ Answer</label>
                </div> 
            </div>
            
            <div class="row ">     

                    <div class="col m4 s12">
                      <label>Set FAQ Status</label>
                      <select name="faqStatus" required>
                      <option value="1" <?php if($row["status"] == '1'){echo "selected"; } ?>> Active </option> 
                        <option value="0" <?php if($row["status"] == '0'){echo "selected"; } ?>> In-Active </option> 
                      </select>
                    </div>

                  </div>
 
            
            <div class="row">   
                <div class="input-field col s12 center"> 
                  <button class="btn ardent-orange waves-effect waves-light" type="submit" name="editFAQ">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div> 
            <?php } ?>
          </form>    
        </div>
      </div>
    </div>
  </div>
</div>
 
    <!-- END: Footer-->
   <!-- BEGIN VENDOR JS-->
   <script src="js/vendors.min.js"></script>   
    <script src="js/plugins.min.js"></script> 
    <script src="vendors/formatter/jquery.formatter.min.js"></script> 
    <script src="js/custom/custom-script.min.js"></script> 
    <!-- END PAGE LEVEL JS-->
  </body> 
</html>