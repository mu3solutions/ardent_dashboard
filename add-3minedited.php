<?php
include('./inc/dbConfig.php');
include('./inc/functions.php');
include('./inc/authenticate.php');
$testDate = ''; 
if (isset($_GET['testDate'])) {
  $testDate = decryptData($_GET['testDate']) ;
} 
$questionCount =  nuggetsQuestionCount($testDate,$dbconnection);
if ($questionCount >= 10) 
{
  header('location:3-min-challenges.php');
}
?>

<!DOCTYPE html> 
<html class="loading" lang="en" data-textdirection="ltr"> <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
  <title>Add New Student | Ardent MDS </title>
  <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
  <link rel="shortcut icon" type="image/x-icon" href="images/logo/014.png">
  <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
  <link rel="stylesheet" href="vendors/select2/select2.min.css" type="text/css">
  <link rel="stylesheet" href="vendors/select2/select2-materialize.css" type="text/css">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/themes/vertical-modern-menu-template/style.min.css">
  <link rel="stylesheet" type="text/css" href="css/pages/page-users.min.css">
  <link rel="stylesheet" href="vendors/introjs/css/introjs.css"> 
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="css/custom/custom.css">
  <!-- END: Custom CSS-->
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

   
   <!-- BEGIN: Header-->
   <?php 
    include_once('inc/header.php');   
    include_once('sidebar.php'); ?> 
    <!-- BEGIN: Page Main-->

  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple "></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
          <div class="row">

            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title mt-0 mb-0"><span>Add New 3 Min Questions</span></h5>
              <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="home-dashboard.php">Home</a>
                <li class="breadcrumb-item"><a href="3-min-challenges.php">3 Min Challenges List </a>
                <li class="breadcrumb-item active">Add New 3 Min Questions
                </li>
              </ol>
            </div> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <div class="card"> 
                <ul class="tabs">
                    <li class="tab col m6"><a class="active"  href="#singleQuestion">Upload Single Question</a></li>
                    <li class="tab col m6"><a  href="#excelImport" > Add Questions From Question Bank  </a></li>
                </ul>
            <div class="card-content" id="singleQuestion">
              <div class="row">
                <form action="./inc/_3minchallenge.php" method="POST" enctype="multipart/form-data">
                <div class="row">
                <div class="col m6  s12">
            <?php if (isset($_SESSION['ntquestionaddsuccess'])) { ?> 
                                    <div class="card-alert card green">
                                        <div class="card-content white-text">
                                            <p><?php echo $_SESSION['ntquestionaddsuccess']; ?></p>
                                        </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                            </div> 
                <?php
                }   
                unset($_SESSION['ntquestionaddsuccess']);
                if (isset($_SESSION['ntquestioaddnerror'])) {
                ?>  
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p><?php echo $_SESSION['ntquestioaddnerror']; ?></p>
                        </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                    </div> 
                                <?php
                                }
                                unset($_SESSION['ntquestioaddnerror']);
                                ?>
            </div>
                </div>
                <?php
                if( !empty($testDate)){
                  if($questionCount < 10)
                  { 
                      $balanceQuestion =  10 - $questionCount ; 
                      $chipColor  = 'chip red lighten-5';
                      $chipTextColor  = 'red-text';
                      $chipText  = 'Warning : Only ' .$questionCount.' questions present in this date , Add '. $balanceQuestion. ' More .';
                  }
                ?>
                  <div class="row"> 
                    <div class="input-field col m4 s4"> 
                      <input 
                            type="text" 
                            id="3minChallengeDate" 
                            name="3minChallengeDate"  
                            value="<?php echo $testDate; ?>"  
                            readonly
                      > 
                      <label for="3minChallengeDate">Question Date</label>
                    </div>  
                    <div class="input-field col m6 s4"> 
                        <span class="<?php echo $chipColor; ?>">
                          <span class="<?php echo $chipTextColor; ?>">
                            <?php echo $chipText; ?>
                          </span>
                        </span>
                    </div>  
                  </div>

                  <div class="row">
                    <div class="input-field col s12">
                      <textarea id="3minQuestion" class="materialize-textarea" name="3minQuestion" required></textarea>
                      <label for="3minQuestion">Question</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="3minoption1" type="text" name="3minoption1" required>
                      <label for="3minoption1">Option 1</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="3minoption2" type="text" name="3minoption2" required>
                      <label for="3minoption2">Option 2</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col m6 s12">
                      <input id="3minoption3" type="text" name="3minoption3" required>
                      <label for="3minoption3">Option 3</label>
                    </div>
                    <div class="input-field col m6 s12">
                      <input id="3minoption4" type="text" name="3minoption4" required>
                      <label for="3minoption4">Option 4</label>
                    </div>
                  </div>

                  <div class="row ">
                    <div class="col m2 s12">
                      <label>Set Correct Answer</label>
                      <select name="3mincorrectoption" required>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                        <option value="4">Option 4</option>
                      </select>
                    </div>

                    <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Question Image : </span>
                      </label>
                    </div>


                    <div class="col m4 s12 file-field inline input-field">

                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="questionImage" name="3minquestionImage" onchange="readURL(this);" accept="image/x-png,image/jpeg" />
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text" id="fileWrapper">
                      </div>
                    </div>

                    <div class="col m4 s12 file-field inline input-field ">
                      <div class="wrapper display-none" id="displayDiv">
                        <p>Image Preview : </p>
                        <img class="border-radius-10" id="questionImagePreview" src="#" alt="No Image" />
                        <button type="button" id="removeQuestion" class="btn-floating mb-1 waves-effect waves-light ardent-orange">
                          <i class="material-icons">clear</i>
                        </button>
                      </div>
                    </div>
                  </div>


                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="3minanswerExplanation" name="3minanswerExplanation" class="materialize-textarea"></textarea>
                      <label for="3minanswerExplanation">Answer Explanation</label>
                    </div>
                  </div>

                  <div class="row mt-2">
                    <div class="input-field col s12">
                      <textarea id="answerReferece" name="answerReferece" class="materialize-textarea"></textarea>
                      <label for="answerReferece">Reference Explanation</label>
                    </div>
                  </div>


                  <div class="row ">
                    <div class="col m2 s12 mt-3">
                      <label>
                        <span class="black-text"> Upload Answer Image : </span>
                      </label>
                    </div> 
                    <div class="col m6 s12 file-field inline input-field"> 
                      <div class="btn float-right inline ardent-orange">
                        <span>Choose File</span>
                        <input type="file" id="answerImage" name="answerImage[]" multiple accept="image/x-png,image/jpeg" />
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate valid" type="text">
                      </div>
                    </div>
                  </div> 
                  <div id="preview"></div>
                  <div class="row"> 
                      <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="add3minQuestion">Submit
                          <i class="material-icons right">send</i>
                        </button>
                      </div>  
                  </div> 
                </form>
                <?php } else { ?>
                  <div class="row center-align">  
                <div class="col m12 s12">
                    <h3 class="text-center"> Your Link is Broken</h3>
                    <img class="z-depth-4 circle" width="80" height="80" src="./images/favicon/no-access-icon.png " alt="">
                </div> 
            </div>

                <?php } ?>
              </div>
            </div> 



            <div class="card-content pb-4" id="excelImport">
              <form action="./inc/_3minchallenge.php" method="POST" enctype="multipart/form-data" id="forms">
              <input type="hidden" name="gtId" value="<?php echo $testID; ?>">
                 <div class="row ">
                 <div class="col m6 mt-3 s12">
                            <?php if(empty($testDate)){ ?>
                            <div class="input-field"> 
                              <input type="text" id="3minChallengeDate2" name="3minChallengeDate"  required>
                              <label for="3minChallengeDate" >Question Date</label>
                            </div> 
                            <?php } else { ?>
                            <div class="input-field"> 
                              <input type="text" id="3minChallengeDate2" name="3minChallengeDate" value="<?php echo $testDate; ?>" required readonly>
                              <label for="3minChallengeDate">Question Date</label>
                            </div> 
                        <?php } ?>
                    </div> 
                    <div class="col mt-3 m6 s12">
                          <label>Select Main Module</label>
                          <?php 
                              $query = "SELECT * FROM `mcq_module` ORDER BY `mcq_mid` ASC";
                              $result = mysqli_query($dbconnection, $query);
                              ?>
                              <select name="mainsModule"  class="select2 browser-default" id="multi_search_filter" required>
                              <option value="">Select Module first</option>     
                                  <?php
                                  if (mysqli_num_rows($result) > 0) {
                                    while ($row = mysqli_fetch_array($result)) 
                                    {
                                    echo "
                                      <option value='".$row['mcq_mid']."'>".$row['mcq_mname']."</option>
                                      ";
                                    }
                                  } 
                                  else 
                                  {
                                    echo "<option>'Module Not Available'</option>";
                                  }
                                  ?>
                             </select>
                      </div> 
                  </div>

                  <div class="cools"  style="position:sticky;top:0;background-color:#fff;z-index:999;">
                    <div class="row displayafter display-none"> 
                        <div class="input-field col s12 center">
                        <button class="btn ardent-orange waves-effect waves-light" type="submit" name="addSelectedNtQuestion"> Add Selected Questions <i class="material-icons right">send</i>
                          </button>
                        </div> 
                    </div>
                    
                    <div class="row displayafter display-none"> 
                        <div class="input-field col m4 s12 right">
                          <input type="text" id="search" placeholder="Search Questions"> 
                        </div> 
                    </div>

                  </div>

                   

                  <div class="Wrapper displayafter display-none">
                    <table class="centered bordered"  id="page-length-option">
                      <thead>
                          <tr>
                            <th style="min-width: 50px;">Action</th>
                            <th style="min-width: 300px;">Question</th>
                            <th>Option 1</th>
                            <th>Option 2</th>  
                            <th>Option 3</th>  
                            <th>Option 4</th>  
                          </tr>
                      </thead>
                          <tbody> 
                          </tbody>
                    </table>
                  </div>
              </form> 
            </div>


          </div>
        </div> 
      </div> 
      <script src="js/vendors.min.js"></script> 
      <script src="js/plugins.min.js"></script>
      <script src="js/scripts/ui-alerts.min.js"></script> 
      <script src="js/custom/custom-script.min.js"></script> 
      <script src="vendors/select2/select2.full.min.js"></script>
      <script src="js/scripts/form-select2.min.js"></script>  
      <script> 
     $(function(){

      $('#3minChallengeDate').datepicker({
        autoClose: !0,
        minDate: new Date(),
        format: "dd-mm-yyyy"
      });
      
      $('#3minChallengeDate2').datepicker({
        autoClose: !0,
        minDate: new Date(),
        format: "dd-mm-yyyy"
      });
        
      load_data();                     
              
              function load_data(query='')
              {
                      $.ajax({
                      url:"inc/fetchQBank.php",
                      method:"POST",
                      data:{search:query,fetchQuestions:1},
                      success:function(data)
                      { 
                        if(data != 'false')
                        {  
                          $('tbody').html(data);
                        }
                      }
                      })
              }

              $('#multi_search_filter').change(function(){  
                $('.displayafter').show();
                var query = $('#multi_search_filter').val()
                load_data(query);
                
              var sforms = $("input[type='checkbox']").length; 
              });
              

              var forms = $("input[type='checkbox']");

              // console.log(forms);
              
              $(".change").change(function(){ 
                  var countCheckedCheckboxes = forms.filter(':checked').length;

                  console.log(countCheckedCheckboxes);
                  $('#count-checked-checkboxes').text(countCheckedCheckboxes);
              });



     });

     $("#search").on("keyup", function() {
                var value = $(this).val(); 
                $("table tr").each(function(index) { 
                    if (index !== 0) { 
                        $row = $(this); 
                        var id = $row.find("td:nth-child(2)").text(); 
                        if (id.indexOf(value) !== 0){ $row.hide(); }
                        else { $row.show(); }
                    }
                  });
            }); 
     </script>
</body> 
</html>